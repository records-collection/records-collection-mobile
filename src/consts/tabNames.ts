const TAB_NAMES = {
  SEARCH: 'Buscar',
  COLLECTION: 'Colección',
  PREFERENCES: 'Preferencias',
  POSTS: 'Publicaciones',
}

export { TAB_NAMES }
