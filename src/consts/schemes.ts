const DARK = 'dark'
const LIGHT = 'light'
const SYSTEM = 'system'

export const SCHEMES = {
  DARK,
  LIGHT,
  SYSTEM,
}
