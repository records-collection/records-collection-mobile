export const QUERY_KEYS = {
  COLLECTION_SEARCH_KEY: 'CollectionSearchKey',
  SEARCH_QUERY_KEY: 'SearchQueryKey',
  LIST_IN_SALE_KEY: 'ListInSaleKey',
  LIST_EXCHANGE_KEY: 'ListExchangeKey',
  WISH_LIST_KEY: 'WishListKey',
  GET_RELEASE_BY_ID_KEY: 'GetReleaseByIdKey',
  BARCODE_SCAN_KEY: 'BarCodeSanKey',
  GET_COLLECTION_RELEASE_BY_ID_KEY: 'GetCollectionReleaseByIdKey',
  SEARCH_ARTISTS_KEY: 'SearchArtistKey',
  GET_ARTIST_BY_ID_KEY: 'GetArtistByIdKey',
  GET_MASTERS_BY_ARTIS_ID_KEY: 'GetMastersByArstisIdKey',
  GET_MASTER_BY_ID_KEY: 'GetMasterByIdKey',
  GET_VERSIONS_KEY: 'GetVersionsKey',
  POSTS_KEY: 'PostsKey',
  SAVED_POSTS_KEY: 'SavedPostsKey',
  POST_KEY: 'PostKey',
  LIKES_POST_KEY: 'LikesPostKey',
  CHATS_KEY: 'ChatsKey',
  CHAT_KEY: 'ChatKey',
  MY_POSTS_KEY: 'MyPostsKey',
}
