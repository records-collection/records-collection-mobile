const USER_OR_PASSWORD_INCORRECT = 'Usuario y/o contraseña incorrecta'
const REQUIRED_USER_AND_PASSWORD = 'Ingrese usuario y contraseña'
const USERNAME_ALREADY_IN_USE = 'Ya existe un usuario con ese nombre'
const EMAIL_ALREADY_IN_USE = 'Ya existe un usuario con ese email'
const WRONG_VERIFICATION_CODE = 'Código incorrecto'
const ACCOUNT_NOT_CONFIRMED_ERROR = 'AccountNotConfirmedError'
const RESOURCE_ALREADY_EXISTS = 'ResourceAlreadyExists'

export const ERRORS = {
  USER_OR_PASSWORD_INCORRECT,
  REQUIRED_USER_AND_PASSWORD,
  USERNAME_ALREADY_IN_USE,
  WRONG_VERIFICATION_CODE,
  ACCOUNT_NOT_CONFIRMED_ERROR,
  EMAIL_ALREADY_IN_USE,
  RESOURCE_ALREADY_EXISTS,
}
