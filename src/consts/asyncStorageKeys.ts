const TOKEN = 'token'
const SCHEME = 'scheme'

export const ASYNC_STORAGE_KEYS = {
  TOKEN,
  SCHEME,
}
