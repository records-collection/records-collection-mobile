const MESSAGE_TYPES = {
  MESSAGE: 'Message',
  RESERVATION: 'Reservation',
  CANCELED: 'Canceled',
  SOLD: 'Sold',
}

export { MESSAGE_TYPES }
