import { Platform, Dimensions, PixelRatio } from 'react-native'

const isIOS = Platform.OS === 'ios'
const isAndroid = Platform.OS === 'android'

function getWindowHeight() {
  return Dimensions.get('window').height
}

function getWindowWidth() {
  return Dimensions.get('window').width
}

const widthPercentageToDP = (widthPercent: number) => {
  return PixelRatio.roundToNearestPixel((getWindowWidth() * widthPercent) / 100)
}

const heightPercentageToDP = (heightPercent: number) => {
  return PixelRatio.roundToNearestPixel((getWindowHeight() * heightPercent) / 100)
}

export const DeviceUtils = {
  isIOS,
  isAndroid,
  getWindowHeight,
  getWindowWidth,
  widthPercentageToDP,
  heightPercentageToDP,
}
