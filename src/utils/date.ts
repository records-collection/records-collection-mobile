import moment from 'moment'
require('moment/locale/es')

const getDiff = (dateParam: Date) => {
  const now = moment(new Date())
  const date = moment(dateParam)
  const diff = now.diff(date, 'minutes')
  return moment.duration(diff, 'minutes').humanize()
}

const format = (date: Date) => {
  return moment(date).locale('es').format('LLLL')
}

const formatOnlyDate = (date: Date) => {
  return moment(date).locale('es').format('LL')
}

const formatOnlyHour = (date: Date) => {
  return moment(date).locale('es').format('HH:mm')
}

export const DateUtils = {
  getDiff,
  format,
  formatOnlyDate,
  formatOnlyHour,
}
