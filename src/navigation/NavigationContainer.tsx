import React, { useContext, useEffect, useState } from 'react'
import {
  NavigationContainer as ReactNavigationContainer,
  DefaultTheme,
} from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import EStyleSheet from 'react-native-extended-stylesheet'

import { SCREEN_NAMES, TAB_NAMES, SCHEMES } from 'consts'
import { darkTheme, lightTheme, DarkTheme } from 'styles'
import { SearchStack, CollectionStack, LoginStack, ProfileStack, PostsStack } from 'features'
import { UserContext } from 'contexts/UserContext'
import { AddOrEditRelease, CreatePost, EditPost, ImagesView, TabIcon } from 'components'
import { ThemeContext } from 'contexts'
import { NavigationService } from 'services'

const Tab = createBottomTabNavigator()

const RootStack = createStackNavigator()

const TabsNavigator = () => {
  const { theme } = useContext(ThemeContext)
  const isDarkTheme = theme === SCHEMES.DARK

  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ color }) => {
          return <TabIcon route={route} color={color} />
        },
      })}
      tabBarOptions={{
        activeTintColor: isDarkTheme ? 'white' : 'black',
        inactiveTintColor: 'gray',
      }}
      initialRouteName={TAB_NAMES.POSTS}
    >
      <Tab.Screen name={TAB_NAMES.POSTS} component={PostsStack} />
      <Tab.Screen name={TAB_NAMES.SEARCH} component={SearchStack} />
      <Tab.Screen name={TAB_NAMES.COLLECTION} component={CollectionStack} />
      <Tab.Screen name={TAB_NAMES.PREFERENCES} component={ProfileStack} />
    </Tab.Navigator>
  )
}

const HomeStack = () => (
  <RootStack.Navigator mode="modal" screenOptions={{ headerBackTitleVisible: false }}>
    <RootStack.Screen name="Home" component={TabsNavigator} options={{ headerShown: false }} />
    <RootStack.Screen
      name={SCREEN_NAMES.IMAGE_VIEW_SCREEN}
      component={ImagesView}
      options={{ headerShown: false }}
    />
    <RootStack.Screen name={SCREEN_NAMES.ADD_OR_EDIT_RELEASE_SCREEN} component={AddOrEditRelease} />
    <RootStack.Screen name={SCREEN_NAMES.CREATE_POST_SCREEN} component={CreatePost} />
    <RootStack.Screen name={SCREEN_NAMES.EDIT_POST_SCREEN} component={EditPost} />
  </RootStack.Navigator>
)

const NavigationContainer = () => {
  const { user } = useContext(UserContext)
  const { theme } = useContext(ThemeContext)

  const [renderApp, setRenderApp] = useState(true)

  const updateApp = async () => {
    await setRenderApp(false)
    setRenderApp(true)
  }

  //TODO: find better way to manage change of themes
  useEffect(() => {
    EStyleSheet.build(theme === SCHEMES.DARK ? darkTheme : lightTheme)
    updateApp()
  }, [theme])

  return renderApp ? (
    <ReactNavigationContainer
      ref={NavigationService.setTopLevelNavigator}
      theme={theme === SCHEMES.DARK ? DarkTheme : DefaultTheme}
    >
      {!user ? <LoginStack /> : <HomeStack />}
    </ReactNavigationContainer>
  ) : null
}

export { NavigationContainer }
