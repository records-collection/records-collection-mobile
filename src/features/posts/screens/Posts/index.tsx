import React from 'react'
import { Spinner } from 'native-base'
import { ScrollView } from 'react-native-gesture-handler'
import { RefreshControl } from 'react-native'

import { Container } from 'components'
import { usePosts } from 'hooks'

import { PostCard } from './components'

const Posts: React.FC = () => {
  const { posts, isLoading, refresh, isRefreshing } = usePosts()

  return (
    <Container>
      <ScrollView refreshControl={<RefreshControl refreshing={isRefreshing} onRefresh={refresh} />}>
        {isLoading && <Spinner color="gray" />}
        {posts.map((post) => (
          <PostCard key={post._id} post={post} />
        ))}
      </ScrollView>
    </Container>
  )
}

export { Posts }
