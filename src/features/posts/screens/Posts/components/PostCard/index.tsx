import React from 'react'
import { Body, Card, CardItem, Left, Text, View } from 'native-base'
import { Image, TouchableOpacity } from 'react-native'

import { Post as PostType } from 'types'
import { NavigationService } from 'services'
import { SCREEN_NAMES } from 'consts'
import { AvatarIcon } from 'assets/icons'
import { getFormat } from 'features/collection/utils'
import { CardInfo } from 'features/posts/components'

import { styles } from './styles'

interface PostCardProps {
  post: PostType
}

const PostCard: React.FC<PostCardProps> = ({ post }) => {
  const navigateToPostDetail = (focusInput?: boolean) => {
    NavigationService.navigate(SCREEN_NAMES.POST_DETAIL_SCREEN, { postId: post._id, focusInput })
  }

  const getImage = () => {
    if (post.release.ownImages?.length) return post.release.ownImages[0].uri
    return post.release.images[0].uri
  }

  return (
    <Card style={styles.card}>
      <CardItem style={styles.cardItem} first>
        <Left>
          <TouchableOpacity>
            <AvatarIcon width={45} height={45} />
          </TouchableOpacity>
          <Body>
            <TouchableOpacity>
              <Text style={styles.username}>{post.user.username}</Text>
            </TouchableOpacity>
          </Body>
        </Left>
      </CardItem>
      <CardItem style={styles.cardItem}>
        <Body>
          <View style={styles.infoContainer}>
            <Text style={styles.title}>
              {post.release.artists[0].name} - {post.release.title}
            </Text>
            <Text style={styles.text}>
              {post.release.country} - {post.release.year}
            </Text>
            <Text style={styles.textInfo}>{getFormat(post.release)}</Text>
            <TouchableOpacity style={styles.seeDetail} onPress={() => navigateToPostDetail()}>
              <View style={styles.seeDetailContainer}>
                <Text style={styles.seeMoreText}>Ver más</Text>
              </View>
            </TouchableOpacity>
          </View>
        </Body>
      </CardItem>
      <TouchableOpacity onPress={() => navigateToPostDetail()}>
        <CardItem cardBody style={styles.cardItem}>
          <Image style={styles.image} source={{ uri: getImage() }} />
        </CardItem>
      </TouchableOpacity>
      <CardInfo post={post} isCard />
    </Card>
  )
}

export { PostCard }
