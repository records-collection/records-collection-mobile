import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  card: {
    borderRadius: 0,
    borderWidth: 0,
    borderLeftColor: '$backgroundColor',
    borderRightColor: '$backgroundColor',
  },
  cardItem: {
    borderRadius: 0,
    backgroundColor: '$backgroundColor',
  },
  image: {
    height: 220,
    flex: 1,
  },
  heart: {
    color: '#ED4A6A',
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  username: {
    color: '$textColor',
    textDecorationLine: 'underline',
    fontWeight: '500',
  },
  text: {
    color: '$textColor',
  },
  textUnderline: {
    textDecorationLine: 'underline',
  },
  icon: {
    color: '$textColor',
    fontSize: 25,
    marginRight: 5,
  },
  iconContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  priceContainer: {
    padding: 5,
    borderRadius: 5,
    backgroundColor: '$backgroundContrastColor',
  },
  priceText: {
    color: '$textColor',
    fontWeight: 'bold',
    fontSize: '1.2rem',
  },
  seeMoreText: {
    color: '$textColor',
    fontSize: '1rem',
    textDecorationLine: 'underline',
  },
  button: {
    width: 100,
    justifyContent: 'center',
  },

  infoContainer: {
    flex: 1,
    flexGrow: 1,
  },
  title: {
    color: '$textColor',
    fontSize: '1.2rem',
    fontWeight: '500',
    marginBottom: 20,
  },
  info: {
    flexDirection: 'row',
    flex: 1,
    flexGrow: 1,
  },
  textInfo: {
    color: '$textColor',
    fontSize: '1.rem',
    flex: 1,
    flexWrap: 'wrap',
  },
  textLabel: {
    color: '$textColor',
    fontSize: '1.rem',
    marginBottom: 5,
    width: 130,
  },
  separator: {
    height: 1,
    width: '100%',
    alignSelf: 'center',
    backgroundColor: '#d7d7d7',
    marginVertical: 10,
  },
  section: {
    color: 'gray',
    marginTop: 20,
  },
  seeDetail: {
    marginTop: 20,
  },
  seeDetailContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  fowardIcon: {
    color: 'gray',
  },
})

export { styles }
