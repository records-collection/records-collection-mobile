import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  commentsContainer: {
    margin: 20,
  },
  commentsTile: {
    color: '$textColor',
    fontWeight: 'bold',
    fontSize: '1.1rem',
    marginBottom: 20,
  },
  item: {
    borderColor: '$backgroundContrastColor',
    borderTopWidth: 1,
  },
  input: {
    color: '$textColor',
    backgroundColor: '$backgroundContrastColor',
    borderRadius: 30,
    height: 40,
    margin: 10,
    paddingLeft: 20,
    paddingRight: 20,
    width: '80%',
  },
  sendIcon: {
    color: '$backgroundContrastColor',
    fontSize: 35,
  },
  sendIconActive: {
    color: '$textColor',
    fontSize: 35,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
  },
  headerTitleContent: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 20,
  },
  textUsername: {
    color: '$textColor',
    textDecorationLine: 'underline',
    fontWeight: '500',
    marginLeft: 8,
  },
  infoContainer: {
    margin: 10,
    flex: 1,
    flexGrow: 1,
  },
  title: {
    color: '$textColor',
    fontSize: '1.2rem',
    fontWeight: '500',
    marginBottom: 20,
  },
  info: {
    flexDirection: 'row',
    flex: 1,
    flexGrow: 1,
  },
  text: {
    color: '$textColor',
    fontSize: '1.rem',
    flex: 1,
    flexWrap: 'wrap',
  },
  textLabel: {
    color: '$textColor',
    fontSize: '1.rem',
    marginBottom: 5,
    width: 130,
  },
  separator: {
    height: 1,
    width: '100%',
    alignSelf: 'center',
    backgroundColor: '#d7d7d7',
    marginVertical: 10,
  },
  section: {
    color: 'gray',
    marginTop: 20,
  },
  seeDetail: {
    marginTop: 20,
  },
  seeDetailContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  fowardIcon: {
    color: 'gray',
  },
})

export { styles }
