import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginBottom: 15,
  },
  content: {
    marginLeft: 10,
    flexDirection: 'column',
  },
  commentContainer: {
    backgroundColor: '$backgroundContrastColor',
    borderRadius: 10,
    padding: 10,
    marginBottom: 5,
  },
  username: {
    fontWeight: 'bold',
    color: '$textColor',
  },
  text: {
    color: '$textColor',
  },
})

export { styles }
