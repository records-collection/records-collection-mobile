import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  date: {
    textDecorationLine: 'underline',
    color: '$textColor',
  },
})

export { styles }
