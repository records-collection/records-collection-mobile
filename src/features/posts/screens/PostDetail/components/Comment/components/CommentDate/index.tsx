import React, { useState } from 'react'
import { Text } from 'native-base'
import { TouchableOpacity } from 'react-native'

import { DateUtils } from 'utils'

import { styles } from './styles'

interface CommentDate {
  date: Date
}

const CommentDate: React.FC<CommentDate> = ({ date }) => {
  const [showDiff, setShowDiff] = useState(true)

  return (
    <TouchableOpacity onPress={() => setShowDiff(!showDiff)}>
      <Text style={styles.date}>{showDiff ? DateUtils.getDiff(date) : DateUtils.format(date)}</Text>
    </TouchableOpacity>
  )
}

export { CommentDate }
