import React from 'react'
import { View, Text } from 'native-base'

import { Comment as CommentType } from 'types'
import { AvatarIcon } from 'assets/icons'

import { styles } from './styles'
import { CommentDate } from './components'

interface CommentProps {
  comment: CommentType
}

const Comment: React.FC<CommentProps> = ({ comment }) => (
  <View style={styles.container}>
    <AvatarIcon width={40} height={40} />
    <View style={styles.content}>
      <View style={styles.commentContainer}>
        <Text style={styles.username}>{comment.username}</Text>
        <Text style={styles.text}>{comment.comment}</Text>
      </View>
      <CommentDate date={comment.createdAt} />
    </View>
  </View>
)

export { Comment }
