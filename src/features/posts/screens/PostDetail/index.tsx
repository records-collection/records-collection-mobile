import React, { useContext, useEffect, useLayoutEffect, useRef, useState } from 'react'
import { Content, Icon, Item, Spinner, Text, View } from 'native-base'
import { KeyboardAvoidingView, TouchableOpacity, TextInput } from 'react-native'
import { useNavigation } from '@react-navigation/native'
import { HeaderBackButton } from '@react-navigation/stack'

import { ButtonHeader, Container, ImageSlider } from 'components'
import { useCreateComment, usePost } from 'hooks'
import { AvatarIcon } from 'assets/icons'
import { NavigationService } from 'services'
import { getFormat } from 'features/collection/utils'
import { CardInfo } from 'features/posts/components'
import { SCREEN_NAMES } from 'consts'
import { UserContext } from 'contexts'

import { Comment } from './components'
import { styles } from './styles'

interface PostDetailProps {
  route: { params: { postId: string; focusInput?: boolean } }
}

const PostDetail: React.FC<PostDetailProps> = ({ route }) => {
  const { user } = useContext(UserContext)

  const { postId, focusInput } = route.params
  const { post, isLoading } = usePost(postId)
  const { setOptions } = useNavigation()

  const [message, setMessage] = useState('')

  const { create } = useCreateComment()

  const createComment = () => {
    if (post && message) {
      create({ postId: post._id, comment: message })
    }
    setMessage('')
  }

  const inputRef = useRef<TextInput>(null)

  useEffect(() => {
    if (focusInput && inputRef.current) {
      inputRef.current.focus()
    }
  }, [inputRef, focusInput])

  useLayoutEffect(() => {
    setOptions({
      headerLeft: () => (
        <View style={styles.header}>
          <HeaderBackButton label=" " onPress={NavigationService.goBack} />
          <TouchableOpacity style={styles.headerTitleContent}>
            <AvatarIcon width={35} height={35} />
            <Text style={styles.textUsername}>{post?.user.username ?? ''}</Text>
          </TouchableOpacity>
        </View>
      ),
      headerRight: () => (
        <View style={styles.header}>
          {user?.id === post?.user._id && (
            <ButtonHeader
              icon="edit"
              onPress={() =>
                NavigationService.navigate(SCREEN_NAMES.EDIT_POST_SCREEN, {
                  post,
                })
              }
            />
          )}
        </View>
      ),
    })
  }, [post?.user.username, setOptions, user?.id, post?.user._id, post])

  return (
    <Container>
      {isLoading ? (
        <Spinner color="gray" />
      ) : (
        <>
          {post && (
            <>
              <Content>
                <View style={styles.infoContainer}>
                  <Text style={styles.title}>
                    {post.release.artists[0].name} - {post.release.title}
                  </Text>
                  <Text style={styles.section}>Edición</Text>
                  <View style={styles.separator} />
                  <View style={styles.info}>
                    <Text style={styles.textLabel}>Origen</Text>
                    <Text style={styles.text}>{post.release.country}</Text>
                  </View>
                  <View style={styles.info}>
                    <Text style={styles.textLabel}>Lanzamiento</Text>
                    <Text style={styles.text}>{post.release.year}</Text>
                  </View>
                  <View style={styles.info}>
                    <Text style={styles.textLabel}>Formato</Text>
                    <Text style={styles.text}>{getFormat(post.release)}</Text>
                  </View>
                  <Text style={styles.section}>Estado</Text>
                  <View style={styles.separator} />
                  <View style={styles.info}>
                    <Text style={styles.textLabel}>Estado</Text>
                    <Text style={styles.text}>{post.release.mediaState}</Text>
                  </View>
                  <View style={styles.info}>
                    <Text style={styles.textLabel}>Estado soporte</Text>
                    <Text style={styles.text}>{post.release.supportState}</Text>
                  </View>
                  {!!post.notes && (
                    <>
                      <Text style={styles.section}>Comentarios</Text>
                      <View style={styles.separator} />
                      <View style={styles.info}>
                        <Text style={styles.text}>{post.notes}</Text>
                      </View>
                    </>
                  )}
                  <TouchableOpacity
                    style={styles.seeDetail}
                    onPress={() =>
                      NavigationService.navigate(SCREEN_NAMES.RELEASE_DETAIL_SCREEN, {
                        releaseId: post.release.discogsId,
                      })
                    }
                  >
                    <View style={styles.separator} />
                    <View style={styles.seeDetailContainer}>
                      <Text style={styles.text}>Ver detalle de la edición</Text>
                      <Text style={styles.fowardIcon}>{'>'}</Text>
                    </View>
                    <View style={styles.separator} />
                  </TouchableOpacity>
                </View>
                <ImageSlider ownImages={post.release.ownImages} images={post.release.images} />
                <CardInfo post={post} />
                <View style={styles.commentsContainer}>
                  <Text style={styles.commentsTile}>{`${post.comments.length} comentarios`}</Text>
                  {post.comments.map((comment) => (
                    <Comment key={comment._id} comment={comment} />
                  ))}
                </View>
              </Content>
              <KeyboardAvoidingView enabled behavior="padding" keyboardVerticalOffset={87}>
                <Item style={styles.item}>
                  <TextInput
                    ref={inputRef}
                    style={styles.input}
                    value={message}
                    onChangeText={setMessage}
                    placeholder="Escribe un comentario"
                  />
                  <TouchableOpacity onPress={createComment} disabled={!message}>
                    <Icon
                      type="MaterialIcons"
                      name="send"
                      style={message ? styles.sendIconActive : styles.sendIcon}
                    />
                  </TouchableOpacity>
                </Item>
              </KeyboardAvoidingView>
            </>
          )}
        </>
      )}
    </Container>
  )
}

export { PostDetail }
