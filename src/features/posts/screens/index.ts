import { SCREEN_NAMES } from 'consts'

import { PostDetail } from './PostDetail'
import { Posts } from './Posts'

export const Screens = {
  Posts: {
    name: SCREEN_NAMES.POSTS_SCREEN,
    component: Posts,
    options: { title: 'Publicaciones' },
  },
  PostDetail: {
    name: SCREEN_NAMES.POST_DETAIL_SCREEN,
    component: PostDetail,
    options: { title: '' },
  },
}
