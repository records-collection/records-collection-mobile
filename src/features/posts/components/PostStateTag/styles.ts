import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  container: {
    padding: 5,
    borderRadius: 5,
    backgroundColor: '#dbead5',
  },
  text: {
    color: '$textColor',
    fontSize: '1rem',
  },
})

export { styles }
