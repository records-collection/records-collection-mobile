import React from 'react'
import { View, Text } from 'react-native'

import { PostStateEnum } from 'types'

import { styles } from './styles'

interface Props {
  state: PostStateEnum
}

const States: { [state: string]: { label: string; color: string } } = {
  Available: { label: 'Disponible', color: '#dbead5' },
  Sold: { label: 'Vendido', color: '#ed9d90' },
  Reserved: { label: 'Reservado', color: '#ffd191' },
}

const PostStateTag: React.FC<Props> = ({ state }) => {
  return (
    <View style={[styles.container, { backgroundColor: States[state].color }]}>
      <Text style={styles.text}>{States[state].label}</Text>
    </View>
  )
}

export { PostStateTag }
