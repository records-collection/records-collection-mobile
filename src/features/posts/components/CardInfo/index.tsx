import React, { useContext, useState } from 'react'
import { CardItem, View, Text, Button, Spinner, Icon } from 'native-base'
import { TouchableOpacity } from 'react-native'

import { Post, PostStateEnum } from 'types'
import { Layouts } from 'styles'
import { UserContext } from 'contexts'
import { NavigationService } from 'services'
import { SCREEN_NAMES } from 'consts'
import { useLikePost, useReservePost, useSavePost } from 'hooks'

import { PostStateTag } from '../PostStateTag'
import { styles } from './styles'

interface CardInfoProps {
  post: Post
  isCard?: boolean
}

const CardInfo: React.FC<CardInfoProps> = ({ post, isCard }) => {
  const { user } = useContext(UserContext)

  const [isLiked, setIsLiked] = useState(post.likes.includes(user!.id))
  const [likes, setLikes] = useState(post.likes.length)
  const [isSaved, setIsSaved] = useState(user?.postsSaved.includes(post._id))

  const { reserve, isLoading } = useReservePost()

  const { like, unlike } = useLikePost()

  const { save, remove } = useSavePost()

  const reservePost = () => reserve(post._id)

  const navigateToPostDetail = (focusInput?: boolean) => {
    NavigationService.navigate(SCREEN_NAMES.POST_DETAIL_SCREEN, { postId: post._id, focusInput })
  }

  const likePost = () => {
    if (isLiked) {
      unlike(post._id)
      setIsLiked(false)
      setLikes(likes - 1)
    } else {
      like(post._id)
      setIsLiked(true)
      setLikes(likes + 1)
    }
  }

  const savePost = () => {
    if (isSaved) {
      remove(post._id)
      setIsSaved(false)
    } else {
      save(post._id)
      setIsSaved(true)
    }
  }

  return (
    <>
      <CardItem style={[styles.cardItem, styles.info]}>
        <View style={styles.priceContainer}>
          <Text style={styles.priceText}>{`$${post.price}`}</Text>
        </View>
        <PostStateTag state={post.state} />
        {post.user._id !== user?.id && post.state === PostStateEnum.Available && (
          <Button small bordered onPress={reservePost} style={styles.button}>
            {isLoading ? <Spinner color="blue" size="small" /> : <Text>Reservar</Text>}
          </Button>
        )}
      </CardItem>
      <CardItem style={[styles.cardItem, styles.info]}>
        <Text style={styles.text}>{`${likes} likes`}</Text>
        {isCard && (
          <TouchableOpacity onPress={() => navigateToPostDetail()}>
            <Text style={[styles.text, styles.textUnderline]}>
              {`${post.comments.length} comentarios`}
            </Text>
          </TouchableOpacity>
        )}
      </CardItem>
      <CardItem style={[styles.cardItem, styles.footer]} footer>
        <View style={[Layouts.col, Layouts.center]}>
          <TouchableOpacity style={styles.iconContainer} onPress={likePost}>
            <Icon type="AntDesign" name={isLiked ? 'like1' : 'like2'} style={styles.icon} />
            <Text style={styles.text}>Like</Text>
          </TouchableOpacity>
        </View>
        {isCard && (
          <View style={[Layouts.col, Layouts.center]}>
            <TouchableOpacity
              onPress={() => navigateToPostDetail(true)}
              style={styles.iconContainer}
            >
              <Icon type="FontAwesome" name="comment-o" style={styles.icon} />
              <Text style={styles.text}>Comentar</Text>
            </TouchableOpacity>
          </View>
        )}
        <View style={[Layouts.row, Layouts.center]}>
          <TouchableOpacity style={styles.iconContainer} onPress={savePost}>
            <Icon
              type="FontAwesome"
              name={isSaved ? 'bookmark' : 'bookmark-o'}
              style={styles.icon}
            />
            <Text style={styles.text}>{isSaved ? 'Guardado' : 'Guardar'}</Text>
          </TouchableOpacity>
        </View>
      </CardItem>
    </>
  )
}

export { CardInfo }
