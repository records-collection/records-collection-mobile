import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  cardItem: {
    borderRadius: 0,
    backgroundColor: '$backgroundColor',
  },
  heart: {
    color: '#ED4A6A',
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  text: {
    color: '$textColor',
  },
  textUnderline: {
    textDecorationLine: 'underline',
  },
  info: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  icon: {
    color: '$textColor',
    fontSize: 25,
    marginRight: 5,
  },
  iconContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  priceContainer: {
    padding: 5,
    borderRadius: 5,
    backgroundColor: '$backgroundContrastColor',
  },
  priceText: {
    color: '$textColor',
    fontWeight: 'bold',
    fontSize: '1.2rem',
  },
  button: {
    width: 100,
    justifyContent: 'center',
  },
})

export { styles }
