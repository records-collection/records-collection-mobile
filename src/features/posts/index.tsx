import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import { SCREEN_NAMES } from 'consts'
import { Screens as SearchScreens } from 'features/search/screens'

import { Screens } from './screens'

const Stack = createStackNavigator()

const PostsStack: React.FC = () => (
  <Stack.Navigator
    initialRouteName={SCREEN_NAMES.POSTS_SCREEN}
    screenOptions={{ headerBackTitleVisible: false }}
  >
    <Stack.Screen {...Screens.Posts} />
    <Stack.Screen {...Screens.PostDetail} />
    <Stack.Screen {...SearchScreens.ReleaseDetail} />
  </Stack.Navigator>
)

export { PostsStack }
