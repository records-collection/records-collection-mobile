import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import { SCREEN_NAMES } from 'consts'

import { Screens } from './screens'

const Stack = createStackNavigator()

const ProfileStack: React.FC = () => (
  <Stack.Navigator
    initialRouteName={SCREEN_NAMES.PREFERENCES_SCREEN}
    screenOptions={{ headerBackTitleVisible: false }}
  >
    <Stack.Screen {...Screens.Preferences} />
    <Stack.Screen {...Screens.ChangePassword} />
    <Stack.Screen {...Screens.DarkModeConfig} />
    <Stack.Screen {...Screens.Inbox} />
    <Stack.Screen {...Screens.Chat} />
    <Stack.Screen {...Screens.SavedPosts} />
    <Stack.Screen {...Screens.MyPosts} />
  </Stack.Navigator>
)

export { ProfileStack }
