export interface ChangePasswordPayload {
  idUser: string
  newPassword: string
}
