import React, { useContext, useLayoutEffect, useState } from 'react'
import { Text, List, ListItem, Left, Right, Icon, Separator, View, Content } from 'native-base'
import EStyleSheet from 'react-native-extended-stylesheet'
import { useNavigation } from '@react-navigation/native'

import { SCREEN_NAMES } from 'consts'
import { useLogOut } from 'hooks'
import { NavigationService } from 'services'
import { ButtonHeader, ConfirmDialog, Container } from 'components'
import { AvatarIcon } from 'assets/icons'
import { UserContext } from 'contexts'

import { styles } from './styles'

const Preferences: React.FC = () => {
  const { setOptions } = useNavigation()
  const { user } = useContext(UserContext)

  const [logOut] = useLogOut()

  const [showLogoutConfirmation, setShowLogoutConfirmation] = useState(false)

  useLayoutEffect(() => {
    setOptions({
      headerRight: () => (
        <View style={styles.headerRight}>
          <ButtonHeader
            onPress={() => NavigationService.navigate(SCREEN_NAMES.INBOX_SCREEN)}
            icon="message-circle"
            typeIcon="Feather"
          />
        </View>
      ),
      title: '',
    })
  }, [setOptions])

  return (
    <Container>
      <Content>
        <View style={styles.userContainer}>
          <View style={styles.avatarContainer}>
            <AvatarIcon width={70} height={70} />
          </View>
          <View>
            <Text style={styles.textName}>{user?.username}</Text>
            <Text style={styles.text}>{user?.email}</Text>
          </View>
        </View>
        <List style={styles.list}>
          <Separator style={styles.separator} />
          <ListItem
            onPress={() => NavigationService.navigate(SCREEN_NAMES.MY_POSTS_SCREEN)}
            underlayColor={EStyleSheet.value('$backgroundContrastColor')}
          >
            <Left>
              <Text style={styles.textItem}>Mis publicaciones</Text>
            </Left>
            <Right>
              <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem
            onPress={() => NavigationService.navigate(SCREEN_NAMES.SAVED_POSTS_SCREEN)}
            underlayColor={EStyleSheet.value('$backgroundContrastColor')}
          >
            <Left>
              <Text style={styles.textItem}>Publicaciones guardadas</Text>
            </Left>
            <Right>
              <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <Separator style={styles.separator} />
          <ListItem
            onPress={() => NavigationService.navigate(SCREEN_NAMES.CHANGE_PASSWORD_SCREEN)}
            underlayColor={EStyleSheet.value('$backgroundContrastColor')}
          >
            <Left>
              <Text style={styles.textItem}>Cambiar contraseña</Text>
            </Left>
            <Right>
              <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem
            noBorder
            onPress={() => NavigationService.navigate(SCREEN_NAMES.DARK_MODE_CONFIG_SCREEN)}
            underlayColor={EStyleSheet.value('$backgroundContrastColor')}
          >
            <Left>
              <Text style={styles.textItem}>Tema Dark</Text>
            </Left>
            <Right>
              <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <Separator style={styles.separator} />
          <ListItem
            onPress={() => setShowLogoutConfirmation(true)}
            style={styles.signoutOption}
            underlayColor={EStyleSheet.value('$backgroundContrastColor')}
          >
            <Left>
              <Text style={styles.signoutOption}>Cerrar sesión</Text>
            </Left>
          </ListItem>
        </List>
      </Content>
      <ConfirmDialog
        visible={showLogoutConfirmation}
        onCancel={() => setShowLogoutConfirmation(false)}
        onConfirm={() => logOut()}
        title="Cerrar sesión"
      />
    </Container>
  )
}

export { Preferences }
