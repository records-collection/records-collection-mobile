import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  button: {
    margin: 10,
  },
  overlay: {
    width: 300,
    height: 200,
    textAlign: 'center',
  },
  overlayTitle: {
    textAlign: 'center',
    marginTop: 30,
    marginBottom: 50,
    fontSize: '1.5rem',
  },
  viewButtons: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  list: {
    width: '100%',
  },
  buttonCancel: {
    backgroundColor: 'gray',
  },
  signoutOption: {
    fontWeight: 'bold',
    color: '$textColor',
  },
  separator: {
    backgroundColor: '$backgroundContrastColor',
    height: 10,
  },
  textItem: {
    color: '$textColor',
  },
  userContainer: {
    width: '100%',
    marginVertical: 20,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  avatarContainer: {
    marginHorizontal: 10,
  },
  textName: {
    color: '$textColor',
    fontSize: '1.1rem',
    fontWeight: 'bold',
  },
  text: {
    color: '$textColor',
  },
  headerRight: {
    marginHorizontal: 10,
  },
})

export { styles }
