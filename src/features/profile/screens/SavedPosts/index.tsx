import React from 'react'
import { Button, Container, Content, Icon, Spinner, Text, View } from 'native-base'
import { Image } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'

import { useSavedPosts, useSavePost } from 'hooks'
import { NavigationService } from 'services'
import { SCREEN_NAMES } from 'consts'

import { styles } from './styles'

const SavedPosts: React.FC = () => {
  const { posts, isLoading } = useSavedPosts()

  const { remove } = useSavePost()

  const navigateToPostDetail = (postId: string) => {
    NavigationService.navigate(SCREEN_NAMES.POST_DETAIL_SCREEN, { postId })
  }

  return (
    <Container>
      <Content style={styles.content}>
        {isLoading && <Spinner color="gray" />}

        {!isLoading && !posts.length && (
          <>
            <Text style={styles.noResultsText}>No tienes publicaciones guardadas</Text>
            <Button
              bordered
              style={styles.button}
              onPress={() => NavigationService.navigate(SCREEN_NAMES.POSTS_SCREEN)}
            >
              <Text>Ver publicaciones</Text>
            </Button>
          </>
        )}

        {posts.map((post) => (
          <View key={post._id} style={styles.cardContainer}>
            <View style={styles.imageContainer}>
              <TouchableOpacity onPress={() => navigateToPostDetail(post._id)}>
                <Image style={styles.image} source={{ uri: post.release.images[0]?.uri }} />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigateToPostDetail(post._id)}>
                <Text style={styles.title}>
                  {post.release.title} - {post.release.artists[0]?.name}
                </Text>
              </TouchableOpacity>
            </View>

            <TouchableOpacity style={styles.iconContainer} onPress={() => remove(post._id)}>
              <Icon type="FontAwesome" name={'bookmark'} style={styles.icon} />
            </TouchableOpacity>
          </View>
        ))}
      </Content>
    </Container>
  )
}

export { SavedPosts }
