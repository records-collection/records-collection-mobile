import { SCREEN_NAMES } from 'consts'

import { Preferences } from './Preferences'
import { ChangePassword } from './ChangePassword'
import { DarkModeConfig } from './DarkModeConfig'
import { Inbox } from './Inbox'
import { Chat } from './Chat'
import { SavedPosts } from './SavedPosts'
import { MyPosts } from './MyPosts'

export const Screens = {
  Preferences: {
    name: SCREEN_NAMES.PREFERENCES_SCREEN,
    component: Preferences,
    options: { title: 'Preferencias' },
  },
  ChangePassword: {
    name: SCREEN_NAMES.CHANGE_PASSWORD_SCREEN,
    component: ChangePassword,
    options: { title: 'Cambiar contraseña' },
  },
  DarkModeConfig: {
    name: SCREEN_NAMES.DARK_MODE_CONFIG_SCREEN,
    component: DarkModeConfig,
    options: { title: 'Estilo app' },
  },
  Inbox: {
    name: SCREEN_NAMES.INBOX_SCREEN,
    component: Inbox,
    options: { title: 'Chats' },
  },
  Chat: {
    name: SCREEN_NAMES.CHAT_SCREEN,
    component: Chat,
    options: { title: '' },
  },
  SavedPosts: {
    name: SCREEN_NAMES.SAVED_POSTS_SCREEN,
    component: SavedPosts,
    options: { title: 'Publicaciones guardadas' },
  },
  MyPosts: {
    name: SCREEN_NAMES.MY_POSTS_SCREEN,
    component: MyPosts,
    options: { title: 'Mis publicaciones' },
  },
}
