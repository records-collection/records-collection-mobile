import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  viewContainer: { flex: 1 },
  horizontalMargin: {
    marginHorizontal: 20,
  },
  inboxContainer: {
    flexDirection: 'row',
    marginVertical: 15,
    justifyContent: 'space-between',
  },
  name: {
    fontWeight: 'bold',
    color: '$textColor',
    fontSize: '1.2rem',
  },
  time: {
    color: '$textColor',
    marginLeft: 25,
    marginTop: 5,
    fontSize: '0.7rem',
  },
  message: {
    color: 'gray',
  },
  previewChat: {
    flexDirection: 'row',
    width: '70%',
  },
  noResultsText: {
    marginTop: 30,
    textAlign: 'center',
    fontWeight: 'bold',
    color: '$textColor',
    fontSize: '1.3rem',
    marginBottom: 30,
  },
  noResultsContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
  },
  button: {
    backgroundColor: '$textColor',
    width: '80%',
    borderRadius: 10,
    alignSelf: 'center',
  },
  avatar: {
    marginRight: 20,
  },
})

export { styles }
