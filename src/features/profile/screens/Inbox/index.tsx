import React, { useContext, useState } from 'react'
import { Text, View, Input, Content, Item, Spinner } from 'native-base'
import { TouchableOpacity } from 'react-native'

import { NavigationService } from 'services'
import { SCREEN_NAMES } from 'consts'
import { useChats } from 'hooks'
import { AvatarIcon } from 'assets/icons'
import { UserContext } from 'contexts'
import { DateUtils } from 'utils'
import { Container } from 'components'

import { styles } from './styles'

const Inbox: React.FC = () => {
  const { user } = useContext(UserContext)
  const [name, setName] = useState('')

  const { chats, isLoading } = useChats()

  return (
    <Container>
      <Content style={styles.viewContainer}>
        <View style={styles.horizontalMargin}>
          <Item>
            <Input placeholder="Buscar" onChangeText={setName} value={name} />
          </Item>

          {isLoading && <Spinner color="gray" />}

          {!isLoading && !chats.length && (
            <View style={styles.noResultsContainer}>
              <Text style={styles.noResultsText}>No se encontraron chats</Text>
            </View>
          )}

          {chats.map((chat) => (
            <TouchableOpacity
              style={styles.inboxContainer}
              key={chat._id}
              onPress={() =>
                NavigationService.navigate(SCREEN_NAMES.CHAT_SCREEN, { chatId: chat._id })
              }
            >
              <View style={styles.previewChat}>
                <AvatarIcon width={65} height={65} style={styles.avatar} />
                <View>
                  <Text style={styles.name}>
                    {user?.id === chat.user1._id ? chat.user2.username : chat.user1.username}
                  </Text>
                  <Text style={styles.message}>{chat.messages[0]?.message}</Text>
                </View>
              </View>
              <Text style={styles.time}>
                {chat.messages[0]?.createdAt
                  ? DateUtils.formatOnlyHour(chat.messages[0].createdAt)
                  : ''}
              </Text>
            </TouchableOpacity>
          ))}
        </View>
      </Content>
    </Container>
  )
}

export { Inbox }
