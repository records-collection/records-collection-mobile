import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  chatContainer: {
    marginHorizontal: 20,
    flexGrow: 1,
    justifyContent: 'flex-start',
  },
  date: {
    fontWeight: 'bold',
    color: '$textColor',
    fontSize: '0.8rem',
  },
  dateContainer: {
    alignItems: 'center',
  },
})

export { styles }
