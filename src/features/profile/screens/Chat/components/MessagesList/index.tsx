import React from 'react'
import { View, Text } from 'native-base'

import { Chat, Message } from 'types'
import { DateUtils } from 'utils'

import { styles } from './styles'
import { MessageBox } from '../MessageBox'

interface MessagesListProps {
  chat: Chat
}

const MessagesList: React.FC<MessagesListProps> = ({ chat }) => {
  const messagesGroup = chat.messages.reduce((groups: { [date: string]: Message[] }, item) => {
    const date = DateUtils.formatOnlyDate(item.createdAt)
    return {
      ...groups,
      [date]: [...(groups[date] ?? []), item],
    }
  }, {})

  return (
    <View style={styles.chatContainer}>
      {Object.entries(messagesGroup).map(([date, messagesList]) => (
        <View key={date}>
          <View style={styles.dateContainer}>
            <Text style={styles.date}>{date}</Text>
          </View>
          {messagesList.map((mssg) => (
            <MessageBox key={mssg._id} message={mssg} />
          ))}
        </View>
      ))}
    </View>
  )
}

export { MessagesList }
