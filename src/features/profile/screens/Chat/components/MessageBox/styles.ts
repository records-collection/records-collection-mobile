import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  messagesContainer: {
    flexDirection: 'row',
    padding: 15,
    borderRadius: 10,
    marginVertical: 5,
    backgroundColor: '$backgroundContrastColor',
  },
  message: {
    color: '$textColor',
    width: '80%',
  },
  messageTime: {
    color: 'gray',
    marginLeft: 25,
    fontSize: '0.7rem',
  },
  grayColor: {
    color: 'gray',
  },
  ownMessage: {
    backgroundColor: '#D6EDFF',
    borderBottomRightRadius: 0,
  },
  otherMessage: {
    backgroundColor: '$backgroundContrastColor',
    borderTopLeftRadius: 0,
  },
  postContainer: {
    flex: 1,
    flexGrow: 1,
  },
  image: {
    width: '100%',
    height: 100,
  },
})

export { styles }
