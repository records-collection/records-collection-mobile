import React, { useContext } from 'react'
import { Text, View } from 'native-base'
import { Image } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'

import { Message } from 'types'
import { UserContext } from 'contexts'
import { DateUtils } from 'utils'
import { MESSAGE_TYPES, SCREEN_NAMES } from 'consts'
import { NavigationService } from 'services'

import { styles } from './styles'

interface MessageBoxProps {
  message: Message
}

const MessageBox: React.FC<MessageBoxProps> = ({ message }) => {
  const { user } = useContext(UserContext)

  const isOwnMessage = user?.id === message.user

  return (
    <View
      style={[styles.messagesContainer, isOwnMessage ? styles.ownMessage : styles.otherMessage]}
    >
      {message.type === MESSAGE_TYPES.MESSAGE && (
        <>
          <Text style={styles.message}>{message.message}</Text>
          <Text style={styles.messageTime}>{DateUtils.formatOnlyHour(message.createdAt)}</Text>
        </>
      )}
      {message.type === MESSAGE_TYPES.RESERVATION && (
        <View style={styles.postContainer}>
          {message.postPicture && (
            <TouchableOpacity
              onPress={() =>
                NavigationService.navigate(SCREEN_NAMES.POST_DETAIL_SCREEN, {
                  postId: message.postId,
                })
              }
            >
              <Image style={styles.image} source={{ uri: message.postPicture }} />
            </TouchableOpacity>
          )}
          <Text style={styles.message}>
            {user?.id === message.user
              ? 'Reservaste una publicación, ponete en contacto con el vendedor para coordinar los detalles'
              : 'Te reservaron una publicación, una vez que concretes la transacción podras marcar la publicación como Finalizada'}
          </Text>
        </View>
      )}
      {message.type === MESSAGE_TYPES.CANCELED && (
        <View style={styles.postContainer}>
          {message.postPicture && (
            <TouchableOpacity
              onPress={() =>
                NavigationService.navigate(SCREEN_NAMES.POST_DETAIL_SCREEN, {
                  postId: message.postId,
                })
              }
            >
              <Image style={styles.image} source={{ uri: message.postPicture }} />
            </TouchableOpacity>
          )}
          <Text style={styles.message}>
            {user?.id === message.user
              ? 'El vendedor canceló la reserva que hiciste'
              : 'Cancelaste la reserva de la publicación'}
          </Text>
        </View>
      )}
      {message.type === MESSAGE_TYPES.SOLD && (
        <View style={styles.postContainer}>
          {message.postPicture && (
            <TouchableOpacity
              onPress={() =>
                NavigationService.navigate(SCREEN_NAMES.POST_DETAIL_SCREEN, {
                  postId: message.postId,
                })
              }
            >
              <Image style={styles.image} source={{ uri: message.postPicture }} />
            </TouchableOpacity>
          )}
          <Text style={styles.message}>
            {user?.id === message.user
              ? 'Se concretó la venta de la publicación, se agregó el álbum a tu colección'
              : 'Se concretó la venta de la publicación, se eliminó el álbum de tu colección'}
          </Text>
        </View>
      )}
    </View>
  )
}

export { MessageBox }
