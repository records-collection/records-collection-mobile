import React, { useContext, useLayoutEffect, useRef, useState } from 'react'
import { Icon, Input, View, Text, Spinner, Item } from 'native-base'
import { KeyboardAvoidingView, ScrollView, TouchableOpacity } from 'react-native'
import { useNavigation } from '@react-navigation/native'
import { HeaderBackButton } from '@react-navigation/stack'

import { NavigationService } from 'services'
import { AvatarIcon } from 'assets/icons'
import { UserContext } from 'contexts'
import { useChat, useSendMessage } from 'hooks'
import { Container } from 'components'

import { styles } from './styles'
import { MessagesList } from './components'

interface ChatProps {
  route: { params: { chatId: string } }
}

const Chat: React.FC<ChatProps> = ({ route }) => {
  const { setOptions } = useNavigation()

  const { user } = useContext(UserContext)

  const { chatId } = route.params

  const scrollViewRef = useRef<ScrollView>(null)

  const [message, setMessage] = useState('')

  const { chat, isLoading } = useChat(chatId)
  const { sendMessage } = useSendMessage()

  const sendNewMessage = () => {
    sendMessage({ message, chatId })
    setMessage('')
  }

  useLayoutEffect(() => {
    setOptions({
      headerLeft: () => (
        <View style={styles.header}>
          <HeaderBackButton label=" " onPress={NavigationService.goBack} />
          <TouchableOpacity style={styles.headerTitleContent}>
            <AvatarIcon width={35} height={35} />
            <Text style={styles.textUsername}>
              {user?.id === chat?.user1._id ? chat?.user2.username : chat?.user1.username}
            </Text>
          </TouchableOpacity>
        </View>
      ),
    })
  }, [user, chat, setOptions])

  const scrollToEnd = () => scrollViewRef.current?.scrollToEnd()

  return (
    <Container style={styles.container}>
      {isLoading && <Spinner color="gray" />}
      {chat && (
        <ScrollView
          contentContainerStyle={styles.content}
          ref={scrollViewRef}
          onContentSizeChange={scrollToEnd}
        >
          <MessagesList chat={chat} />
        </ScrollView>
      )}
      <KeyboardAvoidingView enabled behavior="padding" keyboardVerticalOffset={87}>
        <Item style={styles.item}>
          <Input
            style={styles.inputMessage}
            value={message}
            onChangeText={setMessage}
            placeholder="Escribe un mensaje"
          />
          <TouchableOpacity onPress={sendNewMessage} disabled={!message}>
            <Icon
              type="MaterialIcons"
              name="send"
              style={message ? styles.sendIconActive : styles.sendIcon}
            />
          </TouchableOpacity>
        </Item>
      </KeyboardAvoidingView>
    </Container>
  )
}

export { Chat }
