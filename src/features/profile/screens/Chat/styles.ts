import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  conatiner: {
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  content: {
    flexGrow: 1,
    paddingVertical: 10,
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 20,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
  },
  headerTitleContent: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 20,
  },
  textUsername: {
    color: '$textColor',
    textDecorationLine: 'underline',
    fontWeight: '500',
    marginLeft: 8,
  },
  item: {
    borderColor: '$backgroundContrastColor',
    borderTopWidth: 1,
  },
  inputMessage: {
    color: '$textColor',
    backgroundColor: '$backgroundContrastColor',
    borderRadius: 30,
    height: 40,
    margin: 10,
    paddingLeft: 20,
    paddingRight: 20,
    width: '80%',
  },
  sendIcon: {
    color: '$backgroundContrastColor',
    fontSize: 35,
  },
  sendIconActive: {
    color: '$textColor',
    fontSize: 35,
  },
})

export { styles }
