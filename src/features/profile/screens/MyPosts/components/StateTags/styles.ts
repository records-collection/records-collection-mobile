import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  container: {
    marginTop: 15,
    marginBottom: 5,
    marginHorizontal: 10,
  },
  tag: {
    backgroundColor: 'gray',
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 20,
    marginRight: 10,
  },
  tagActive: {
    backgroundColor: 'green',
  },
  text: {
    color: 'white',
  },
})

export { styles }
