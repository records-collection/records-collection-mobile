import React, { useState } from 'react'
import { View, Text, ScrollView } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'

import { styles } from './styles'

const states = [
  {
    label: 'Todas',
    state: '',
  },
  {
    label: 'Disponibles',
    state: 'Available',
  },
  {
    label: 'Reservados',
    state: 'Reserved',
  },
  {
    label: 'Vendidos',
    state: 'Sold',
  },
]

interface StateTagsProps {
  onChange: (state: string) => void
}

const StateTags: React.FC<StateTagsProps> = ({ onChange }) => {
  const [stateSelected, setStateSelected] = useState('')

  const handleSelectState = (state: string) => {
    setStateSelected(state)
    onChange(state)
  }

  return (
    <View style={styles.container}>
      <ScrollView horizontal showsHorizontalScrollIndicator={false}>
        {states.map((state) => (
          <TouchableOpacity onPress={() => handleSelectState(state.state)}>
            <View
              key={state.label}
              style={[styles.tag, stateSelected === state.state && styles.tagActive]}
            >
              <Text style={styles.text}>{state.label}</Text>
            </View>
          </TouchableOpacity>
        ))}
      </ScrollView>
    </View>
  )
}

export { StateTags }
