import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  content: {
    margin: 10,
  },
  cardContainer: {
    flex: 1,
    flexGrow: 1,
    padding: 10,
    backgroundColor: '$backgroundContrastColor',
    marginBottom: 15,
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
  },
  imageContainer: {
    flex: 1,
    flexGrow: 1,
  },
  image: {
    width: '100%',
    height: 100,
    borderRadius: 10,
    marginRight: 20,
  },
  title: {
    color: '$textColor',
    fontSize: '1.1rem',
    fontWeight: '500',
    marginTop: 3,
  },
  icon: {
    color: 'gray',
  },

  noResultsText: {
    color: '$textColor',
    textAlign: 'center',
    marginTop: 30,
    fontSize: '1.2rem',
    fontWeight: '500',
  },
  button: {
    alignSelf: 'center',
    marginTop: 20,
  },
})

export { styles }
