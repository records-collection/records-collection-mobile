import React, { useState } from 'react'
import { Container, Content, Spinner, Text, View } from 'native-base'
import { Image } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'

import { useMyPosts } from 'hooks'
import { NavigationService } from 'services'
import { SCREEN_NAMES } from 'consts'
import { PostStateTag } from 'features/posts/components/PostStateTag'
import { Layouts } from 'styles'

import { styles } from './styles'
import { StateTags } from './components'

const MyPosts: React.FC = () => {
  const [state, setState] = useState('')

  const { posts, isLoading } = useMyPosts(state)

  const navigateToPostDetail = (postId: string) => {
    NavigationService.navigate(SCREEN_NAMES.POST_DETAIL_SCREEN, { postId })
  }

  return (
    <Container>
      <StateTags onChange={setState} />
      <Content style={styles.content}>
        {isLoading && <Spinner color="gray" />}
        {!isLoading && !posts.length && (
          <Text style={styles.noResultsText}>No hay publicaciones</Text>
        )}

        {posts.map((post) => (
          <View key={post._id} style={styles.cardContainer}>
            <View style={styles.imageContainer}>
              <TouchableOpacity onPress={() => navigateToPostDetail(post._id)}>
                <Image style={styles.image} source={{ uri: post.release.images[0]?.uri }} />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigateToPostDetail(post._id)}>
                <Text style={styles.title}>
                  {post.release.title} - {post.release.artists[0]?.name}
                </Text>
              </TouchableOpacity>
              <View style={Layouts.row}>
                <PostStateTag state={post.state} />
              </View>
            </View>
          </View>
        ))}
      </Content>
    </Container>
  )
}

export { MyPosts }
