import React, { useContext, useState } from 'react'
import { useMutation } from 'react-query'

import { LoginInput } from 'features/login/components/LoginInput'
import { ButtonLogin } from 'features/login/components/ButtonLogin'
import { Container, Toast } from 'components'
import { UserContext } from 'contexts'

import { styles } from './styles'
import { PreferencesApi } from '../../api'

const ChangePassword: React.FC = () => {
  const [confirmPassword, setConfirmPassword] = useState('')
  const [password, setPassword] = useState('')

  const { user } = useContext(UserContext)

  const [changePassword, { isLoading }] = useMutation(PreferencesApi.changePassword, {
    onSuccess: () => {
      Toast.show({
        text: 'Contraseña actualizada',
        type: 'success',
      })
      setPassword('')
      setConfirmPassword('')
    },
    onError: () => {
      Toast.show({
        text: 'Error actualizando contraseña',
        type: 'danger',
      })
    },
  })

  const handleChangePassword = () => {
    if (!password || !confirmPassword) {
      Toast.show({
        text: 'Ingrese la nueva contraseña',
      })
    } else if (password !== confirmPassword) {
      Toast.show({
        text: 'Las contraseñas no coinciden',
      })
    } else if (user?.id) {
      const payload = { idUser: user?.id, newPassword: password }
      changePassword(payload)
    }
  }

  return (
    <Container style={styles.container}>
      <LoginInput
        placeholder="Nueva contraseña"
        onChangeText={setPassword}
        secureTextEntry
        value={password}
      />
      <LoginInput
        placeholder="Confirmar nueva contraseña"
        onChangeText={setConfirmPassword}
        value={confirmPassword}
        secureTextEntry
      />
      <ButtonLogin
        label="Guardar"
        isLoading={isLoading}
        onPress={handleChangePassword}
        style={styles.button}
      />
    </Container>
  )
}

export { ChangePassword }
