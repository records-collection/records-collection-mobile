import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 40,
  },
  button: {
    backgroundColor: 'gray',
  },
  title: {
    fontSize: 25,
    marginBottom: 30,
  },
})

export { styles }
