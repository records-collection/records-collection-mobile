import EStyleSheet from 'react-native-extended-stylesheet'

import { COLORS } from 'styles'

const styles = EStyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  item: {
    width: '90%',
    marginBottom: 30,
    flexDirection: 'row',
    paddingBottom: 10,
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderColor: COLORS.PRIMARY,
  },
  itemTouchable: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
  },
  checkBox: {
    marginRight: 10,
    color: '$textColor',
  },
  list: {
    width: '100%',
  },
  listItem: {
    minHeight: 60,
  },
  noMarginLeft: {
    marginLeft: 0,
  },
  textActive: {
    fontWeight: 'bold',
  },
  textItem: {
    color: '$textColor',
  },
  textNotes: {
    color: '$textColor',
  },
})

export { styles }
