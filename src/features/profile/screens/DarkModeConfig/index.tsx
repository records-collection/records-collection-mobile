import React, { useContext } from 'react'
import { CheckBox, Text, List, ListItem, Left, Right, Body } from 'native-base'
import EStyleSheet from 'react-native-extended-stylesheet'

import { SCHEMES } from 'consts'
import { ThemeContext } from 'contexts'
import { Container } from 'components'

import { styles } from './styles'

const DarkModeConfig: React.FC = () => {
  const { scheme, setScheme } = useContext(ThemeContext)

  return (
    <Container style={styles.container}>
      <List style={styles.list}>
        <ListItem
          style={styles.listItem}
          onPress={() => setScheme(SCHEMES.DARK)}
          underlayColor={EStyleSheet.value('$backgroundContrastColor')}
        >
          <Left>
            <Text style={[styles.textItem, scheme === SCHEMES.DARK && styles.textActive]}>
              Activado
            </Text>
          </Left>
          <Right>
            <CheckBox
              checked={scheme === SCHEMES.DARK}
              color={EStyleSheet.value('$textColor')}
              onPress={() => setScheme(SCHEMES.DARK)}
              style={styles.checkBox}
            />
          </Right>
        </ListItem>

        <ListItem
          style={styles.listItem}
          onPress={() => setScheme(SCHEMES.LIGHT)}
          underlayColor={EStyleSheet.value('$backgroundContrastColor')}
        >
          <Left>
            <Text style={[styles.textItem, scheme === SCHEMES.LIGHT && styles.textActive]}>
              Desactivado
            </Text>
          </Left>
          <Right>
            <CheckBox
              checked={scheme === SCHEMES.LIGHT}
              color={EStyleSheet.value('$textColor')}
              onPress={() => setScheme(SCHEMES.LIGHT)}
              style={styles.checkBox}
            />
          </Right>
        </ListItem>
        <ListItem
          style={styles.listItem}
          onPress={() => setScheme(SCHEMES.SYSTEM)}
          underlayColor={EStyleSheet.value('$backgroundContrastColor')}
        >
          <Body>
            <Text
              style={[
                styles.textItem,
                styles.noMarginLeft,
                scheme === SCHEMES.SYSTEM && styles.textActive,
              ]}
            >
              Automático
            </Text>
            <Text note style={[styles.noMarginLeft, styles.textNotes]}>
              Se ajusta la apariencia de acuerdo a la configuración del sistema.
            </Text>
          </Body>
          <Right>
            <CheckBox
              checked={scheme === SCHEMES.SYSTEM}
              color={EStyleSheet.value('$textColor')}
              onPress={() => setScheme(SCHEMES.SYSTEM)}
              style={styles.checkBox}
            />
          </Right>
        </ListItem>
      </List>
    </Container>
  )
}

export { DarkModeConfig }
