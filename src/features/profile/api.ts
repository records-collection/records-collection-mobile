import { api } from 'utils'

import { ChangePasswordPayload } from './types'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const changePassword = async (payload: ChangePasswordPayload): Promise<any> => {
  const response = await api.post('/auth/changePassword', payload)

  return response.data
}

export const PreferencesApi = { changePassword }
