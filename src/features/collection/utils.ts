import { ReleaseCollection } from 'types'

import { Company } from './types'

export const getSerie = (release: ReleaseCollection) => {
  if (release?.series?.length > 0) {
    const serie = release.series[0]
    const { name, catno } = serie
    return `${name} - ${catno}`
  }
  return '-'
}

export const getLabel = (release: ReleaseCollection) => {
  if (release?.label?.length > 0) {
    return release.label[0]
  }
  return '-'
}

export const getFormat = (release: ReleaseCollection) => {
  if (release?.formats?.length > 0) {
    const format = release.formats[0]
    const { name, descriptions } = format
    if (descriptions) {
      return [name, ...descriptions].join(', ')
    } else {
      return name
    }
  }
  return '-'
}

export const getArtist = (release: ReleaseCollection) => {
  return release?.title?.split('-')[0].trim()
}

export const getTitle = (release: ReleaseCollection) => {
  return release?.title?.split('-')[1].trim()
}

export const getGenres = (release: ReleaseCollection) => {
  return release?.genres?.join(', ') || '-'
}

export const getStyles = (release: ReleaseCollection) => {
  return release?.styles?.join(', ') || '-'
}

export const getCompany = (company: Company) => {
  if (company) {
    return `${company.entity_type_name}: ${company.name}`
  } else {
    return ''
  }
}

export const getImageUri = (release: ReleaseCollection) => {
  if (release.ownImage) return release.ownImage.uri
  if (release.images.length > 0) {
    return release.images[0].uri
  }
}

export const getArtistName = (release: ReleaseCollection) => {
  if (release.artists.length > 0) {
    return release.artists[0].name
  }
}
