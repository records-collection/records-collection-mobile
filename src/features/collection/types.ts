import { RouteProp } from '@react-navigation/native'

import { ReleaseCollection } from 'types'

export interface DeleteReleasePayload {
  idCollection: string
  idRelease: string
}

export interface Format {
  name: string
  descriptions?: string[]
}

export interface AdditionalData {
  personalNotes?: string
  mediaState?: string
  supportState?: string
}

export interface Company {
  name: string
  entity_type_name: string
  catno: string
}

export interface Serie {
  id: number
  name: string
  catno: string
}

type ParamList = {
  ReleaseCollectionDetail: {
    releaseCollection: ReleaseCollection
  }
}

export type ReleaseCollectionDetailRoute = RouteProp<ParamList, 'ReleaseCollectionDetail'>
