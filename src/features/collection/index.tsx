import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import { Screens } from './screens'

const Stack = createStackNavigator()

const CollectionStack: React.FC = () => (
  <Stack.Navigator screenOptions={{ headerBackTitleVisible: false }}>
    <Stack.Screen {...Screens.Collection} />
    <Stack.Screen {...Screens.ReleaseCollectionDetail} />
    <Stack.Screen {...Screens.WishList} />
    <Stack.Screen {...Screens.ReleaseWanted} />
  </Stack.Navigator>
)

export { CollectionStack }
