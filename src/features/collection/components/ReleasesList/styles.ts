import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  itemInput: {
    paddingHorizontal: '1rem',
    backgroundColor: '$backgroundColor',
  },
  listItem: {
    borderBottomWidth: 1,
    borderBottomColor: '#e5e5e5',
    paddingVertical: 10,
    marginLeft: '1rem',
    marginRight: '1rem',
    backgroundColor: '$backgroundColor',
  },
  thumbnail: {
    width: 100,
    height: 100,
  },
  body: {
    borderBottomWidth: 0,
  },
  title: {
    fontWeight: 'bold',
    fontSize: '1.1rem',
    color: '$textColor',
  },
  info: {
    fontWeight: 'normal',
    fontSize: '1rem',
    color: 'gray',
  },
  headerView: {
    marginRight: 10,
  },
})

export { styles }
