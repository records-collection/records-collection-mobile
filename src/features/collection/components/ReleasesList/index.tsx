import React from 'react'
import { Body, Left, List, ListItem, Spinner, Thumbnail, Text } from 'native-base'
import EStyleSheet from 'react-native-extended-stylesheet'

import { ReleaseCollection } from 'types'

import { styles } from './styles'
import { getImageUri, getArtistName, getFormat } from '../../utils'

interface ReleasesListProps {
  isLoading?: boolean
  releases: ReleaseCollection[]
  onPressRow: (release: ReleaseCollection) => void
}

const ReleasesList: React.FC<ReleasesListProps> = ({ isLoading, releases, onPressRow }) => {
  return (
    <>
      {isLoading && <Spinner color="gray" />}
      <List>
        {releases.map((release) => (
          <ListItem
            key={release._id}
            thumbnail
            style={styles.listItem}
            onPress={() => onPressRow(release)}
            underlayColor={EStyleSheet.value('$backgroundColor')}
          >
            <Left>
              <Thumbnail square source={{ uri: getImageUri(release) }} style={styles.thumbnail} />
            </Left>
            <Body style={styles.body}>
              <Text style={styles.title}>{release.title}</Text>
              <Text style={styles.info}>{getArtistName(release)}</Text>
              <Text style={styles.info}>
                {release.year} - {release.country}
              </Text>
              <Text style={styles.info}>{getFormat(release)}</Text>
            </Body>
          </ListItem>
        ))}
      </List>
    </>
  )
}

export { ReleasesList }
