import React from 'react'
import { Content } from 'native-base'

import { SCREEN_NAMES } from 'consts'
import { NavigationService } from 'services'
import { ReleasesList } from 'features/collection/components'
import { Container, InputSearch } from 'components'
import { useWishList, useDebounceText } from 'hooks'
import { ReleaseCollection } from 'types'

const WishListScreen: React.FC = () => {
  const [, search, setSearch] = useDebounceText('')

  const { releases, isLoadingWishList } = useWishList({
    search,
  })

  const navigateToDetail = (release: ReleaseCollection) => {
    if (release) {
      NavigationService.navigate(SCREEN_NAMES.RELEASE_WANTED_DETAIL_SCREEN, {
        releaseId: release.discogsId,
      })
    }
  }

  return (
    <Container>
      <InputSearch placeholder="Artista, álbum, etc..." onChangeText={setSearch} />
      <Content>
        {releases && (
          <ReleasesList
            releases={releases}
            isLoading={isLoadingWishList}
            onPressRow={navigateToDetail}
          />
        )}
      </Content>
    </Container>
  )
}

export { WishListScreen }
