import React, { useContext, useLayoutEffect, useState } from 'react'
import { useNavigation, RouteProp } from '@react-navigation/native'
import { View, Content, Spinner, Text } from 'native-base'

import { SCREEN_NAMES } from 'consts'
import { Accordion, ButtonHeader, ConfirmDialog, Container, ImageSlider } from 'components'
import { NavigationService } from 'services'
import { UserContext } from 'contexts'
import { useWishList, useRelease } from 'hooks'

import { styles } from './styles'
import {
  getCompany,
  getFormat,
  getGenres,
  getLabel,
  getSerie,
  getStyles,
} from '../../../search/utils'

type ParamList = {
  ReleaseDetail: {
    releaseId: number
  }
}

type ReleaseWantedDetailRoute = RouteProp<ParamList, 'ReleaseDetail'>

interface ReleaseWantedDetailProps {
  route: ReleaseWantedDetailRoute
}

const ReleaseWantedDetail: React.FC<ReleaseWantedDetailProps> = ({ route }) => {
  const { releaseId } = route.params

  const { user } = useContext(UserContext)

  const { setOptions } = useNavigation()

  const [isConfirmDeletionDialogVisible, setIsConfirmDeletionDialogVisible] = useState(false)

  const { release, isLoading } = useRelease(releaseId)

  const { removeRelease } = useWishList({
    onSuccessRemove: () => {
      setIsConfirmDeletionDialogVisible(false)
      NavigationService.goBack()
    },
  })

  useLayoutEffect(() => {
    setOptions({
      headerRight: () => (
        <View style={styles.headerRightView}>
          <ButtonHeader
            icon="plus"
            onPress={() =>
              NavigationService.navigate(SCREEN_NAMES.ADD_OR_EDIT_RELEASE_SCREEN, { release })
            }
          />
          <ButtonHeader icon="trash" onPress={() => setIsConfirmDeletionDialogVisible(true)} />
        </View>
      ),
    })
  }, [setOptions, release])

  const ItemValue = ({ label, value }: { label: string; value: string }) => (
    <View style={styles.itemValue}>
      <Text style={styles.label}>{label}</Text>
      <Text style={styles.value}>{value}</Text>
    </View>
  )

  const deleteReleaseFromWishList = () => {
    if (user?.recordCollection) {
      removeRelease({ collectionId: user.recordCollection, releaseDiscogsId: releaseId })
    }
  }

  return (
    <Container>
      <Content>
        {release && (
          <>
            <ImageSlider images={release.images ?? []} />
            <View>
              <Text style={styles.artistName}>{release.artists[0].name}</Text>
              <Text style={styles.title}>{release.title}</Text>
            </View>
            <View style={styles.containerInfo}>
              <ItemValue label="Sello:" value={getLabel(release)} />
              <ItemValue label="Serie:" value={getSerie(release)} />
              <ItemValue label="Formato:" value={getFormat(release)} />
              <ItemValue label="Origen:" value={release.country} />
              <ItemValue
                label="Publicado:"
                value={release.released_formatted || release.year.toString() || release.released}
              />
              <ItemValue label="Género:" value={getGenres(release)} />
              <ItemValue label="Estilo:" value={getStyles(release)} />
            </View>
            {release.companies?.length > 0 && (
              <Accordion
                containerStyle={styles.accordion}
                title="Compañías"
                contentComponent={
                  <View>
                    {release.companies.map((comp) => (
                      <Text key={getCompany(comp)} style={styles.value}>
                        {getCompany(comp)}
                      </Text>
                    ))}
                  </View>
                }
              />
            )}
            {release.notes && (
              <Accordion
                containerStyle={styles.accordion}
                title="Notas"
                contentText={release.notes}
              />
            )}
          </>
        )}
        {!releaseId || (isLoading && <Spinner color="gray" />)}
      </Content>
      <ConfirmDialog
        visible={isConfirmDeletionDialogVisible}
        onCancel={() => setIsConfirmDeletionDialogVisible(false)}
        onConfirm={deleteReleaseFromWishList}
        title={'Eliminar de la lista de deseados'}
      />
    </Container>
  )
}

export { ReleaseWantedDetail }
