import { SCREEN_NAMES } from 'consts'

import { Collection } from './Collection'
import { ReleaseCollectionDetail } from './ReleaseCollectionDetail'
import { WishListScreen } from './WishListScreen'
import { ReleaseWantedDetail } from './ReleaseWantedDetail'

export const Screens = {
  Collection: {
    name: SCREEN_NAMES.COLLECTION_SCREEN,
    component: Collection,
    options: { title: 'Colección' },
  },
  ReleaseCollectionDetail: {
    name: SCREEN_NAMES.RELEASE_COLLECTION_SCREEN,
    component: ReleaseCollectionDetail,
    options: { title: '' },
  },
  WishList: {
    name: SCREEN_NAMES.WISH_LIST_SCREEN,
    component: WishListScreen,
    options: { title: 'Mis deseados' },
  },
  ReleaseWanted: {
    name: SCREEN_NAMES.RELEASE_WANTED_DETAIL_SCREEN,
    component: ReleaseWantedDetail,
    options: { title: '' },
  },
}
