import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  headerView: {
    flexDirection: 'row',
  },
  overlay: {
    width: 300,
    height: 200,
    textAlign: 'center',
  },
  viewButtons: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  overlayTitle: {
    textAlign: 'center',
    marginTop: 30,
    marginBottom: 50,
  },
  button: {
    width: 100,
  },
  image: {
    resizeMode: 'center',
    height: 200,
  },
  carouselContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  artistName: {
    fontWeight: 'bold',
    fontSize: 20,
    marginTop: 10,
    marginBottom: 5,
    textAlign: 'center',
    color: '$textColor',
  },
  title: {
    fontSize: 18,
    textAlign: 'center',
    color: '$textColor',
  },
  label: {
    fontSize: 16,
    textAlign: 'left',
    fontWeight: 'bold',
    marginRight: 10,
    color: '$textColor',
  },
  value: {
    fontSize: 16,
    textAlign: 'left',
    color: '$textColor',
  },
  containerInfo: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    padding: 30,
  },
  extraInfoView: {
    paddingHorizontal: 30,
    paddingBottom: 30,
  },
  bottomSheetContent: {
    backgroundColor: '#dfdede',
    padding: 16,
    height: '100%',

    flexGrow: 1,
    flexDirection: 'column',
  },
  buttonAddToCollection: {
    width: '80%',
    marginVertical: 20,
    alignSelf: 'center',
    backgroundColor: '#2ecc71',
    height: 50,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 7,
  },
  textArea: {
    backgroundColor: '#fff',
    borderRadius: 10,
  },
  itemValue: {
    flexDirection: 'row',
  },
})

export { styles }
