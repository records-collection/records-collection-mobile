import React, { useLayoutEffect, useState } from 'react'
import { useNavigation, useRoute } from '@react-navigation/native'
import { Content, Spinner, Text, View } from 'native-base'

import { SCREEN_NAMES } from 'consts'
import { ButtonHeader, ConfirmDialog, Container, ImageSlider } from 'components'
import { NavigationService } from 'services'
import { useReleaseCollection, useCollection } from 'hooks'

import { styles } from './styles'
import { ReleaseCollectionDetailRoute } from '../../types'
import { getFormat, getGenres, getLabel, getSerie } from '../../utils'

const ReleaseCollectionDetail: React.FC = () => {
  const { setOptions } = useNavigation()

  const {
    params: { releaseCollection },
  } = useRoute<ReleaseCollectionDetailRoute>()

  const [showDeleteConfirmation, setShowDeleteConfirmation] = useState(false)

  const { release, isLoadingRelease } = useReleaseCollection(releaseCollection._id)

  const { removeRelease } = useCollection()

  const removeReleaseAction = () => {
    if (release?._id) {
      removeRelease(release?._id)
    }
  }

  useLayoutEffect(() => {
    setOptions({
      headerRight: () => (
        <View style={styles.headerView}>
          <ButtonHeader
            icon="edit"
            onPress={() =>
              NavigationService.navigate(SCREEN_NAMES.ADD_OR_EDIT_RELEASE_SCREEN, {
                isUpdate: true,
                releaseCollection: release,
              })
            }
          />
          <ButtonHeader
            icon="store"
            typeIcon="MaterialCommunityIcons"
            onPress={() =>
              NavigationService.navigate(SCREEN_NAMES.CREATE_POST_SCREEN, {
                releaseCollection: release,
              })
            }
          />
          <ButtonHeader icon="trash" onPress={() => setShowDeleteConfirmation(true)} />
        </View>
      ),
    })
  }, [setOptions, release])

  const ItemValue = ({ label, value }: { label: string; value: string }) => (
    <View style={styles.itemValue}>
      <Text style={styles.label}>{label}</Text>
      <Text style={styles.value}>{value}</Text>
    </View>
  )

  const renderRelease = () => (
    <>
      <ImageSlider ownImages={release?.ownImages} images={release?.images ?? []} />
      <View>
        <Text style={styles.artistName}>{release!.artists[0].name}</Text>
        <Text style={styles.title}>{release!.title}</Text>
      </View>
      <View style={styles.containerInfo}>
        <ItemValue label="Sello:" value={getLabel(release!)} />
        <ItemValue label="Serie:" value={getSerie(release!)} />
        <ItemValue label="Formato:" value={getFormat(release!)} />
        <ItemValue label="Origen:" value={release!.country} />
        <ItemValue label="Publicado:" value={release!.year.toString()!} />
        <ItemValue label="Género:" value={getGenres(release!)} />
        <ItemValue label="Estado del soporte:" value={release!.supportState ?? '-'} />
        <ItemValue label="Estado del disco:" value={release!.mediaState ?? '-'} />
        <ItemValue label="Notas:" value={release!.personalNotes ?? '-'} />
      </View>
      <View style={styles.containerInfo} />
    </>
  )

  return (
    <>
      <Container>
        <Content>
          {isLoadingRelease && <Spinner color="gray" />}
          {!!release && renderRelease()}
        </Content>
      </Container>
      <ConfirmDialog
        visible={showDeleteConfirmation}
        onCancel={() => setShowDeleteConfirmation(false)}
        onConfirm={() => removeReleaseAction()}
        title={'Eliminar de la colección'}
      />
    </>
  )
}

export { ReleaseCollectionDetail }
