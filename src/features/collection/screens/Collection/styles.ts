import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  headerView: {
    marginRight: 10,
  },
})

export { styles }
