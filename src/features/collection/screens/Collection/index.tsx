import React, { useContext, useLayoutEffect } from 'react'
import { Content, View } from 'native-base'
import { useNavigation } from '@react-navigation/native'

import { SCREEN_NAMES } from 'consts'
import { NavigationService } from 'services'
import { UserContext } from 'contexts'
import { ReleaseCollection } from 'types'
import { ButtonHeader, Container, InputSearch } from 'components'
import { useCollection, useDebounceText } from 'hooks'

import { ReleasesList } from '../../components'
import { styles } from './styles'

const Collection: React.FC = () => {
  const { setOptions } = useNavigation()

  const { user } = useContext(UserContext)

  const [, search, setSearch] = useDebounceText('')

  const { releases, isLoadingReleases } = useCollection({
    collectionId: user?.recordCollection,
    search,
  })

  useLayoutEffect(() => {
    setOptions({
      headerRight: () => (
        <View style={styles.headerView}>
          <ButtonHeader
            typeIcon="MaterialCommunityIcons"
            icon="heart"
            onPress={() => NavigationService.navigate(SCREEN_NAMES.WISH_LIST_SCREEN)}
            active={false}
            activeColor="#e74c3c"
          />
        </View>
      ),
    })
  }, [setOptions])

  const navigateToDetail = (release: ReleaseCollection) => {
    NavigationService.navigate(SCREEN_NAMES.RELEASE_COLLECTION_SCREEN, {
      releaseCollection: release,
    })
  }

  return (
    <Container>
      <InputSearch placeholder="Artista, álbum, etc..." onChangeText={(text) => setSearch(text)} />
      <Content>
        <ReleasesList
          isLoading={isLoadingReleases}
          releases={releases ?? []}
          onPressRow={navigateToDetail}
        />
      </Content>
    </Container>
  )
}

export { Collection }
