import { CreateRelease } from 'hooks/useWishList/types'
import { AdditionalData, Release, User } from 'types'

import { Company } from './types'

export const discogsReleaseToCollectionRelease = (
  release: Release,
  user: User,
  additionalData?: AdditionalData,
): CreateRelease => {
  const collectionRelease: CreateRelease = {
    collectionId: user.recordCollection,
    userId: user.id,
    ...additionalData,
    discogsId: release.id,
    artists: release.artists,
    companies: release.companies,
    country: release.country,
    genres: release.genres,
    images: release.images,
    label: release.label,
    series: release.series,
    title: release.title,
    year: release.year,
    formats: release.formats,
    styles: release.styles,
  }
  return collectionRelease
}

export const discogsReleaseToWishRelease = (release: Release, user: User): CreateRelease => {
  const collectionRelease: CreateRelease = {
    discogsId: release.id,
    artists: release.artists,
    companies: release.companies,
    country: release.country,
    genres: release.genres,
    images: release.images,
    label: release.label,
    series: release.series,
    title: release.title,
    year: release.year,
    formats: release.formats,
    styles: release.styles,
    collectionId: user.recordCollection,
    userId: user.id,
  }
  return collectionRelease
}

export const getSerie = (release: Release) => {
  if (release?.series?.length > 0) {
    const serie = release.series[0]
    const { name, catno } = serie
    return `${name} - ${catno}`
  }
  return '-'
}

export const getLabel = (release: Release) => {
  if (release?.label?.length > 0) {
    return release.label[0]
  }
  return '-'
}

export const getFormat = (release: Release) => {
  if (release?.formats?.length > 0) {
    const format = release.formats[0]
    const { name, descriptions } = format
    if (descriptions) {
      return [name, ...descriptions].join(', ')
    } else {
      return name
    }
  }
  return '-'
}

export const getArtist = (release: Release) => {
  return release?.title?.split('-')[0].trim()
}

export const getTitle = (release: Release) => {
  return release?.title
}

export const getGenres = (release: Release) => {
  return release?.genres?.join(', ') || '-'
}

export const getStyles = (release: Release) => {
  return release?.styles?.join(', ') || '-'
}

export const getCompany = (company: Company) => {
  if (company) {
    return `${company.entity_type_name}: ${company.name}`
  } else {
    return ''
  }
}
