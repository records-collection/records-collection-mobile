import { Release } from 'types'
import { api } from 'utils'

const getRelease = async (_: string, id: number): Promise<Release> => {
  const response = await api.get(`/discogs/releases/${id}`)

  return response.data
}

export const SearchApi = { getRelease }
