import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import { SCREEN_NAMES } from 'consts'

import { Screens } from './screens'

const Stack = createStackNavigator()

const InnerStack = () => (
  <Stack.Navigator
    initialRouteName={SCREEN_NAMES.SEARCH_SCREEN}
    screenOptions={{ headerBackTitleVisible: false }}
  >
    <Stack.Screen {...Screens.Search} />
    <Stack.Screen {...Screens.ReleaseDetail} />
    <Stack.Screen {...Screens.Artist} />
    <Stack.Screen {...Screens.MasterRelease} />
  </Stack.Navigator>
)

const SearchStack: React.FC = () => (
  <Stack.Navigator
    initialRouteName={SCREEN_NAMES.SEARCH_SCREEN}
    screenOptions={{ headerBackTitleVisible: false }}
    mode="modal"
  >
    <Stack.Screen component={InnerStack} name="InnerStack" options={{ headerShown: false }} />
    <Stack.Screen {...Screens.BarCodeScanner} />
  </Stack.Navigator>
)

export { SearchStack }
