import { useRoute, useNavigation } from '@react-navigation/native'
import React, { useLayoutEffect } from 'react'
import { View, Content, Spinner, Text } from 'native-base'

import { Accordion, ButtonHeader, Container, ImageSlider } from 'components'
import { SCREEN_NAMES } from 'consts'
import { NavigationService } from 'services'
import { useRelease, useWishList } from 'hooks'

import { ReleaseDetailRoute } from '../../types'
import { styles } from './styles'
import { getCompany, getFormat, getGenres, getLabel, getSerie, getStyles } from '../../utils'

const ReleaseDetail: React.FC = () => {
  const {
    params: { releaseId },
  } = useRoute<ReleaseDetailRoute>()

  const { setOptions } = useNavigation()

  const { release, isLoading } = useRelease(releaseId)

  const { isInWishList, toogleWishList } = useWishList({ release })

  useLayoutEffect(() => {
    setOptions({
      headerRight: () => (
        <View style={styles.headerRightView}>
          <ButtonHeader
            icon="heart"
            onPress={() => toogleWishList()}
            active={isInWishList}
            activeColor="#e74c3c"
          />
          <ButtonHeader
            icon="plus"
            onPress={() =>
              NavigationService.navigate(SCREEN_NAMES.ADD_OR_EDIT_RELEASE_SCREEN, { release })
            }
          />
        </View>
      ),
    })
  }, [setOptions, release, isInWishList, toogleWishList])

  const ItemValue = ({ label, value }: { label: string; value: string }) => (
    <View style={styles.itemValue}>
      <Text style={styles.label}>{label}</Text>
      <Text style={styles.value}>{value}</Text>
    </View>
  )

  return (
    <Container>
      <Content>
        {release && (
          <>
            <ImageSlider images={release.images ?? []} />
            <View>
              <Text style={styles.artistName}>{release.artists[0].name}</Text>
              <Text style={styles.title}>{release.title}</Text>
            </View>
            <View style={styles.containerInfo}>
              <ItemValue label="Sello:" value={getLabel(release)} />
              <ItemValue label="Serie:" value={getSerie(release)} />
              <ItemValue label="Formato:" value={getFormat(release)} />
              <ItemValue label="Origen:" value={release.country} />
              <ItemValue
                label="Publicado:"
                value={release.released_formatted || release.year.toString() || release.released}
              />
              <ItemValue label="Género:" value={getGenres(release)} />
              <ItemValue label="Estilo:" value={getStyles(release)} />
            </View>
            {release.companies?.length > 0 && (
              <Accordion
                title="Compañías"
                containerStyle={styles.accordion}
                contentComponent={
                  <View>
                    {release.companies.map((comp) => (
                      <Text key={getCompany(comp)} style={styles.value}>
                        {getCompany(comp)}
                      </Text>
                    ))}
                  </View>
                }
              />
            )}
            {release.notes && (
              <Accordion
                containerStyle={styles.accordion}
                title="Notas"
                contentText={release.notes}
              />
            )}
          </>
        )}
        {!releaseId || (isLoading && <Spinner color="gray" />)}
      </Content>
    </Container>
  )
}
export { ReleaseDetail }
