import { Dimensions } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'

const { width: screenWidth } = Dimensions.get('window')

const styles = EStyleSheet.create({
  item: {
    width: screenWidth - 60,
    height: screenWidth - 60,
  },
  artistName: {
    fontWeight: 'bold',
    fontSize: 20,
    marginTop: 10,
    marginBottom: 5,
    textAlign: 'center',
    color: '$textColor',
  },
  title: {
    fontSize: 18,
    textAlign: 'center',
    color: '$textColor',
  },
  label: {
    fontSize: 16,
    textAlign: 'left',
    fontWeight: 'bold',
    marginRight: 10,
    color: '$textColor',
  },
  value: {
    fontSize: 16,
    textAlign: 'left',
    color: '$textColor',
  },
  containerInfo: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    padding: 30,
  },
  accordion: {
    paddingHorizontal: 30,
    paddingBottom: 30,
  },
  itemValue: {
    flexDirection: 'row',
  },
  headerRightView: {
    flexDirection: 'row',
  },
})

export { styles }
