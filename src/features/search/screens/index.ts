import { SCREEN_NAMES } from 'consts'
import { BarCodeScanner } from 'components'

import { Search } from './Search'
import { ReleaseDetail } from './ReleaseDetail'
import { Artist } from './Artist'
import { MasterRelease } from './MasterRelease'

export const Screens = {
  Search: {
    name: SCREEN_NAMES.SEARCH_SCREEN,
    component: Search,
    options: { title: 'Buscar' },
  },
  ReleaseDetail: {
    name: SCREEN_NAMES.RELEASE_DETAIL_SCREEN,
    component: ReleaseDetail,
    options: { title: 'Detalle' },
  },
  BarCodeScanner: {
    name: SCREEN_NAMES.BAR_CODE_SCANNER_SCREEN,
    component: BarCodeScanner,
    options: { title: 'Escanear código' },
  },
  Artist: {
    name: SCREEN_NAMES.ARTIST_DETAIL_SCREEN,
    component: Artist,
    options: { title: '' },
  },
  MasterRelease: {
    name: SCREEN_NAMES.MASTER_RELEASE_SCREEN,
    component: MasterRelease,
    options: { title: '' },
  },
}
