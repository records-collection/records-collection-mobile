import React from 'react'
import { Image } from 'react-native'
import { View, Text, Content, Spinner } from 'native-base'
import { useRoute } from '@react-navigation/native'
import { TouchableOpacity } from 'react-native-gesture-handler'

import { Container, ImageSlider } from 'components'
import { ArtistRoute } from 'features/search/types'
import { useArtist, useMasters } from 'hooks'
import { NavigationService } from 'services'
import { SCREEN_NAMES } from 'consts'

import { styles } from './styles'

const Artist: React.FC = () => {
  const {
    params: { artistId },
  } = useRoute<ArtistRoute>()

  const { artist, isLoading } = useArtist(artistId)

  const { masters, isLoading: isLoadingMasters } = useMasters(artistId)

  return (
    <Container>
      <Content>
        {isLoading && <Spinner color="gray" />}
        <ImageSlider images={artist?.images ?? []} />
        <View>
          <Text style={styles.nameText}>{artist?.name}</Text>
          <Text style={styles.profileText}>{artist?.profile}</Text>
        </View>

        <View style={styles.mastersContainer}>
          {!isLoadingMasters && <Text style={styles.title}>Albunes</Text>}
          {masters.map((master, index) => (
            <>
              <TouchableOpacity
                key={master.id}
                onPress={() =>
                  NavigationService.navigate(SCREEN_NAMES.MASTER_RELEASE_SCREEN, {
                    masterId: master.id,
                  })
                }
              >
                <View style={styles.masterItem}>
                  {!!master.thumb && (
                    <Image source={{ uri: master.thumb }} style={styles.masterThumb} />
                  )}
                  <View style={styles.masterInfo}>
                    <Text style={styles.masterTitle}>{master.title}</Text>
                    <Text style={styles.masterYear}>{master.year}</Text>
                  </View>
                </View>
              </TouchableOpacity>
              {index + 1 < masters.length && <View style={styles.separator} />}
            </>
          ))}
        </View>
      </Content>
    </Container>
  )
}

export { Artist }
