import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  carouselContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  nameText: {
    fontWeight: 'bold',
    fontSize: 20,
    marginTop: 10,
    marginBottom: 5,
    textAlign: 'center',
    color: '$textColor',
  },
  profileText: {
    fontSize: 16,
    margin: 10,
    textAlign: 'left',
    color: '$textColor',
  },
  mastersContainer: {
    margin: 20,
  },
  masterThumb: {
    width: 100,
    height: 100,
    marginRight: 20,
  },
  masterTitle: {
    color: '$textColor',
    fontWeight: 'bold',
  },
  masterItem: {
    flexDirection: 'row',
  },
  masterInfo: {
    flexDirection: 'column',
    justifyContent: 'center',
  },
  masterYear: {
    color: '$textColor',
  },
  title: {
    fontSize: '1.3rem',
    color: '$textColor',
    fontWeight: 'bold',
    marginBottom: 20,
  },
  separator: {
    height: 1,
    backgroundColor: '$backgroundContrastColor',
    marginVertical: 10,
  },
})

export { styles }
