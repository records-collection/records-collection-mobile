import { Platform } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  container: {
    display: 'flex',
    backgroundColor: '$backgroundColor',
  },
  itemInput: {
    paddingHorizontal: '1rem',
    backgroundColor: '$backgroundColor',
  },
  moreView: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  moreText: {
    color: 'gray',
    textAlign: 'center',
  },
  filtersView: {
    borderBottomColor: '#e5e5e5',
    borderBottomWidth: 1,
    marginHorizontal: 15,
    flexDirection: 'row',
    width: Platform.OS === 'ios' ? 'auto' : '100%',
  },
  filtersViewContent: {
    width: Platform.OS === 'ios' ? 'auto' : '100%',
    alignItems: 'center',
  },
  iconClearFilters: {
    color: 'gray',
  },
  iconBarcode: {
    color: 'gray',
    marginRight: 20,
  },
  inputIcon: {
    color: '$textColor',
  },
  input: {
    color: '$textPlaceholderColor',
  },
  tabsContainer: {
    backgroundColor: '$backgroundColor',
  },
  activeTab: {
    backgroundColor: '$textColor',
    borderBottomWidth: 2,
    borderColor: '$textColor',
  },
  textTab: {
    color: '$textColor',
  },
})

export { styles }
