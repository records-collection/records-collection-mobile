import React, { useEffect, useLayoutEffect, useState } from 'react'
import { Input, Container, Item, Icon, View } from 'native-base'
import { ScrollView, TouchableOpacity } from 'react-native'
import { useNavigation } from '@react-navigation/native'
import EStyleSheet from 'react-native-extended-stylesheet'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'

import { SCREEN_NAMES } from 'consts'
import { NavigationService } from 'services'
import { Picker } from 'components'
import { countryOptions, formatOptions, yearOptions } from 'features/search/filterOptions'
import { useDebounceText, useSearch } from 'hooks'

import { styles } from './styles'
import { SearchRoute } from '../../types'
import { ArtistsList, ReleasesList } from './components'

interface SarchProps {
  route?: SearchRoute
}

const Search: React.FC<SarchProps> = ({ route }) => {
  const { setOptions } = useNavigation()

  const Tab = createMaterialTopTabNavigator()

  const tabBarOptions = {
    activeTintColor: EStyleSheet.value('$textColor'),
    inactiveTintColor: 'gray',

    indicatorStyle: {
      ...styles.activeTab,
    },
    pressOpacity: 1,
  }

  const [search, searchDebounce, setSearch] = useDebounceText('')

  const [showFilters, setShowFilters] = useState(false)
  const [formatFilter, setFormatFilter] = useState<string | undefined>()
  const [countryFilter, setCountryFilter] = useState<string | undefined>()
  const [yearFilter, setYearFilter] = useState<string | undefined>()

  const {
    releases,
    isLoadingReleases,
    isFetchingReleases,
    searchMoreReleases,
    canFetchMoreReleases,
    artists,
    isFetchingArtists,
    isLoadingArtists,
    canFetchMoreArtists,
    searchMoreArtists,
  } = useSearch({
    search: searchDebounce,
    formatFilter,
    countryFilter,
    yearFilter,
  })

  useEffect(() => {
    if (route?.params?.barcode) {
      setSearch(route.params.barcode)
    }
  }, [route, setSearch])

  useLayoutEffect(() => {
    setOptions({
      headerRight: () => (
        <TouchableOpacity
          onPress={() => NavigationService.navigate(SCREEN_NAMES.BAR_CODE_SCANNER_SCREEN)}
        >
          <Icon type="AntDesign" name="barcode" style={styles.iconBarcode} />
        </TouchableOpacity>
      ),
    })
  }, [setOptions])

  const clearFilters = () => {
    setCountryFilter(undefined)
    setYearFilter(undefined)
    setFormatFilter(undefined)
  }

  return (
    <Container style={styles.container}>
      <Item style={styles.itemInput}>
        <Icon name="ios-search" style={styles.inputIcon} />
        <Input
          placeholder="Artista, álbum, etc..."
          onChangeText={setSearch}
          autoCapitalize="none"
          style={styles.input}
          value={search}
          placeholderTextColor={EStyleSheet.value('$textPlaceholderColor')}
        />
        <TouchableOpacity onPress={() => setShowFilters(!showFilters)}>
          <Icon name="ios-options" style={styles.inputIcon} />
        </TouchableOpacity>
      </Item>
      {showFilters && (
        <View>
          <ScrollView
            horizontal
            style={styles.filtersView}
            contentContainerStyle={styles.filtersViewContent}
          >
            <Picker
              placeholder="Formato"
              selectedValue={formatFilter}
              onValueChange={setFormatFilter}
              options={formatOptions}
            />
            <Picker
              placeholder="País"
              selectedValue={countryFilter}
              onValueChange={setCountryFilter}
              options={countryOptions}
            />
            <Picker
              placeholder="Año"
              selectedValue={yearFilter}
              onValueChange={setYearFilter}
              options={yearOptions}
            />
            <TouchableOpacity onPress={() => clearFilters()}>
              <Icon name="md-close-circle" style={styles.iconClearFilters} />
            </TouchableOpacity>
          </ScrollView>
        </View>
      )}

      <Tab.Navigator
        backBehavior="none"
        initialRouteName="Perfil"
        tabBarOptions={tabBarOptions}
        sceneContainerStyle={styles.tabsContainer}
      >
        <Tab.Screen
          name="Releases"
          options={{
            tabBarLabel: 'Albunes',
          }}
        >
          {() => (
            <ReleasesList
              releases={releases}
              canFetchMore={canFetchMoreReleases}
              isFetching={isFetchingReleases}
              onEndReached={searchMoreReleases}
              isLoading={isLoadingReleases}
            />
          )}
        </Tab.Screen>
        <Tab.Screen
          name="Artists"
          options={{
            tabBarLabel: 'Artistas',
          }}
        >
          {() => (
            <ArtistsList
              artists={artists}
              onEndReached={searchMoreArtists}
              isLoading={isLoadingArtists}
              isFetching={isFetchingArtists}
              canFetchMore={canFetchMoreArtists}
            />
          )}
        </Tab.Screen>
      </Tab.Navigator>
    </Container>
  )
}

export { Search }
