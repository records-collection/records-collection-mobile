import React from 'react'
import { FlatList } from 'react-native'
import { Body, Left, ListItem, Thumbnail, Text, Spinner } from 'native-base'
import EStyleSheet from 'react-native-extended-stylesheet'

import { Release } from 'types'
import { NavigationService } from 'services'
import { SCREEN_NAMES } from 'consts'

import { getFormat, getLabel, getTitle } from '../../../../utils'
import { styles } from './styles'

interface ReleasesListProps {
  onEndReached: () => void
  releases?: Release[]
  isFetching: boolean
  canFetchMore?: boolean
  isLoading: boolean
}

const ReleasesList: React.FC<ReleasesListProps> = ({
  onEndReached,
  releases,
  isFetching,
  canFetchMore,
  isLoading,
}) => {
  const renderListFooter = (
    <>
      {releases && isFetching && <Spinner color="gray" />}
      {releases && !canFetchMore && (
        <Text style={styles.noMoreResultsText}>No hay más resultados</Text>
      )}
    </>
  )

  return (
    <>
      {isLoading && <Spinner color="gray" />}
      <FlatList<Release>
        onEndReachedThreshold={0}
        onEndReached={onEndReached}
        data={releases ?? []}
        keyExtractor={(item) => item.id.toString()}
        ListFooterComponent={renderListFooter}
        renderItem={({ item }) => (
          <ListItem
            key={item.id}
            thumbnail
            onPress={() =>
              NavigationService.navigate(SCREEN_NAMES.RELEASE_DETAIL_SCREEN, {
                releaseId: item.id,
              })
            }
            style={styles.listItem}
            underlayColor={EStyleSheet.value('$backgroundColor')}
          >
            <Left>
              <Thumbnail style={styles.thumbnail} square source={{ uri: item.cover_image }} />
            </Left>
            <Body style={styles.itemBody}>
              <Text style={styles.title}>{getTitle(item)}</Text>
              <Text style={styles.info}>{getFormat(item)}</Text>
              <Text style={styles.info}>
                {item.year && `${item.year} - `}
                {item.country}
              </Text>
              <Text style={styles.info}>
                {getLabel(item)} - {item.catno}
              </Text>
            </Body>
          </ListItem>
        )}
      />
    </>
  )
}
export { ReleasesList }
