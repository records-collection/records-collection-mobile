import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  listItem: {
    borderBottomWidth: 1,
    borderBottomColor: '#e5e5e5',
    marginLeft: '1rem',
    marginRight: '1rem',
    backgroundColor: '$backgroundColor',
  },
  thumbnail: {
    width: 100,
    height: 100,
    marginTop: '1rem',
    marginBottom: '1rem',
  },
  itemBody: {
    borderBottomWidth: 0,
  },
  title: {
    fontWeight: 'bold',
    fontSize: '1rem',
    color: '$textColor',
  },
  info: {
    fontWeight: 'normal',
    fontSize: '0.9rem',
    color: 'gray',
  },
  noMoreResultsText: {
    textAlign: 'center',
    marginVertical: 20,
    color: '$textColor',
  },
})

export { styles }
