import React from 'react'
import { FlatList } from 'react-native'
import { Body, Left, ListItem, Thumbnail, Text, Spinner } from 'native-base'
import EStyleSheet from 'react-native-extended-stylesheet'

import { ArtistDiscogs } from 'types'
import { NavigationService } from 'services'
import { SCREEN_NAMES } from 'consts'

import { styles } from './styles'

interface ArtistsListProps {
  onEndReached: () => void
  artists?: ArtistDiscogs[]
  isFetching: boolean
  canFetchMore?: boolean
  isLoading: boolean
}

const ArtistsList: React.FC<ArtistsListProps> = ({
  onEndReached,
  artists,
  isFetching,
  canFetchMore,
  isLoading,
}) => {
  const renderListFooter = (
    <>
      {artists && isFetching && <Spinner color="gray" />}
      {artists && !canFetchMore && (
        <Text style={styles.noMoreResultsText}>No hay más resultados</Text>
      )}
    </>
  )

  return (
    <>
      {isLoading && <Spinner color="gray" />}
      <FlatList<ArtistDiscogs>
        onEndReachedThreshold={0}
        onEndReached={onEndReached}
        data={artists ?? []}
        keyExtractor={(item) => item.id.toString()}
        ListFooterComponent={renderListFooter}
        renderItem={({ item }) => (
          <ListItem
            key={item.id}
            thumbnail
            onPress={() =>
              NavigationService.navigate(SCREEN_NAMES.ARTIST_DETAIL_SCREEN, {
                artistId: item.id,
              })
            }
            style={styles.listItem}
            underlayColor={EStyleSheet.value('$backgroundColor')}
          >
            <Left>
              <Thumbnail style={styles.thumbnail} square source={{ uri: item.cover_image }} />
            </Left>
            <Body style={styles.itemBody}>
              <Text style={styles.title}>{item.title}</Text>
            </Body>
          </ListItem>
        )}
      />
    </>
  )
}
export { ArtistsList }
