import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  container: {
    display: 'flex',
    backgroundColor: '$backgroundColor',
  },
  content: {
    marginHorizontal: 20,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 20,
    marginTop: 10,
    marginBottom: 20,
    textAlign: 'center',
    color: '$textColor',
  },
  artistName: {
    fontSize: 18,
    textAlign: 'center',
    color: '$textColor',
  },
  versionsTitle: {
    fontWeight: 'bold',
    fontSize: '1.3rem',
    color: '$textColor',
    marginTop: 30,
  },
})

export { styles }
