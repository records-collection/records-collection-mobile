import React from 'react'
import { FlatList } from 'react-native'
import { Left, ListItem, Thumbnail, Text, Spinner, Body } from 'native-base'
import EStyleSheet from 'react-native-extended-stylesheet'

import { VersionRelease } from 'types'
import { NavigationService } from 'services'
import { SCREEN_NAMES } from 'consts'

import { styles } from './styles'

interface VersionsListProps {
  onEndReached: () => void
  versions: VersionRelease[]
  isFetching: boolean
  canFetchMore?: boolean
}

const VersionsList: React.FC<VersionsListProps> = ({
  onEndReached,
  versions,
  isFetching,
  canFetchMore,
}) => {
  const renderListFooter = (
    <>
      {versions && isFetching && <Spinner color="gray" />}
      {versions && !canFetchMore && (
        <Text style={styles.noMoreResultsText}>No hay más resultados</Text>
      )}
    </>
  )

  return (
    <FlatList<VersionRelease>
      onEndReachedThreshold={0.1}
      onEndReached={onEndReached}
      data={versions}
      keyExtractor={(item) => item.id.toString()}
      ListFooterComponent={renderListFooter}
      renderItem={({ item }) => (
        <ListItem
          noIndent
          key={item.id}
          thumbnail
          onPress={() =>
            NavigationService.navigate(SCREEN_NAMES.RELEASE_DETAIL_SCREEN, {
              releaseId: item.id,
            })
          }
          style={styles.listItem}
          underlayColor={EStyleSheet.value('$backgroundColor')}
        >
          <Left>
            <Thumbnail style={styles.thumbnail} square source={{ uri: item.thumb }} />
          </Left>
          <Body style={styles.itemBody}>
            <Text style={styles.itemTitle}>{item.title}</Text>
            <Text style={styles.info}>{`${item.format} - ${item.major_formats}`}</Text>
            <Text style={styles.info}>{`${item.released} - ${item.country}`}</Text>
            <Text style={styles.info}>{item.catno}</Text>
          </Body>
        </ListItem>
      )}
    />
  )
}
export { VersionsList }
