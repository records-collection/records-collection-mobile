import React from 'react'
import { Text, Content, Spinner, View } from 'native-base'

import { Accordion, Container, ImageSlider, Tracklist } from 'components'
import { useMaster, useVersions } from 'hooks'
import { Layouts } from 'styles'

import { styles } from './styles'
import { VersionsList } from './components'

interface MasterReleaseProps {
  route: {
    params: {
      masterId: number
    }
  }
}

const MasterRelease: React.FC<MasterReleaseProps> = ({ route }) => {
  const { masterId } = route.params

  const { master, isLoading: isLoadingMaster } = useMaster(masterId)

  const { versions, isFetching, canFetchMore, searchMore, count } = useVersions(masterId)

  return (
    <Container>
      <Content style={Layouts.flexGrow}>
        {isLoadingMaster ? (
          <Spinner color="gray" />
        ) : (
          <>
            <ImageSlider images={master?.images ?? []} />
            <View style={styles.content}>
              <Text style={styles.artistName}>{master?.artists[0]?.name}</Text>
              <Text style={styles.title}>{master?.title}</Text>
              <Accordion
                title="TrackList"
                contentComponent={<Tracklist tracklist={master?.tracklist ?? []} />}
              />
              <Text style={styles.versionsTitle}>{`Versiones ${count ? `(${count})` : ''}`}</Text>
              <VersionsList
                versions={versions}
                isFetching={isFetching}
                canFetchMore={canFetchMore}
                onEndReached={searchMore}
              />
            </View>
          </>
        )}
      </Content>
    </Container>
  )
}

export { MasterRelease }
