import { RouteProp } from '@react-navigation/native'

export interface ImageUri {
  uri: string
}

export interface Artist {
  id: number
  name: string
}

export interface Serie {
  id: number
  name: string
  catno: string
}

export interface Label {
  id: number
  name: string
  catno: string
}

export interface Format {
  name: string
  descriptions?: string[]
}

export interface Company {
  name: string
  entity_type_name: string
  catno: string
}

export interface AdditionalData {
  personalNotes: string
  mediaState: string
  supportState: string
}

type ParamList = {
  ReleaseDetail: {
    releaseId: number
  }
}

type SearchParamList = {
  Search: {
    barcode?: string
  }
}

export type ReleaseDetailRoute = RouteProp<ParamList, 'ReleaseDetail'>

export type SearchRoute = RouteProp<SearchParamList, 'Search'>

type ArtistParamList = {
  Artist: {
    artistId: number
  }
}

export type ArtistRoute = RouteProp<ArtistParamList, 'Artist'>
