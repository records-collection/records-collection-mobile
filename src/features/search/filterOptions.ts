export const formatOptions = [
  {
    label: 'Todos',
    value: '',
  },
  {
    label: 'Vinyl',
    value: 'Vinyl',
  },
  {
    label: 'CD',
    value: 'CD',
  },
  {
    label: 'DVD',
    value: 'DVD',
  },
  {
    label: 'Cassette',
    value: 'Cassette',
  },
  {
    label: 'LP',
    value: 'LP',
  },
  {
    label: 'EP',
    value: 'EP',
  },
  {
    label: 'Single',
    value: 'Single',
  },
  {
    label: 'Reissue',
    value: 'Reissue',
  },
  {
    label: '7"',
    value: '7"',
  },
  {
    label: '10"',
    value: '10"',
  },
  {
    label: '12"',
    value: '12"',
  },
  {
    label: 'Remastered',
    value: 'Remastered',
  },
  {
    label: 'Unofficial Release',
    value: 'Unofficial Release',
  },
  {
    label: 'Test Pressing',
    value: 'Test Pressing',
  },
  {
    label: 'Remastered',
    value: 'Remastered',
  },
  {
    label: 'Promo',
    value: 'Promo',
  },
  {
    label: 'Box Set',
    value: 'Box Set',
  },
]

export const countryOptions = [
  {
    label: 'Todos',
    value: '',
  },
  {
    label: 'Argentina',
    value: 'Argentina',
  },
  {
    label: 'Alemania',
    value: 'Alemania',
  },
  {
    label: 'Japón',
    value: 'Japon',
  },
]

export const yearOptions = [
  {
    label: 'Todos',
    value: '',
  },
  {
    label: '1970',
    value: '1970',
  },
  {
    label: '1971',
    value: '1971',
  },
  {
    label: '1972',
    value: '1972',
  },
]
