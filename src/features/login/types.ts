import { RouteProp } from '@react-navigation/native'

export interface LoginResponse {
  token: string
  user: UserResponse
}

interface UserResponse {
  _id: string
  email: string
  username: string
  password?: string
  recordCollection: string
  postsSaved: string[]
}

export interface LoginPayload {
  username: string
  password: string
}

export interface SignupPayload {
  username: string
  email: string
  password: string
}

export interface ActivateAccountPayload {
  username: string
  code: string
}

export interface ResendActivationCodePayload {
  username: string
}

type ParamList = {
  Username: {
    username: string
  }
}

export type AccountConfirmationRoute = RouteProp<ParamList, 'Username'>
