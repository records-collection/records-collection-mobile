import { api } from 'utils'

import {
  LoginResponse,
  LoginPayload,
  SignupPayload,
  ActivateAccountPayload,
  ResendActivationCodePayload,
} from './types'

const login = async (payload: LoginPayload): Promise<LoginResponse> => {
  const response = await api.post('/auth/login/user', payload)

  return response.data
}

const loginToken = async (): Promise<LoginResponse> => {
  const response = await api.post('/auth/login/token')

  return response.data
}

const signup = async (payload: SignupPayload): Promise<String> => {
  const response = await api.post('/auth/signup', payload)

  return response.data
}

const activateAccount = async (payload: ActivateAccountPayload): Promise<String> => {
  const response = await api.post('/auth/activateAccount', payload)

  return response.data
}

const resendActivationCode = async (payload: ResendActivationCodePayload): Promise<String> => {
  const response = await api.post('/auth/resendActivationCode', payload)

  return response.data
}

export const LoginApi = {
  login,
  signup,
  activateAccount,
  resendActivationCode,
  loginToken,
}
