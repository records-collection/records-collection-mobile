import React from 'react'
import { StyleProp, ViewStyle } from 'react-native'
import { Item, Input, NativeBase } from 'native-base'

import { styles } from './styles'

interface LoginInput {
  style?: StyleProp<ViewStyle>
}

const LoginInput: React.FC<NativeBase.Input> = (props) => {
  const { style, ...otherProps } = props
  return (
    <Item style={[styles.item, style]} rounded>
      <Input style={styles.input} autoCapitalize="none" {...otherProps} />
    </Item>
  )
}

LoginInput.defaultProps = {
  secureTextEntry: false,
}

export { LoginInput }
