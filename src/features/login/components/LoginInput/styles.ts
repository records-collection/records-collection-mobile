import EStyleSheet from 'react-native-extended-stylesheet'

import { COLORS } from 'styles'

const styles = EStyleSheet.create({
  item: {
    borderRadius: 10,
    marginBottom: '1.5rem',
    width: '100%',
    backgroundColor: COLORS.WHITE,
    shadowColor: COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.32,
    shadowRadius: 5.46,
    elevation: 9,
  },
  input: {
    height: '3rem',
    fontSize: '1rem',
    paddingTop: 0,
    paddingBottom: 0,
  },
  '@media (min-width: 400)': {
    item: {
      marginBottom: '1.3rem',
    },
    input: {
      height: '2.6rem',
      fontSize: '0.9rem',
    },
  },
})

export { styles }
