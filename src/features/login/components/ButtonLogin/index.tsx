import React from 'react'
import { Button as NativeBaseButton, Spinner, Text } from 'native-base'
import EStyleSheet from 'react-native-extended-stylesheet'

import { COLORS } from 'styles'

import { styles } from './styles'

interface ButtonProps {
  isLoading?: boolean
  label: string
  onPress: () => void
  outlined?: boolean
  style?: EStyleSheet.AnyObject
  textStyle?: EStyleSheet.AnyObject
  disabled?: boolean
}

const ButtonLogin: React.FC<ButtonProps> = ({
  isLoading,
  label,
  onPress,
  outlined,
  style,
  textStyle,
  disabled,
}) => (
  <NativeBaseButton
    style={[styles.button, outlined && styles.buttonOutlined, style]}
    onPress={onPress}
    disabled={disabled || isLoading}
  >
    {isLoading ? (
      <Spinner color={COLORS.WHITE} size={15} />
    ) : (
      <Text
        style={[styles.buttonText, outlined && styles.textOutlined, textStyle]}
        uppercase={false}
        numberOfLines={1}
      >
        {label}
      </Text>
    )}
  </NativeBaseButton>
)

export { ButtonLogin }
