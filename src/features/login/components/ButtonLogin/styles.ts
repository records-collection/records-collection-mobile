import EStyleSheet from 'react-native-extended-stylesheet'

import { COLORS } from 'styles'

const styles = EStyleSheet.create({
  button: {
    backgroundColor: COLORS.PRIMARY,
    width: '100%',
    height: 50,
    alignSelf: 'center',
    display: 'flex',
    justifyContent: 'center',
    borderRadius: 10,
    shadowColor: COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.32,
    shadowRadius: 5.46,
    elevation: 9,
  },
  buttonOutlined: {
    backgroundColor: COLORS.WHITE,
    borderColor: COLORS.PRIMARY,
    borderWidth: 2,
  },
  buttonText: {
    color: COLORS.WHITE,
    fontSize: '1.2rem',
  },
  view: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
})

export { styles }
