import EStyleSheet from 'react-native-extended-stylesheet'

import { COLORS } from 'styles'

const styles = EStyleSheet.create({
  item: {
    borderRadius: 10,
    width: '100%',
    marginBottom: '1.5rem',
    backgroundColor: COLORS.WHITE,
    shadowColor: COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.32,
    shadowRadius: 5.46,
    elevation: 9,
  },
  input: {
    height: '3.3rem',
    fontSize: '1.5rem',
    paddingTop: 0,
    paddingBottom: 0,
    textAlign: 'center',
    letterSpacing: 10,
    fontWeight: 'bold',
  },
})

export { styles }
