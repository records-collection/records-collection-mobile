import React from 'react'
import { StyleProp, ViewStyle } from 'react-native'
import { Item, Input, NativeBase } from 'native-base'

import { styles } from './styles'

interface CodeInput {
  style?: StyleProp<ViewStyle>
}

const CodeInput: React.FC<NativeBase.Input> = (props) => {
  const { style, ...otherProps } = props
  return (
    <Item style={[styles.item, style]} rounded>
      <Input style={styles.input} autoCapitalize="none" {...otherProps} maxLength={6} />
    </Item>
  )
}

CodeInput.defaultProps = {
  secureTextEntry: false,
}

export { CodeInput }
