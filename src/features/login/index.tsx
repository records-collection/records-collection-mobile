import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import { Screens } from './screens'

const Stack = createStackNavigator()

const LoginStack: React.FC = () => (
  <Stack.Navigator>
    <Stack.Screen {...Screens.Login} />
    <Stack.Screen {...Screens.Signup} />
    <Stack.Screen {...Screens.AccountConfirmation} />
  </Stack.Navigator>
)

export { LoginStack }
