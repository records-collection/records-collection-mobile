import React, { useState } from 'react'
import { Text, Container } from 'native-base'
import { useMutation } from 'react-query'
import { ImageBackground, TouchableOpacity } from 'react-native'
import { useRoute } from '@react-navigation/native'

import { SCREEN_NAMES } from 'consts'
import { ERRORS } from 'consts/errors'
import { NavigationService } from 'services'
import { Toast } from 'components'
import imageBackground from 'assets/background.png'

import { CodeInput, ButtonLogin } from '../../components'
import { LoginApi } from '../../api'
import { styles } from './styles'
import { AccountConfirmationRoute } from '../../types'

const AccountConfirmation: React.FC = () => {
  const {
    params: { username },
  } = useRoute<AccountConfirmationRoute>()

  const [verificationCode, setVerificationCode] = useState('')

  const [activateAccount, { isLoading }] = useMutation(LoginApi.activateAccount, {
    onSuccess: () => {
      NavigationService.navigate(SCREEN_NAMES.LOGIN_SCREEN)
      Toast.show({
        text: 'Ya podés iniciar sesión',
        type: 'success',
      })
    },
    onError: () => {
      Toast.show({
        text: ERRORS.WRONG_VERIFICATION_CODE,
        type: 'danger',
      })
    },
  })

  const [resendActivationCode] = useMutation(LoginApi.resendActivationCode, {
    onSuccess: () => {
      Toast.show({
        text: 'Te enviamos un nuevo código',
        type: 'success',
      })
    },
    onError: () => {
      Toast.show({
        text: 'Ocurrió un error',
        type: 'danger',
      })
    },
  })

  const onActivateAccount = () => {
    const payload = {
      username,
      code: verificationCode,
    }
    activateAccount(payload)
  }

  const onResendActivationCode = () => {
    const payload = {
      username,
    }
    resendActivationCode(payload)
  }

  return (
    <Container style={styles.container}>
      <ImageBackground source={imageBackground} style={styles.imageBackground} />
      <Text style={styles.title}>Activar cuenta</Text>
      <Text style={styles.caption}>
        Te enviamos un email con un código de verificacion para confirmar tu cuenta. Si no lo
        recibiste te podemos enviar un nuevo código.
      </Text>
      <CodeInput onChangeText={setVerificationCode} />
      <ButtonLogin
        style={styles.button}
        label="Aceptar"
        onPress={onActivateAccount}
        isLoading={isLoading}
        disabled={verificationCode.length < 6}
      />
      <TouchableOpacity onPress={() => onResendActivationCode()}>
        <Text style={styles.resendText}>Reenviar</Text>
      </TouchableOpacity>
    </Container>
  )
}

export { AccountConfirmation }
