import React, { useState } from 'react'
import { ImageBackground, TouchableOpacity } from 'react-native'
import { Container, Text } from 'native-base'
import { useMutation } from 'react-query'
import { AxiosError } from 'axios'

import { SCREEN_NAMES } from 'consts'
import { LoginInput } from 'features/login/components/LoginInput'
import { ERRORS } from 'consts/errors'
import { NavigationService } from 'services'
import { ButtonLogin } from 'features/login/components/ButtonLogin'
import { Toast } from 'components'
import imageBackground from 'assets/background.png'

import { styles } from './styles'
import { LoginApi } from '../../api'

const SignupScreen: React.FC = () => {
  const [username, setUsername] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const [signupApi, { isLoading }] = useMutation(LoginApi.signup, {
    onSuccess: () => {
      NavigationService.navigate(SCREEN_NAMES.ACCOUNT_ACTIVATION_SCREEN, { username })
    },
    onError: ({ response }: AxiosError) => {
      const responseError = response?.data?.error
      if (responseError?.type === 'DuplicateKeyError') {
        if (responseError?.duplicatekey === 'email') {
          Toast.show({
            text: ERRORS.EMAIL_ALREADY_IN_USE,
            type: 'danger',
          })
        }
        if (responseError?.duplicatekey === 'username') {
          Toast.show({
            text: ERRORS.USERNAME_ALREADY_IN_USE,
            type: 'danger',
          })
        }
      } else {
        Toast.show({
          text: 'Ocurrió un error',
          type: 'danger',
        })
      }
    },
  })

  const handleRegister = () => {
    if (!username || !password || !email) {
      Toast.show({
        text: ERRORS.REQUIRED_USER_AND_PASSWORD,
      })
    } else {
      signupApi({ username, email, password })
    }
  }

  return (
    <Container style={styles.container}>
      <ImageBackground source={imageBackground} style={styles.imageBackground} />
      <Text style={styles.title}>Crear cuenta</Text>
      <LoginInput placeholder="Usuario" onChangeText={setUsername} value={username} />
      <LoginInput placeholder="Email" onChangeText={setEmail} value={email} />
      <LoginInput
        placeholder="Contraseña"
        onChangeText={setPassword}
        secureTextEntry
        value={password}
      />
      <ButtonLogin label="Registrate" onPress={handleRegister} isLoading={isLoading} />
      <TouchableOpacity
        onPress={() => {
          NavigationService.navigate(SCREEN_NAMES.LOGIN_SCREEN)
        }}
      >
        <Text style={styles.text}>Ya tengo cuenta</Text>
      </TouchableOpacity>
    </Container>
  )
}

export { SignupScreen }
