import { SCREEN_NAMES } from 'consts'

import { LoginScreen } from './Login'
import { SignupScreen } from './SignUp'
import { AccountConfirmation } from './AccountConfirmation'

export const Screens = {
  Login: {
    name: SCREEN_NAMES.LOGIN_SCREEN,
    component: LoginScreen,
    options: { headerShown: false },
  },
  Signup: {
    name: SCREEN_NAMES.SIGNUP_SCREEN,
    component: SignupScreen,
    options: { headerShown: false },
  },
  AccountConfirmation: {
    name: SCREEN_NAMES.ACCOUNT_ACTIVATION_SCREEN,
    component: AccountConfirmation,
    options: { headerShown: false },
  },
}
