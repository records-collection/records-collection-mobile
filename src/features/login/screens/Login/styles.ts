import EStyleSheet from 'react-native-extended-stylesheet'

import { COLORS } from 'styles'
import { DeviceUtils } from 'utils'

const styles = EStyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 40,
  },
  title: {
    fontSize: 25,
    marginBottom: 30,
    color: COLORS.PRIMARY,
  },
  imageBackground: {
    position: 'absolute',
    width: DeviceUtils.getWindowWidth(),
    height: DeviceUtils.getWindowHeight(),
  },
  signupText: {
    fontWeight: 'bold',
    textDecorationLine: 'underline',
    marginTop: 30,
    color: COLORS.PRIMARY,
  },
})

export { styles }
