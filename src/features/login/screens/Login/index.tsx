import React, { useState } from 'react'
import { ImageBackground, TouchableOpacity } from 'react-native'
import { Container, Text } from 'native-base'
import { useMutation } from 'react-query'
import { AxiosError } from 'axios'

import { SCREEN_NAMES } from 'consts'
import { useLogIn } from 'hooks'
import { ERRORS } from 'consts/errors'
import { Toast } from 'components'
import { NavigationService } from 'services'
import { ButtonLogin } from 'features/login/components/ButtonLogin'
import imageBackground from 'assets/background.png'

import { LoginInput } from '../../components'
import { styles } from './styles'
import { LoginApi } from '../../api'

const LoginScreen: React.FC = () => {
  const [logIn] = useLogIn()

  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')

  const [loginUser, { isLoading }] = useMutation(LoginApi.login, {
    onSuccess: (response) => {
      const { user, token } = response

      const loggedUser = {
        id: user._id,
        username: user.username,
        email: user.email,
        password: user.password,
        recordCollection: user.recordCollection,
        postsSaved: user.postsSaved,
      }

      logIn(token, loggedUser)
    },
    onError: ({ response }: AxiosError) => {
      if (response?.data?.error?.type === ERRORS.ACCOUNT_NOT_CONFIRMED_ERROR) {
        NavigationService.navigate(SCREEN_NAMES.ACCOUNT_ACTIVATION_SCREEN, { username })
        return
      }
      Toast.show({
        text: ERRORS.USER_OR_PASSWORD_INCORRECT,
        type: 'danger',
      })
    },
  })

  const handleLogin = () => {
    if (!username || !password) {
      Toast.show({
        text: ERRORS.REQUIRED_USER_AND_PASSWORD,
      })
    } else {
      loginUser({ username, password })
    }
  }

  return (
    <Container style={styles.container}>
      <ImageBackground source={imageBackground} style={styles.imageBackground} />
      <Text style={styles.title}>Iniciar sesión</Text>
      <LoginInput placeholder="Usuario / Email" onChangeText={setUsername} />
      <LoginInput placeholder="Contraseña" onChangeText={setPassword} secureTextEntry />
      <ButtonLogin label="Iniciar sesión" onPress={() => handleLogin()} isLoading={isLoading} />
      <TouchableOpacity onPress={() => NavigationService.navigate(SCREEN_NAMES.SIGNUP_SCREEN)}>
        <Text style={styles.signupText}>Registrate</Text>
      </TouchableOpacity>
    </Container>
  )
}

export { LoginScreen }
