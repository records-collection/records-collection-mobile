import { Pagination, VersionRelease } from 'types'
import { api } from 'utils'

interface GetVersionsParams {
  masterId: number
  format?: string
  country?: string
  year?: string
  pageSize?: number
}

interface GetVersionsResponse {
  pagination: Pagination
  versions: VersionRelease[]
}

const getVersions = async (
  _: string,
  { masterId, format, country, year, pageSize }: GetVersionsParams,
  page: number = 0,
) => {
  const params = {
    page,
    pageSize: pageSize ?? 10,
    format: format ?? '',
    released: year ?? '',
    country: country ?? '',
    sort: '',
  }
  const response = await api.get<GetVersionsResponse>(`/discogs/versions/${masterId}`, { params })

  return response.data
}

export const VersionsApi = {
  getVersions,
}
