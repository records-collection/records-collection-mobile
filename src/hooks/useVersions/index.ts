import { useInfiniteQuery } from 'react-query'
import { useMemo } from 'react'

import { QUERY_KEYS } from 'consts'

import { VersionsApi } from './api'

const useVersions = (masterId?: number) => {
  const { data: versionsData, isLoading, fetchMore, canFetchMore, isFetching } = useInfiniteQuery(
    [QUERY_KEYS.GET_VERSIONS_KEY, { masterId }],
    VersionsApi.getVersions,
    {
      enabled: masterId,
      getFetchMore: ({ pagination }) => {
        if (pagination.page < pagination.pages) {
          return pagination.page + 1
        }
        return false
      },
    },
  )

  const searchMore = () => {
    if (canFetchMore) fetchMore()
  }

  const flatVersions = useMemo(() => {
    const result = versionsData?.flatMap((data) => data.versions)

    return result ? [...new Map(result.map((item) => [item.id, item])).values()] : undefined
  }, [versionsData])

  const count = useMemo(() => {
    if (!versionsData) return 0
    return versionsData[0]?.pagination.items
  }, [versionsData])

  return {
    versions: flatVersions ?? [],
    count,
    isLoading,
    canFetchMore,
    isFetching,
    searchMore,
  }
}

export { useVersions }
