import { useMutation, useQueryCache } from 'react-query'

import { QUERY_KEYS } from 'consts'
import { SendMessageForm } from 'types'

import { Toast } from '../../components/Toast'
import { MessagesApi } from './api'

const useSendMessage = () => {
  const queryCache = useQueryCache()

  const [create, { isLoading }] = useMutation(MessagesApi.create, {
    onSuccess: () => {
      queryCache.invalidateQueries(QUERY_KEYS.CHAT_KEY)
    },
    onError: () => {
      Toast.show({
        text: 'Ocurrió un error',
        type: 'danger',
      })
    },
  })

  const sendMessage = (form: SendMessageForm) => create(form)

  return {
    sendMessage,
    isLoading,
  }
}

export { useSendMessage }
