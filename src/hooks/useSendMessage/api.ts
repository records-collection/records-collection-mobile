import { api } from 'utils'
import { SendMessageForm } from 'types'

const create = async (payload: SendMessageForm) => {
  const { chatId, message } = payload

  const response = await api.put(`/chats/${chatId}/messages`, {
    message: { message },
  })

  return response.data
}

export const MessagesApi = {
  create,
}
