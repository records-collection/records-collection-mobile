import { api } from 'utils'

const save = async (postId: string) => {
  const response = await api.put(`/posts/${postId}/save`)

  return response.data
}

const remove = async (postId: string) => {
  const response = await api.put(`/posts/${postId}/removeSaved`)

  return response.data
}

export const SavePostApi = {
  save,
  remove,
}
