import { useMutation, useQueryCache } from 'react-query'

import { QUERY_KEYS } from 'consts'

import { Toast } from '../../components/Toast'
import { SavePostApi } from './api'

const useSavePost = () => {
  const queryCache = useQueryCache()

  const [saveMutation, { isLoading: isLoadingSave }] = useMutation(SavePostApi.save, {
    onSuccess: () => {
      queryCache.invalidateQueries(QUERY_KEYS.POSTS_KEY)
    },
    onError: () => {
      Toast.show({
        text: 'Ocurrió un error',
        type: 'danger',
      })
    },
  })

  const [removeMutation, { isLoading: isLoadingRemove }] = useMutation(SavePostApi.remove, {
    onSuccess: () => {
      queryCache.invalidateQueries(QUERY_KEYS.SAVED_POSTS_KEY)
      queryCache.invalidateQueries(QUERY_KEYS.POSTS_KEY)
    },
    onError: () => {
      Toast.show({
        text: 'Ocurrió un error',
        type: 'danger',
      })
    },
  })

  const save = (postId: string) => saveMutation(postId)
  const remove = (postId: string) => removeMutation(postId)

  return {
    save,
    remove,
    isLoadingSave,
    isLoadingRemove,
  }
}

export { useSavePost }
