import { Master, Pagination } from 'types'
import { api } from 'utils'

interface GetMastersByArtistIdResponse {
  pagination: Pagination
  releases: Master[]
}

const getMastersByArtistId = async (_: string, artistId: string) => {
  const response = await api.get<GetMastersByArtistIdResponse>(
    `/discogs/artists/${artistId}/masters`,
  )

  return response.data
}

export const MastersApi = {
  getMastersByArtistId,
}
