import { useQuery } from 'react-query'

import { QUERY_KEYS } from 'consts'

import { MastersApi } from './api'

const useMasters = (artistId?: number) => {
  const { data, isLoading } = useQuery(
    [QUERY_KEYS.GET_MASTERS_BY_ARTIS_ID_KEY, artistId],
    MastersApi.getMastersByArtistId,
    {
      enabled: !!artistId,
    },
  )

  return {
    masters: data?.releases ?? [],
    isLoading,
  }
}

export { useMasters }
