import { useMutation, useQueryCache } from 'react-query'

import { QUERY_KEYS } from 'consts'
import { UpdatePostForm } from 'types'
import { NavigationService } from 'services'

import { Toast } from '../../components/Toast'
import { PostsApi } from './api'

const useUpdatePost = () => {
  const queryCache = useQueryCache()

  const [update, { isLoading }] = useMutation(PostsApi.update, {
    onSuccess: () => {
      queryCache.invalidateQueries(QUERY_KEYS.POSTS_KEY)
      queryCache.invalidateQueries(QUERY_KEYS.POST_KEY)
      queryCache.invalidateQueries(QUERY_KEYS.COLLECTION_SEARCH_KEY)
      NavigationService.goBack()
      Toast.show({
        text: 'Publicación actualizada',
        type: 'success',
      })
    },
    onError: () => {
      Toast.show({
        text: 'Ocurrió un error actualizando la publicación',
        type: 'danger',
      })
    },
  })

  const updatePost = (form: UpdatePostForm) => {
    update(form)
  }

  return {
    updatePost,
    isLoading,
  }
}

export { useUpdatePost }
