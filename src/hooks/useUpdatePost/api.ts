import { api } from 'utils'
import { UpdatePostForm } from 'types'

const update = async (form: UpdatePostForm) => {
  const { releaseUpdate, images, postId, ...post } = form
  const formData = new FormData()
  if (images?.length) {
    images.forEach((image) => {
      //TODO: fix type
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      formData.append('images', image as any)
    })
  }

  formData.append('post', JSON.stringify({ ...post, releaseUpdate }))

  const response = await api.put(`/posts/${postId}`, formData)

  return response.data
}

export const PostsApi = {
  update,
}
