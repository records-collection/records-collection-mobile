import { useContext } from 'react'
import { useQuery } from 'react-query'

import { QUERY_KEYS } from 'consts'
import { UserContext } from 'contexts'

import { SavedPostsApi } from './api'

const useSavedPosts = () => {
  const { user } = useContext(UserContext)

  const { data, isLoading } = useQuery([QUERY_KEYS.SAVED_POSTS_KEY], SavedPostsApi.getPosts, {
    enabled: user,
  })

  return {
    posts: data ?? [],
    isLoading,
  }
}

export { useSavedPosts }
