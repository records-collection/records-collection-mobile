import { api } from 'utils'
import { Chat } from 'types'

interface GetChatsResponse {
  chat: Chat
}

const getChat = async (_: string, chatId: string) => {
  const response = await api.get<GetChatsResponse>(`/chats/${chatId}`)

  return response.data.chat
}

export const ChatsApi = {
  getChat,
}
