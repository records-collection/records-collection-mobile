import { useQuery } from 'react-query'

import { QUERY_KEYS } from 'consts'

import { ChatsApi } from './api'

const useChat = (chatId?: string) => {
  const { data, isLoading } = useQuery([QUERY_KEYS.CHAT_KEY, chatId], ChatsApi.getChat, {
    enabled: chatId,
  })

  return {
    chat: data,
    isLoading,
  }
}

export { useChat }
