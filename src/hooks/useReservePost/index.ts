import { useMutation, useQueryCache } from 'react-query'

import { QUERY_KEYS } from 'consts'

import { Toast } from '../../components/Toast'
import { PostsApi } from './api'

const useReservePost = (onSuccess?: () => void) => {
  const queryCache = useQueryCache()

  const [reserveMutation, { isLoading }] = useMutation(PostsApi.reserve, {
    onSuccess: () => {
      queryCache.invalidateQueries(QUERY_KEYS.POSTS_KEY)
      queryCache.invalidateQueries(QUERY_KEYS.POST_KEY)
      onSuccess && onSuccess()
    },
    onError: () => {
      Toast.show({
        text: 'Ocurrió un error',
        type: 'danger',
      })
    },
  })

  const reserve = (postId: string) => reserveMutation(postId)

  return {
    reserve,
    isLoading,
  }
}

export { useReservePost }
