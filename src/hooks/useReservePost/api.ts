import { PostUpdateResponse } from 'types'
import { api } from 'utils'

interface ReserveResponseResponse {
  post: PostUpdateResponse
}

const reserve = async (postId: string) => {
  const response = await api.put<ReserveResponseResponse>(`/posts/${postId}/reserve`)

  return response.data.post
}

export const PostsApi = {
  reserve,
}
