import { useQuery } from 'react-query'

import { QUERY_KEYS } from 'consts'

import { ReleaseApi } from './api'

const useRelease = (releaseId?: number) => {
  const { data, isLoading } = useQuery(
    [QUERY_KEYS.GET_RELEASE_BY_ID_KEY, releaseId],
    ReleaseApi.getRelease,
    {
      enabled: !!releaseId,
    },
  )

  return {
    release: data,
    isLoading,
  }
}

export { useRelease }
