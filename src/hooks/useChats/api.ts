import { api } from 'utils'
import { Chat } from 'types'

interface GetChatsResponse {
  chats: Chat[]
}

const getChats = async (_: string) => {
  const response = await api.get<GetChatsResponse>(`/chats`)

  return response.data.chats
}

export const ChatsApi = {
  getChats,
}
