import { useContext } from 'react'
import { useQuery } from 'react-query'

import { QUERY_KEYS } from 'consts'
import { UserContext } from 'contexts'

import { ChatsApi } from './api'

const useChats = () => {
  const { user } = useContext(UserContext)

  const { data, isLoading } = useQuery([QUERY_KEYS.CHATS_KEY], ChatsApi.getChats, {
    enabled: user,
  })

  return {
    chats: data ?? [],
    isLoading,
  }
}

export { useChats }
