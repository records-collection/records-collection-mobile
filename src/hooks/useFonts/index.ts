import { useEffect, useState } from 'react'
import { Ionicons } from '@expo/vector-icons'
import * as Font from 'expo-font'

const useFonts = () => {
  const [isLoadingFonts, setIsLoadingFonts] = useState(true)

  useEffect(() => {
    const loadFonts = async () => {
      await Font.loadAsync({
        Roboto: require('native-base/Fonts/Roboto.ttf'),
        Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
        ...Ionicons.font,
      })
      setIsLoadingFonts(false)
    }
    loadFonts()
  }, [])
  return { isLoadingFonts }
}

export { useFonts }
