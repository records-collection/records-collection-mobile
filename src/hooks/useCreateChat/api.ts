import { api } from 'utils'
import { Chat } from 'types'

interface PostChatResponse {
  chat: Chat
}

const create = async (userId: string) => {
  const response = await api.post<PostChatResponse>('/chats', { userId })

  return response.data.chat
}

export const ChatsApi = {
  create,
}
