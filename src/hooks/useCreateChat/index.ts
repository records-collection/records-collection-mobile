import { useMutation, useQueryCache } from 'react-query'

import { QUERY_KEYS } from 'consts'
import { Chat } from 'types'

import { Toast } from '../../components/Toast'
import { ChatsApi } from './api'

const useCreateChat = (onSuccess?: (chat: Chat) => void) => {
  const queryCache = useQueryCache()

  const [create, { isLoading }] = useMutation(ChatsApi.create, {
    onSuccess: (chat: Chat) => {
      queryCache.invalidateQueries(QUERY_KEYS.CHATS_KEY)
      onSuccess && onSuccess(chat)
    },
    onError: () => {
      Toast.show({
        text: 'Ocurrió un error creando el chat',
        type: 'danger',
      })
    },
  })

  const createChat = (userId: string) => {
    create(userId)
  }

  return {
    createChat,
    isLoading,
  }
}

export { useCreateChat }
