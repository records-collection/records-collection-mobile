import { useContext } from 'react'
import AsyncStorage from '@react-native-async-storage/async-storage'

import { UserContext } from 'contexts'
import { removeAuthHeader } from 'utils'
import { ASYNC_STORAGE_KEYS } from 'consts'

const useLogOut = () => {
  const { setUser } = useContext(UserContext)

  return [
    async () => {
      removeAuthHeader()
      setUser(undefined)
      AsyncStorage.removeItem(ASYNC_STORAGE_KEYS.TOKEN)
    },
  ]
}

export { useLogOut }
