import { useQuery } from 'react-query'

import { QUERY_KEYS } from 'consts'

import { ArtistApi } from './api'

const useArtist = (artistId?: number) => {
  const { data: artist, isLoading } = useQuery(
    [QUERY_KEYS.GET_ARTIST_BY_ID_KEY, artistId],
    ArtistApi.getArtistById,
    {
      enabled: !!artistId,
    },
  )

  return {
    artist,
    isLoading,
  }
}

export { useArtist }
