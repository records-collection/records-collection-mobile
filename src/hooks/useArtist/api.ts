import { ArtistDetailDiscogs } from 'types'
import { api } from 'utils'

interface GetArtistByIdResponse {
  artist: ArtistDetailDiscogs
}

const getArtistById = async (_: string, artistId: string) => {
  const response = await api.get<GetArtistByIdResponse>(`/discogs/artists/${artistId}`)

  return response.data.artist
}

export const ArtistApi = {
  getArtistById,
}
