import { useInfiniteQuery } from 'react-query'
import { useCallback } from 'react'

import { SearchApi } from 'api'
import { QUERY_KEYS } from 'consts'

interface SearchParams {
  search?: string
  formatFilter?: string
  countryFilter?: string
  yearFilter?: string
}

const useSearch = ({ search, formatFilter, countryFilter, yearFilter }: SearchParams) => {
  const {
    data: releasesData,
    isLoading: isLoadingReleases,
    fetchMore: fetchMoreReleases,
    canFetchMore: canFetchMoreReleases,
    isFetching: isFetchingReleases,
  } = useInfiniteQuery(
    [QUERY_KEYS.SEARCH_QUERY_KEY, { query: search, formatFilter, countryFilter, yearFilter }],
    SearchApi.searchReleases,
    {
      enabled: !!search,
      getFetchMore: ({ pagination }) => {
        if (pagination.page < pagination.pages) {
          return pagination.page + 1
        }
        return false
      },
    },
  )

  const {
    data: artistsData,
    isLoading: isLoadingArtists,
    fetchMore: fetchMoreArtists,
    canFetchMore: canFetchMoreArtists,
    isFetching: isFetchingArtists,
  } = useInfiniteQuery(
    [
      QUERY_KEYS.SEARCH_ARTISTS_KEY,
      { query: search, formatFilter, countryFilter, yearFilter, type: 'artist' },
    ],
    SearchApi.searchArtists,
    {
      enabled: !!search,
      getFetchMore: ({ pagination }) => {
        if (pagination.page < pagination.pages) {
          return pagination.page + 1
        }
        return false
      },
    },
  )

  const searchMoreReleases = () => {
    if (canFetchMoreReleases) fetchMoreReleases()
  }

  const searchMoreArtists = () => {
    if (canFetchMoreArtists) fetchMoreArtists()
  }

  const flatReleases = useCallback(() => {
    const result = releasesData?.flatMap((data) => data.results)

    return result ? [...new Map(result.map((item) => [item.id, item])).values()] : undefined
  }, [releasesData])

  const flatArtists = useCallback(() => {
    const result = artistsData?.flatMap((data) => data.results)

    return result ? [...new Map(result.map((item) => [item.id, item])).values()] : undefined
  }, [artistsData])

  return {
    releases: flatReleases(),
    isLoadingReleases,
    canFetchMoreReleases,
    isFetchingReleases,
    searchMoreReleases,
    artists: flatArtists(),
    isLoadingArtists,
    canFetchMoreArtists,
    isFetchingArtists,
    searchMoreArtists,
  }
}

export { useSearch }
