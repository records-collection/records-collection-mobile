import { useMutation, useQueryCache } from 'react-query'
import { AxiosError } from 'axios'

import { QUERY_KEYS } from 'consts'
import { CreatePostForm } from 'types'
import { NavigationService } from 'services'
import { ERRORS } from 'consts/errors'

import { Toast } from '../../components/Toast'
import { PostsApi } from './api'

const useCreatePost = () => {
  const queryCache = useQueryCache()

  const [create, { isLoading }] = useMutation(PostsApi.create, {
    onSuccess: () => {
      queryCache.invalidateQueries(QUERY_KEYS.POSTS_KEY)
      queryCache.invalidateQueries(QUERY_KEYS.GET_RELEASE_BY_ID_KEY)
      NavigationService.goBack()
      Toast.show({
        text: 'Publicación creada',
        type: 'success',
      })
    },
    onError: ({ response }: AxiosError) => {
      console.log({ response })
      if (response?.data?.error?.type === ERRORS.RESOURCE_ALREADY_EXISTS) {
        Toast.show({
          text: 'Ya existe una publicación con este álbum',
          type: 'danger',
        })
      } else {
        Toast.show({
          text: 'Ocurrió un error creando la publicación',
          type: 'danger',
        })
      }
    },
  })

  const createPost = (form: CreatePostForm) => {
    create(form)
  }

  return {
    createPost,
    isLoading,
  }
}

export { useCreatePost }
