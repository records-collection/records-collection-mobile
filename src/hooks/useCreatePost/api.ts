import { api } from 'utils'
import { CreatePostForm } from 'types'

const create = async (form: CreatePostForm) => {
  const { releaseUpdate, ...post } = form
  const formData = new FormData()
  if (releaseUpdate?.images?.length) {
    releaseUpdate.images.forEach((image) => {
      //TODO: fix type
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      formData.append('images', image as any)
    })
  }

  formData.append('post', JSON.stringify({ ...post, releaseUpdate: { ...releaseUpdate?.release } }))

  const response = await api.post('/posts', formData)

  return response.data
}

export const PostsApi = {
  create,
}
