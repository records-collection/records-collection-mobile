import { api } from 'utils'

const like = async (postId: string) => {
  const response = await api.put(`/posts/${postId}/like`)

  return response.data
}

const unlike = async (postId: string) => {
  const response = await api.put(`/posts/${postId}/unlike`)

  return response.data
}

export const LikePostApi = {
  like,
  unlike,
}
