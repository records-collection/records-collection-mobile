import { useMutation, useQueryCache } from 'react-query'

import { QUERY_KEYS } from 'consts'

import { Toast } from '../../components/Toast'
import { LikePostApi } from './api'

const useLikePost = () => {
  const queryCache = useQueryCache()

  const [likeMutation, { isLoading: isLoadingLike }] = useMutation(LikePostApi.like, {
    onSuccess: () => {
      queryCache.invalidateQueries(QUERY_KEYS.POSTS_KEY)
    },
    onError: () => {
      Toast.show({
        text: 'Ocurrió un error',
        type: 'danger',
      })
    },
  })

  const [unlikeMutation, { isLoading: isLoadingUnlike }] = useMutation(LikePostApi.unlike, {
    onSuccess: () => {
      queryCache.invalidateQueries(QUERY_KEYS.POSTS_KEY)
    },
    onError: () => {
      Toast.show({
        text: 'Ocurrió un error',
        type: 'danger',
      })
    },
  })

  const like = (postId: string) => likeMutation(postId)
  const unlike = (postId: string) => unlikeMutation(postId)

  return {
    like,
    unlike,
    isLoadingLike,
    isLoadingUnlike,
  }
}

export { useLikePost }
