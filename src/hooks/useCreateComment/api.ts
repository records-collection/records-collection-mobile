import { api } from 'utils'
import { Comment } from 'types'

interface CreatePostCommentResponse {
  comment: Comment
}

interface CreatePostCommentPayload {
  postId: string
  comment: string
}

const create = async (payload: CreatePostCommentPayload) => {
  const { postId, comment } = payload

  const response = await api.post<CreatePostCommentResponse>(`/posts/${postId}/comments`, {
    comment,
  })

  return response.data.comment
}

export const CommentsApi = {
  create,
}
