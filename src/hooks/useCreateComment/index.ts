import { useMutation, useQueryCache } from 'react-query'

import { QUERY_KEYS } from 'consts'

import { Toast } from '../../components/Toast'
import { CommentsApi } from './api'

const useCreateComment = () => {
  const queryCache = useQueryCache()

  const [create, { isLoading }] = useMutation(CommentsApi.create, {
    onSuccess: () => {
      queryCache.invalidateQueries(QUERY_KEYS.POST_KEY)
    },
    onError: () => {
      Toast.show({
        text: 'Ocurrió un error',
        type: 'danger',
      })
    },
  })

  return {
    create,
    isLoading,
  }
}

export { useCreateComment }
