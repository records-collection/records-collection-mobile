import { ReleaseCollection } from 'types'

export interface GetReleaseResponse {
  release: ReleaseCollection
}
