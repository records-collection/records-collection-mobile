import { api } from 'utils'

import { GetReleaseResponse } from './types'

const getRelease = async (_: string, releaseId: string): Promise<GetReleaseResponse> => {
  const response = await api.get(`/releases/${releaseId}`)

  return response.data
}

export const ReleaseCollectionApi = {
  getRelease,
}
