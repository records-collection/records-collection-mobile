import { useQuery } from 'react-query'

import { QUERY_KEYS } from 'consts'

import { ReleaseCollectionApi } from './api'

const useReleaseCollection = (releaseId?: string) => {
  const { data, isLoading: isLoadingRelease } = useQuery(
    [QUERY_KEYS.GET_COLLECTION_RELEASE_BY_ID_KEY, releaseId],
    ReleaseCollectionApi.getRelease,
    {
      enabled: !!releaseId,
    },
  )

  return {
    release: data?.release,
    isLoadingRelease,
  }
}

export { useReleaseCollection }
