import { MasterRelease } from 'types'
import { api } from 'utils'

interface GetMasterByIdResponse {
  master: MasterRelease
}

const getMasterById = async (_: string, masterId: string) => {
  const response = await api.get<GetMasterByIdResponse>(`/discogs/masters/${masterId}`)

  return response.data.master
}

export const MasterApi = {
  getMasterById,
}
