import { useQuery } from 'react-query'

import { QUERY_KEYS } from 'consts'

import { MasterApi } from './api'

const useMaster = (masterId?: number) => {
  const { data, isLoading } = useQuery(
    [QUERY_KEYS.GET_MASTERS_BY_ARTIS_ID_KEY, masterId],
    MasterApi.getMasterById,
    {
      enabled: !!masterId,
    },
  )

  return {
    master: data,
    isLoading,
  }
}

export { useMaster }
