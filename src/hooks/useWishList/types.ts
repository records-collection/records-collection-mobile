import { ReleaseCollection } from 'types'

export interface NewWishReleasePayload {
  collectionId: string
  release: CreateRelease
}

export type CreateRelease = Omit<ReleaseCollection, '_id'>

export interface WishListResponse {
  id: string
  releases: ReleaseCollection[]
}

export interface RemoveReleaseParams {
  collectionId: string
  releaseDiscogsId: number
}
