import { useMutation, useQuery, useQueryCache } from 'react-query'
import { useContext, useMemo } from 'react'

import { QUERY_KEYS } from 'consts'
import { UserContext } from 'contexts'
import { Release } from 'types'
import { discogsReleaseToWishRelease } from 'features/search/utils'

import { Toast } from '../../components/Toast'
import { WishListApi } from './api'
import { CreateRelease, RemoveReleaseParams } from './types'

interface UseWishListParams {
  search?: string
  onSuccessRemove?: () => void
  release?: Release
}

const useWishList = (params?: UseWishListParams) => {
  const queryCache = useQueryCache()

  const { user } = useContext(UserContext)

  const { data, isLoading: isLoadingWishList } = useQuery(
    [QUERY_KEYS.WISH_LIST_KEY, user?.recordCollection, params?.search],
    WishListApi.getReleases,
    {
      enabled: !!user?.recordCollection,
    },
  )

  const [create, { isLoading: isLoadingAddRelease }] = useMutation(WishListApi.addRelease, {
    onSuccess: () => {
      queryCache.invalidateQueries(QUERY_KEYS.WISH_LIST_KEY)
      Toast.show({
        text: 'Agregado a la lista de deseados',
        type: 'success',
      })
    },
    onError: () => {
      Toast.show({
        text: 'Ocurrió un error agregando a la lista de deseados',
        type: 'danger',
      })
    },
  })

  const addRelease = (formData: { collectionId: string; release: CreateRelease }) => {
    create(formData)
  }

  const [remove] = useMutation(WishListApi.removeRelease, {
    onSuccess: () => {
      queryCache.invalidateQueries(QUERY_KEYS.WISH_LIST_KEY)
      params?.onSuccessRemove && params?.onSuccessRemove()
    },
    onError: () => {
      Toast.show({
        text: 'Ocurrió un error',
        type: 'danger',
      })
    },
  })

  const removeRelease = (removeParams: RemoveReleaseParams) => {
    remove(removeParams)
  }

  const isInWishList = useMemo(() => {
    const exists = data?.releases.find((release) => release.discogsId === params?.release?.id)
    return !!exists
  }, [data, params?.release?.id])

  const toogleWishList = () => {
    if (user?.recordCollection && params?.release?.id) {
      if (isInWishList) {
        removeRelease({
          collectionId: user.recordCollection,
          releaseDiscogsId: params.release.id,
        })
      } else {
        addRelease({
          collectionId: user.recordCollection,
          release: discogsReleaseToWishRelease(params.release, user),
        })
      }
    }
  }

  return {
    releases: data?.releases ?? [],
    addRelease,
    isLoadingWishList,
    isLoadingAddRelease,
    removeRelease,
    isInWishList,
    toogleWishList,
  }
}

export { useWishList }
