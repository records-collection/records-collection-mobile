import { api } from 'utils'

import { NewWishReleasePayload, RemoveReleaseParams, WishListResponse } from './types'

const getReleases = async (
  _: string,
  idCollection: string,
  search: string,
): Promise<WishListResponse> => {
  const response = await api.get(`collection/${idCollection}/wishList/?search=${search ?? ''}`)

  return response.data
}

const addRelease = async (payload: NewWishReleasePayload) => {
  const response = await api.post(`/collection/wishList`, payload)

  return response.data
}

const removeRelease = async ({ collectionId, releaseDiscogsId }: RemoveReleaseParams) => {
  const response = await api.delete(`/collection/wishList/${collectionId}/${releaseDiscogsId}`)

  return response.data
}

export const WishListApi = {
  getReleases,
  addRelease,
  removeRelease,
}
