import { api } from 'utils'
import { Post } from 'types'

interface GetPostsResponse {
  posts: Post[]
}

const getPosts = async (_: string, state: string) => {
  const params = {
    state,
  }
  const response = await api.get<GetPostsResponse>('/posts/user/myposts', { params })

  return response.data.posts
}

export const MyPostsApi = {
  getPosts,
}
