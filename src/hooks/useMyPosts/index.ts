import { useContext } from 'react'
import { useQuery } from 'react-query'

import { QUERY_KEYS } from 'consts'
import { UserContext } from 'contexts'

import { MyPostsApi } from './api'

const useMyPosts = (state?: string) => {
  const { user } = useContext(UserContext)

  const { data, isLoading } = useQuery([QUERY_KEYS.MY_POSTS_KEY, state], MyPostsApi.getPosts, {
    enabled: user,
  })

  return {
    posts: data ?? [],
    isLoading,
  }
}

export { useMyPosts }
