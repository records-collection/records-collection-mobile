import { useContext } from 'react'
import AsyncStorage from '@react-native-async-storage/async-storage'

import { UserContext } from 'contexts'
import { User } from 'types'
import { setAuthHeader } from 'utils'
import { ASYNC_STORAGE_KEYS } from 'consts'

const useLogIn = () => {
  const { setUser } = useContext(UserContext)

  return [
    async (token: string, user: User) => {
      setAuthHeader(token)
      await AsyncStorage.setItem(ASYNC_STORAGE_KEYS.TOKEN, token)
      setUser(user)
    },
  ]
}

export { useLogIn }
