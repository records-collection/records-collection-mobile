import { useQuery } from 'react-query'

import { QUERY_KEYS } from 'consts'

import { PostApi } from './api'

const usePost = (postId?: string) => {
  const { data, isLoading } = useQuery([QUERY_KEYS.POST_KEY, postId], PostApi.getPost, {
    enabled: postId,
  })

  return {
    post: data,
    isLoading,
  }
}

export { usePost }
