import { api } from 'utils'
import { Post } from 'types'

interface GetPostResponse {
  post: Post
}

const getPost = async (_: string, postId: string) => {
  const response = await api.get<GetPostResponse>(`/posts/${postId}`)

  return response.data.post
}

export const PostApi = {
  getPost,
}
