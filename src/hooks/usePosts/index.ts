import { useContext, useState } from 'react'
import { useQuery, useQueryCache } from 'react-query'

import { QUERY_KEYS } from 'consts'
import { UserContext } from 'contexts'

import { PostsApi } from './api'

const usePosts = () => {
  const { user } = useContext(UserContext)

  const queryCache = useQueryCache()

  const [isRefreshing, setIsRefreshing] = useState(false)

  const { data, isLoading } = useQuery([QUERY_KEYS.POSTS_KEY], PostsApi.getPosts, {
    onSuccess: () => {
      setIsRefreshing(false)
    },
    enabled: user,
    refetchOnMount: true,
    refetchOnWindowFocus: true,
  })

  const refresh = () => {
    setIsRefreshing(true)
    queryCache.invalidateQueries(QUERY_KEYS.POSTS_KEY)
  }

  return {
    posts: data ?? [],
    isLoading,
    refresh,
    isRefreshing,
  }
}

export { usePosts }
