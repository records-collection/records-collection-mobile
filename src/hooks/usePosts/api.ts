import { api } from 'utils'
import { Post } from 'types'

interface GetPostsResponse {
  posts: Post[]
}

const getPosts = async (_: string) => {
  const response = await api.get<GetPostsResponse>(`/posts`)

  return response.data.posts
}

export const PostsApi = {
  getPosts,
}
