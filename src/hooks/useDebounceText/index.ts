import React, { useState } from 'react'
import { useDebounce } from 'use-debounce'

const useDebounceText = (
  defaultValue?: string,
): [string, string, React.Dispatch<React.SetStateAction<string>>] => {
  const [text, setText] = useState(defaultValue ?? '')
  const [textDebounced] = useDebounce(text, 500)

  return [text, textDebounced, setText]
}

export { useDebounceText }
