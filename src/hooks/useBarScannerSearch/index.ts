import { useQuery } from 'react-query'

import { SearchApi } from 'api'
import { SearchResponse } from 'types'
import { QUERY_KEYS } from 'consts'

const useBarScannerSearch = (barcode?: string, onSuccess?: (reponse: SearchResponse) => void) => {
  const { isLoading } = useQuery(
    [QUERY_KEYS.BARCODE_SCAN_KEY, { query: barcode }],
    SearchApi.searchReleases,
    {
      enabled: !!barcode,
      onSuccess: (response: SearchResponse) => {
        onSuccess && onSuccess(response)
      },
    },
  )

  return {
    isLoading,
  }
}

export { useBarScannerSearch }
