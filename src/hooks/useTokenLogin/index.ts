import { useCallback, useEffect, useState } from 'react'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { useMutation } from 'react-query'

import { ASYNC_STORAGE_KEYS } from 'consts'
import { setAuthHeader } from 'utils'
import { LoginApi } from 'features/login/api'

import { useLogIn } from '../useLogIn'

const useTokenLogin = (): { isLoading: boolean } => {
  const [logIn] = useLogIn()
  const [isLoading, setIsLoading] = useState(true)

  const [loginToken] = useMutation(LoginApi.loginToken, {
    onSuccess: (response) => {
      const { user, token } = response

      const loggedUser = {
        id: user._id,
        username: user.username,
        email: user.email,
        password: user.password,
        recordCollection: user.recordCollection,
        postsSaved: user.postsSaved,
      }
      logIn(token, loggedUser)
      setTimeout(() => {
        setIsLoading(false)
      }, 200)
    },
    onError: () => {
      setIsLoading(false)
    },
  })

  const handleLogin = useCallback(async () => {
    const token = await AsyncStorage.getItem(ASYNC_STORAGE_KEYS.TOKEN)
    if (!token) {
      setIsLoading(false)
      return
    }
    setAuthHeader(token)
    loginToken()
  }, [loginToken])

  useEffect(() => {
    handleLogin()
  }, [handleLogin])

  return { isLoading }
}

export { useTokenLogin }
