import { api } from 'utils'

import {
  CollectionResponse,
  GetReleasesParams,
  AddReleasePayload,
  UpdateReleasePayload,
} from './types'

const getReleases = async (_: string, params: GetReleasesParams): Promise<CollectionResponse> => {
  const response = await api.get('/releases', { params })

  return response.data
}

const addRelease = async (payload: AddReleasePayload) => {
  const { release, images } = payload

  const formData = new FormData()
  if (images?.length) {
    images.forEach((image) => {
      //TODO: fix type
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      formData.append('images', image as any)
    })
  }

  formData.append('release', JSON.stringify(release))

  const response = await api.post(`/releases`, formData)

  return response.data
}

const updateRelease = async (payload: UpdateReleasePayload) => {
  const { releaseId, release, images } = payload

  const formData = new FormData()
  if (images?.length) {
    images.forEach((image) => {
      //TODO: fix type
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      formData.append('images', image as any)
    })
  }

  formData.append('release', JSON.stringify(release))

  const response = await api.put(`/releases/${releaseId}`, formData)

  return response.data
}

const deleteRelease = async (releaseId: string) => {
  const response = await api.delete(`/releases/${releaseId}`)

  return response.data
}

export const CollectionApi = {
  getReleases,
  addRelease,
  updateRelease,
  deleteRelease,
}
