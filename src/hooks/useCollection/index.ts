import { useMutation, useQuery, useQueryCache } from 'react-query'

import { QUERY_KEYS } from 'consts'
import { NavigationService } from 'services'

import { Toast } from '../../components/Toast'
import { CollectionApi } from './api'
import { AddReleasePayload, UpdateReleasePayload } from './types'

interface UseCollectionParams {
  collectionId?: string
  search?: string
}

const useCollection = (params?: UseCollectionParams) => {
  const queryCache = useQueryCache()

  const { data, isLoading: isLoadingReleases } = useQuery(
    [
      QUERY_KEYS.COLLECTION_SEARCH_KEY,
      { collectionId: params?.collectionId, search: params?.search },
    ],
    CollectionApi.getReleases,
    {
      enabled: !!params?.collectionId,
    },
  )

  const [create, { isLoading: isLoadingAddRelease }] = useMutation(CollectionApi.addRelease, {
    onSuccess: () => {
      queryCache.invalidateQueries(QUERY_KEYS.COLLECTION_SEARCH_KEY)
      NavigationService.goBack()
      Toast.show({
        text: 'Agregado a la colección',
        type: 'success',
      })
    },
    onError: () => {
      Toast.show({
        text: 'Ocurrió un error',
        type: 'danger',
      })
    },
  })

  const addRelease = (payload: AddReleasePayload) => {
    create(payload)
  }

  const [update, { isLoading: isLoadingUpdateRelease }] = useMutation(CollectionApi.updateRelease, {
    onSuccess: () => {
      queryCache.invalidateQueries(QUERY_KEYS.LIST_IN_SALE_KEY)
      queryCache.invalidateQueries(QUERY_KEYS.LIST_EXCHANGE_KEY)
      queryCache.invalidateQueries(QUERY_KEYS.COLLECTION_SEARCH_KEY)
      queryCache.invalidateQueries(QUERY_KEYS.GET_COLLECTION_RELEASE_BY_ID_KEY)
      Toast.show({
        text: 'Actualizado',
        type: 'success',
      })
      NavigationService.goBack()
    },
    onError: () => {
      Toast.show({
        text: 'Ocurrió un error',
        type: 'danger',
      })
    },
  })

  const updateRelease = (payload: UpdateReleasePayload) => {
    update(payload)
  }

  const [remove, { isLoading: isLoadingRemoveRelease }] = useMutation(CollectionApi.deleteRelease, {
    onSuccess: () => {
      queryCache.invalidateQueries(QUERY_KEYS.LIST_IN_SALE_KEY)
      queryCache.invalidateQueries(QUERY_KEYS.LIST_EXCHANGE_KEY)
      queryCache.invalidateQueries(QUERY_KEYS.COLLECTION_SEARCH_KEY)
      NavigationService.goBack()
    },
    onError: () => {
      Toast.show({
        text: 'Ocurrió un error',
        type: 'danger',
      })
    },
  })

  const removeRelease = (releaseId: string) => {
    remove(releaseId)
  }

  return {
    releases: data?.releases,
    addRelease,
    updateRelease,
    removeRelease,
    isLoadingReleases,
    isLoadingAddRelease,
    isLoadingUpdateRelease,
    isLoadingRemoveRelease,
  }
}

export { useCollection }
