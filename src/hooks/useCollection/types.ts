import { ImageResult, ReleaseCollection } from 'types'

export interface CollectionResponse {
  id: string
  releases: ReleaseCollection[]
}

export interface GetReleasesParams {
  collectionId: string
  search?: string
  size?: number
  page?: number
}

export type CreateRelease = Omit<ReleaseCollection, '_id'>

export interface AddReleasePayload {
  release: CreateRelease
  images?: ImageResult[]
}

export interface UpdateReleasePayload {
  releaseId: string
  release: {
    mediaState?: string
    supportState?: string
    personalNotes?: string
    imagesKeys?: string[]
  }
  images?: ImageResult[]
}
