import React from 'react'
import { Icon, Picker as PickerNative } from 'native-base'

interface PickerOption {
  label: string
  value: string
}

interface PickerProps {
  placeholder: string
  selectedValue: string | undefined
  onValueChange: React.Dispatch<React.SetStateAction<string | undefined>>
  options: PickerOption[]
}

const Picker: React.FC<PickerProps> = ({ placeholder, selectedValue, onValueChange, options }) => (
  <PickerNative
    mode="dropdown"
    iosHeader="Formatos"
    iosIcon={<Icon name="arrow-down" />}
    placeholder={placeholder}
    selectedValue={selectedValue}
    onValueChange={onValueChange}
    headerBackButtonText="Atras"
  >
    {options.map((option) => (
      <PickerNative.Item label={option.label} value={option.value} />
    ))}
  </PickerNative>
)

export { Picker }
