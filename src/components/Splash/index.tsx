import React, { useEffect } from 'react'
import * as SplashScreen from 'expo-splash-screen'

import { useTokenLogin } from 'hooks'

const Splash: React.FC = ({ children }) => {
  const { isLoading } = useTokenLogin()

  useEffect(() => {
    if (!isLoading) {
      SplashScreen.hideAsync()
    }
  }, [isLoading])

  return <>{children}</>
}

export { Splash }
