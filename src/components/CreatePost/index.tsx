import React, { useCallback, useLayoutEffect, useState } from 'react'
import { Container, Content, Spinner, Text, Textarea, Icon, Item, Input, View } from 'native-base'
import { RouteProp, useNavigation } from '@react-navigation/native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import EStyleSheet from 'react-native-extended-stylesheet'

import { ImageResult, OwnImage, ReleaseCollection } from 'types'
import { NavigationService } from 'services'
import { useCreatePost } from 'hooks'
import { ImagePicker } from 'components/ImagePicker'
import { Toast } from 'components/Toast'

import { StatePicker } from '../StatePicker'
import { styles } from './styles'

type ParamList = {
  CreatePostRoute: {
    releaseCollection: ReleaseCollection
  }
}

export type CreatePostRoute = RouteProp<ParamList, 'CreatePostRoute'>

interface CreatePostProps {
  route: CreatePostRoute
}

const CreatePost: React.FC<CreatePostProps> = ({ route }) => {
  const { setOptions } = useNavigation()

  const { releaseCollection } = route.params

  const [supportState, setSupportState] = useState(releaseCollection?.supportState ?? '')
  const [mediaState, setMediaState] = useState(releaseCollection?.mediaState ?? '')
  const [notes, setNotes] = useState('')
  const [price, setPrice] = useState('')

  const [images, setImages] = useState<ImageResult[]>([])
  const [imagesKeys, setImagesKeys] = useState<string[]>(
    releaseCollection.ownImages?.map((img) => img.key) ?? [],
  )

  const { createPost, isLoading } = useCreatePost()

  const onCreatePost = useCallback(() => {
    if (!price) {
      Toast.show({ text: 'Debe ingresar el precio' })
      return
    }
    if (releaseCollection) {
      createPost({
        release: releaseCollection?._id,
        price,
        notes,
        releaseUpdate: {
          releaseId: releaseCollection?._id,
          release: {
            mediaState,
            supportState,
            imagesKeys,
          },
          images,
        },
      })
    }
  }, [createPost, releaseCollection, notes, price, images, mediaState, supportState, imagesKeys])

  useLayoutEffect(() => {
    setOptions({
      headerLeft: () => (
        <TouchableOpacity onPress={NavigationService.goBack}>
          <Icon type="AntDesign" name="close" style={styles.closeIcon} />
        </TouchableOpacity>
      ),
      headerRight: () => (
        <TouchableOpacity onPress={onCreatePost} style={styles.headerRight}>
          {isLoading ? (
            <Spinner color={EStyleSheet.value('$textColor')} style={styles.spinner} size="small" />
          ) : (
            <Text style={styles.textHeaderRight}>Publicar</Text>
          )}
        </TouchableOpacity>
      ),
      title: 'Nueva pulicación',
    })
  }, [setOptions, onCreatePost, isLoading])

  const onChangeImages = (newImages: ImageResult[], ownImages: OwnImage[]) => {
    setImages(newImages)
    setImagesKeys(ownImages.map((image) => image.key))
  }

  return (
    <Container style={styles.container}>
      <Content contentContainerStyle={styles.contentContainer}>
        <StatePicker
          current={supportState}
          title="Estado del soporte"
          onSelect={(value) => setSupportState(value)}
        />
        <StatePicker
          current={mediaState}
          title="Estado del vinilo/cassete/cd"
          onSelect={(value) => setMediaState(value)}
        />
        <Item style={styles.item}>
          <Text style={styles.textCurrency}>$</Text>
          <Input
            placeholder="Precio"
            onChangeText={(text: string) => {
              if (!isNaN(Number(text))) setPrice(text)
            }}
            value={price}
            keyboardType="numeric"
            style={styles.inputCurrency}
          />
        </Item>
        <Textarea
          rowSpan={5}
          bordered
          placeholder="Escribe cualquier aclaración que necesites hacer sobre la publicación"
          style={styles.textArea}
          underline
          onChangeText={setNotes}
          value={notes}
        />
        <Text style={styles.label}>Imagenes</Text>
        <View style={styles.divider} />
        <ImagePicker onChange={onChangeImages} defaultImages={releaseCollection.ownImages} />
      </Content>
    </Container>
  )
}

export { CreatePost }
