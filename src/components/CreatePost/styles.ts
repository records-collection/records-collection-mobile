import EStyleSheet from 'react-native-extended-stylesheet'

import { COLORS } from 'styles'

const styles = EStyleSheet.create({
  container: {
    backgroundColor: '#dfdede',
  },
  contentContainer: {
    backgroundColor: '#dfdede',
    padding: 16,
  },
  button: {
    width: '80%',
    marginVertical: 20,
    alignSelf: 'center',
    backgroundColor: '#2ecc71',
    height: 50,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 7,
  },
  textArea: {
    backgroundColor: COLORS.WHITE,
    borderRadius: 10,
    marginBottom: 30,
  },
  closeIcon: {
    marginLeft: 10,
    color: '$textColor',
  },
  headerRight: {
    marginRight: 10,
  },
  textHeaderRight: {
    fontSize: '1.1rem',
    textDecorationLine: 'underline',
    color: '$textColor',
  },
  spinner: {
    marginRight: 30,
  },
  textCurrency: {
    color: '$textColor',
    fontSize: '1.5rem',
  },
  item: {
    marginBottom: 10,
  },
  inputCurrency: {
    width: 100,
    backgroundColor: COLORS.WHITE,
    borderRadius: 10,
  },
  label: {
    color: 'gray',
  },
  divider: {
    width: '100%',
    height: 1,
    backgroundColor: 'gray',
  },
})

export { styles }
