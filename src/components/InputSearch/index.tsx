import { Icon, Input, Item } from 'native-base'
import React from 'react'

import { styles } from './styles'

interface InputSearchProps {
  placeholder: string
  onChangeText: (text: string) => void
}

const InputSearch: React.FC<InputSearchProps> = ({ placeholder, onChangeText }) => {
  return (
    <Item style={styles.item}>
      <Icon name="ios-search" style={styles.icon} />
      <Input
        placeholder={placeholder}
        onChangeText={onChangeText}
        style={styles.input}
        autoCapitalize="none"
      />
    </Item>
  )
}

export { InputSearch }
