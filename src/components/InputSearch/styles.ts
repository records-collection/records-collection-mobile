import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  item: {
    paddingHorizontal: '1rem',
    backgroundColor: '$backgroundColor',
    marginLeft: 0,
  },
  icon: {
    color: '$textColor',
  },
  input: {
    color: '$textPlaceholderColor',
  },
})

export { styles }
