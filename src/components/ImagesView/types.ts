import { RouteProp } from '@react-navigation/native'

type ImagesViewParamList = {
  ImagesView: {
    images: string[]
    initialIndex: number
  }
}

export type ImagesViewRoute = RouteProp<ImagesViewParamList, 'ImagesView'>
