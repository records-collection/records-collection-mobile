import EStyleSheet from 'react-native-extended-stylesheet'

import { COLORS } from 'styles'

const styles = EStyleSheet.create({
  closeIcon: {
    fontSize: 35,
    color: COLORS.WHITE,
    zIndex: 1000,
    position: 'absolute',
    top: 30,
    left: 10,
    height: 200,
  },
})

export { styles }
