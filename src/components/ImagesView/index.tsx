import React from 'react'
import { View, Icon } from 'native-base'
import ImageViewer from 'react-native-image-zoom-viewer'

import { NavigationService } from 'services'
import { Layouts } from 'styles'

import { ImagesViewRoute } from './types'
import { styles } from './styles'

interface ImagesViewProps {
  route: ImagesViewRoute
}

const ImagesView: React.FC<ImagesViewProps> = ({ route }) => {
  const { images, initialIndex } = route.params

  return (
    <View style={Layouts.flex}>
      <Icon
        type="AntDesign"
        name="close"
        style={styles.closeIcon}
        onPress={NavigationService.goBack}
      />
      <ImageViewer
        index={initialIndex}
        imageUrls={images.map((image) => ({ url: image }))}
        onCancel={NavigationService.goBack}
        enableSwipeDown
      />
    </View>
  )
}

export { ImagesView }
