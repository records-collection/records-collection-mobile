import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  button: {
    height: 40,
    width: 40,
    backgroundColor: '#e7e7e7',
    borderRadius: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 5,
    marginBottom: 5,
  },
  icon: {
    fontSize: 22,
    color: '#404040',
  },
})

export { styles }
