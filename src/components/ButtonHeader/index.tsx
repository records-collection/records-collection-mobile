import React from 'react'
import { Icon } from 'native-base'
import { TouchableOpacity } from 'react-native'

import { styles } from './styles'

interface ButtonHeaderProps {
  icon: string
  onPress: () => void
  active?: boolean
  activeColor?: string
  typeIcon?:
    | 'AntDesign'
    | 'Entypo'
    | 'EvilIcons'
    | 'Feather'
    | 'FontAwesome'
    | 'FontAwesome5'
    | 'Foundation'
    | 'Ionicons'
    | 'MaterialCommunityIcons'
    | 'MaterialIcons'
    | 'Octicons'
    | 'SimpleLineIcons'
    | 'Zocial'
}

const ButtonHeader: React.FC<ButtonHeaderProps> = ({
  icon,
  onPress,
  active,
  activeColor,
  typeIcon,
}) => (
  <TouchableOpacity style={styles.button} onPress={() => onPress()}>
    <Icon
      type={typeIcon ?? 'FontAwesome'}
      name={icon}
      // eslint-disable-next-line react-native/no-inline-styles
      style={[styles.icon, { color: active ? activeColor : '#404040' }]}
    />
  </TouchableOpacity>
)

export { ButtonHeader }
