import EStyleSheet from 'react-native-extended-stylesheet'

import { COLORS } from 'styles'

const styles = EStyleSheet.create({
  container: {
    backgroundColor: '#dfdede',
  },
  contentContainer: {
    backgroundColor: '#dfdede',
    padding: 16,
  },
  button: {
    width: '80%',
    marginVertical: 20,
    alignSelf: 'center',
    backgroundColor: '#2ecc71',
    height: 50,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 7,
  },
  textArea: {
    backgroundColor: COLORS.WHITE,
    borderRadius: 10,
    marginBottom: 30,
  },
  closeIcon: {
    marginLeft: 10,
    color: '$textColor',
  },
  headerRight: {
    marginRight: 10,
  },
  textHeaderRight: {
    fontSize: '1.1rem',
    textDecorationLine: 'underline',
    color: '$textColor',
  },
  spinner: {
    marginRight: 30,
  },
  textCurrency: {
    color: 'gray',
    fontSize: '1.5rem',
  },
  item: {
    marginBottom: 10,
  },
  inputCurrency: {
    width: 100,
    backgroundColor: COLORS.WHITE,
    borderRadius: 10,
  },
  label: {
    color: 'gray',
  },
  divider: {
    width: '100%',
    height: 1,
    backgroundColor: 'gray',
  },
  tag: {
    marginTop: 10,
    marginBottom: 15,
  },
  forward: {
    alignSelf: 'center',
    marginHorizontal: 10,
    marginBottom: 5,
  },
  stateContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  stateInfo: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  buttonAction: {
    alignSelf: 'center',
    marginVertical: 5,
  },
  avatar: {
    marginRight: 10,
  },
  textChangeState: {
    marginBottom: 15,
    fontSize: '1.2rem',
    fontWeight: '600',
  },
})

export { styles }
