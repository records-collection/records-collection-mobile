import React, { useCallback, useLayoutEffect, useState } from 'react'
import {
  Container,
  Content,
  Spinner,
  Text,
  Textarea,
  Icon,
  Item,
  Input,
  View,
  Button,
} from 'native-base'
import { RouteProp, useNavigation } from '@react-navigation/native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import EStyleSheet from 'react-native-extended-stylesheet'

import { ImageResult, OwnImage, Post, PostStateEnum } from 'types'
import { NavigationService } from 'services'
import { useUpdatePost } from 'hooks'
import { Layouts } from 'styles'
import { AvatarIcon } from 'assets/icons'

import { ImagePicker } from '../ImagePicker'
import { Toast } from '../Toast'
import { Tag } from '../Tag'
import { StatePicker } from '../StatePicker'
import { ConfirmDialog } from '../ConfirmDialog'
import { styles } from './styles'

type ParamList = {
  EditPostRoute: {
    post: Post
  }
}

export type EditPostRoute = RouteProp<ParamList, 'EditPostRoute'>

interface EditPostProps {
  route: EditPostRoute
}

const EditPost: React.FC<EditPostProps> = ({ route }) => {
  const { setOptions } = useNavigation()

  const { post } = route.params

  const [isConfirmReservationVisible, setIsConfirmReservationVisible] = useState(false)
  const [isCancelReservationVisible, setIsCancelReservationVisible] = useState(false)

  const [supportState, setSupportState] = useState(post.release?.supportState ?? '')
  const [mediaState, setMediaState] = useState(post.release?.mediaState ?? '')
  const [notes, setNotes] = useState(post.notes ?? '')
  const [price, setPrice] = useState(post.price.toString())

  const [newState, setNewState] = useState<string>()
  const [soldTo, setSoldTo] = useState<string>()

  const [images, setImages] = useState<ImageResult[]>([])
  const [imagesKeys, setImagesKeys] = useState<string[]>(
    post.release.ownImages?.map((img) => img.key) ?? [],
  )

  const { updatePost, isLoading } = useUpdatePost()

  const onCreatePost = useCallback(() => {
    if (!price) {
      Toast.show({ text: 'Debe ingresar el precio' })
      return
    }
    if (post) {
      updatePost({
        postId: post._id,
        images,
        notes,
        state: newState,
        price,
        soldTo,
        releaseUpdate: {
          releaseId: post.release._id,
          imagesKeys,
          supportState,
          mediaState,
        },
      })
    }
  }, [
    updatePost,
    notes,
    price,
    post,
    images,
    imagesKeys,
    mediaState,
    supportState,
    newState,
    soldTo,
  ])

  useLayoutEffect(() => {
    setOptions({
      headerLeft: () => (
        <TouchableOpacity onPress={NavigationService.goBack}>
          <Icon type="AntDesign" name="close" style={styles.closeIcon} />
        </TouchableOpacity>
      ),
      headerRight: () => (
        <TouchableOpacity onPress={onCreatePost} style={styles.headerRight}>
          {isLoading ? (
            <Spinner color={EStyleSheet.value('$textColor')} style={styles.spinner} size="small" />
          ) : (
            <Text style={styles.textHeaderRight}>Confirmar</Text>
          )}
        </TouchableOpacity>
      ),
      title: 'Editar pulicación',
    })
  }, [setOptions, onCreatePost, isLoading])

  const onChangeImages = (newImages: ImageResult[], ownImages: OwnImage[]) => {
    setImages(newImages)
    setImagesKeys(ownImages.map((image) => image.key))
  }

  return (
    <Container style={styles.container}>
      <Content contentContainerStyle={styles.contentContainer}>
        <StatePicker
          current={supportState}
          title="Estado del soporte"
          onSelect={(value) => setSupportState(value)}
        />
        <StatePicker
          current={mediaState}
          title="Estado del vinilo/cassete/cd"
          onSelect={(value) => setMediaState(value)}
        />
        <Item style={styles.item}>
          <Text style={styles.textCurrency}>$</Text>
          <Input
            placeholder="Precio"
            onChangeText={(text: string) => {
              if (!isNaN(Number(text))) setPrice(text)
            }}
            value={price}
            keyboardType="numeric"
            style={styles.inputCurrency}
          />
        </Item>
        <Textarea
          rowSpan={5}
          bordered
          placeholder="Escribe cualquier aclaración que necesites hacer sobre la publicación"
          style={styles.textArea}
          underline
          onChangeText={setNotes}
          value={notes}
        />
        <Text style={styles.label}>Estado</Text>
        <View style={styles.divider} />
        {post.state === PostStateEnum.Available && (
          <View style={Layouts.row}>
            <Tag text="Disponible" color="green" style={styles.tag} />
          </View>
        )}
        {post.state === PostStateEnum.Reserved && (
          <View style={styles.stateContainer}>
            <View style={styles.stateInfo}>
              <Tag text="Reservado" color="orange" style={styles.tag} />
              <Text style={styles.forward}>{'>'}</Text>
              <AvatarIcon width={35} height={35} style={styles.avatar} />
              <Text>{post.reservedBy.username}</Text>
            </View>
            <View>
              <Button
                success
                bordered
                small
                style={styles.buttonAction}
                onPress={() => setIsConfirmReservationVisible(true)}
              >
                <Icon type="Entypo" name="check" />
              </Button>
              <Button
                danger
                bordered
                small
                style={styles.buttonAction}
                onPress={() => setIsCancelReservationVisible(true)}
              >
                <Icon type="AntDesign" name="close" />
              </Button>
            </View>
          </View>
        )}
        {post.state === PostStateEnum.Sold && (
          <View>
            <View style={styles.stateContainer}>
              <View style={styles.stateInfo}>
                <Tag text="Vendido" color="red" style={styles.tag} />
                <Text style={styles.forward}>{'>'}</Text>
                <AvatarIcon width={35} height={35} style={styles.avatar} />
                <Text>{post.reservedBy.username}</Text>
              </View>
            </View>
          </View>
        )}
        {newState === PostStateEnum.Available && (
          <Text style={styles.textChangeState}>
            *Se cancelará la reserva luego de confirmar los cambios
          </Text>
        )}
        {newState === PostStateEnum.Sold && (
          <Text style={styles.textChangeState}>*Se confirmará la reserva </Text>
        )}
        <Text style={styles.label}>Imagenes</Text>
        <View style={styles.divider} />
        <ImagePicker onChange={onChangeImages} defaultImages={post.release.ownImages} />
      </Content>
      <ConfirmDialog
        title="Confirmar la reserva y actualizar la publicación a Vendida"
        visible={isConfirmReservationVisible}
        onCancel={() => setIsConfirmReservationVisible(false)}
        onConfirm={() => {
          setIsConfirmReservationVisible(false)
          setNewState(PostStateEnum.Sold)
          setSoldTo(post.reservedBy._id)
        }}
      />
      <ConfirmDialog
        title="Cancelar reserva"
        visible={isCancelReservationVisible}
        onCancel={() => setIsCancelReservationVisible(false)}
        onConfirm={() => {
          setIsCancelReservationVisible(false)
          setNewState(PostStateEnum.Available)
        }}
      />
    </Container>
  )
}

export { EditPost }
