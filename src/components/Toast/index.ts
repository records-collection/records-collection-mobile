import { Toast as NBToast } from 'native-base'

const Toast = {
  show: ({ text, type }: { text: string; type?: 'danger' | 'success' | 'warning' }) =>
    NBToast.show({ text, textStyle: { textAlign: 'center' }, type }),
}

export { Toast }
