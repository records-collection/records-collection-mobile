import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  content: {
    paddingTop: 5,
    padding: 15,
  },
  headerStyle: {
    color: '$backgroundColor',
  },
  contentStyle: {
    color: '$backgroundColor',
  },
  title: {
    color: '$textColor',
  },
  contentText: {
    color: '$textColor',
  },
  icon: {
    color: '$textColor',
  },
})

export { styles }
