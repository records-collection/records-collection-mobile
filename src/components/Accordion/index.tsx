import React from 'react'
import { Accordion as NBAccordion, RnViewStyleProp, Text, View } from 'native-base'

import { styles } from './styles'

interface AccordionProps {
  title: string
  contentText?: string
  contentComponent?: React.ReactElement
  headerStyle?: RnViewStyleProp | Array<RnViewStyleProp>
  contentStyle?: RnViewStyleProp | Array<RnViewStyleProp>
  containerStyle?: RnViewStyleProp | Array<RnViewStyleProp>
}

const Accordion: React.FC<AccordionProps> = ({
  title,
  contentText,
  contentComponent,
  headerStyle,
  contentStyle,
  containerStyle,
}) => {
  const renderContent = () => (
    <View style={styles.content}>
      {contentText && <Text style={styles.contentText}>{contentText}</Text>}
      {contentComponent}
    </View>
  )

  return (
    <View style={containerStyle}>
      <NBAccordion
        iconStyle={styles.icon}
        headerStyle={[styles.headerStyle, headerStyle]}
        contentStyle={[styles.contentStyle, contentStyle]}
        renderContent={renderContent}
        dataArray={[
          {
            title: <Text style={styles.title}>{title}</Text>,
            content: renderContent,
          },
        ]}
      />
    </View>
  )
}

export { Accordion }
