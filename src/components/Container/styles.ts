import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  container: {
    backgroundColor: '$backgroundColor',
  },
})

export { styles }
