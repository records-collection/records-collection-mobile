import React from 'react'
import { Container as NBContainer, RnViewStyleProp } from 'native-base'

import { styles } from './styles'

interface ContainerProps {
  style?: RnViewStyleProp | Array<RnViewStyleProp>
}

const Container: React.FC<ContainerProps> = ({ children, style }) => (
  <NBContainer style={[styles.container, style]}>{children}</NBContainer>
)

export { Container }
