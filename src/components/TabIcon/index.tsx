import React from 'react'
import { RouteProp } from '@react-navigation/native'
import { Icon } from 'native-base'

import { CollectionIcon } from 'assets/icons'
import { TAB_NAMES } from 'consts'

interface Props {
  route: RouteProp<Record<string, object | undefined>, string>
  color: string
}

const TabIcon: React.FC<Props> = ({ route, color }) => {
  let iconName
  let type:
    | 'AntDesign'
    | 'Entypo'
    | 'EvilIcons'
    | 'Feather'
    | 'FontAwesome'
    | 'FontAwesome5'
    | 'Foundation'
    | 'Ionicons'
    | 'MaterialCommunityIcons'
    | 'MaterialIcons'
    | 'Octicons'
    | 'SimpleLineIcons'
    | 'Zocial'
    | undefined

  if (route.name === TAB_NAMES.SEARCH) {
    iconName = 'search'
  } else if (route.name === TAB_NAMES.PREFERENCES) {
    iconName = 'ios-person'
  } else if (route.name === TAB_NAMES.POSTS) {
    iconName = 'store'
    type = 'MaterialCommunityIcons'
  } else if (route.name === TAB_NAMES.COLLECTION) {
    return <CollectionIcon fill={color} width={24} height={25} />
  }

  return <Icon type={type} name={iconName} style={{ color }} />
}

export { TabIcon }
