import React, { useCallback, useContext, useLayoutEffect, useState } from 'react'
import { Container, Content, Spinner, Text, Textarea, Icon } from 'native-base'
import { RouteProp, useNavigation } from '@react-navigation/native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import EStyleSheet from 'react-native-extended-stylesheet'

import { ImageResult, OwnImage, Release, ReleaseCollection } from 'types'
import { UserContext } from 'contexts'
import { discogsReleaseToCollectionRelease } from 'features/search/utils'
import { NavigationService } from 'services'
import { useCollection } from 'hooks'

import { StatePicker } from '../StatePicker'
import { ImagePicker } from '../ImagePicker'
import { styles } from './styles'

type ParamList = {
  AddOrEditRelease: {
    release?: Release
    releaseCollection?: ReleaseCollection
    isUpdate?: boolean
  }
}

export type AddOrEditReleaseRoute = RouteProp<ParamList, 'AddOrEditRelease'>

interface AddOrEditReleaseProps {
  route: AddOrEditReleaseRoute
}

const AddOrEditRelease: React.FC<AddOrEditReleaseProps> = ({ route }) => {
  const { setOptions } = useNavigation()

  const { release, releaseCollection, isUpdate } = route.params

  const { user } = useContext(UserContext)

  const [supportState, setSupportState] = useState(releaseCollection?.supportState ?? '')
  const [mediaState, setMediaState] = useState(releaseCollection?.mediaState ?? '')
  const [personalNotes, setPersonalNotes] = useState(releaseCollection?.personalNotes ?? '')

  const [images, setImages] = useState<ImageResult[]>([])
  const [imagesKeys, setImagesKeys] = useState<string[]>(
    releaseCollection?.ownImages?.map((img) => img.key) ?? [],
  )

  const { addRelease, isLoadingAddRelease, updateRelease, isLoadingUpdateRelease } = useCollection()

  const updateReleaseCollection = useCallback(() => {
    if (user?.recordCollection && releaseCollection?._id) {
      const payload = {
        releaseId: releaseCollection._id,
        release: {
          supportState,
          mediaState,
          personalNotes,
          imagesKeys,
        },
        images,
      }
      updateRelease(payload)
    }
  }, [
    mediaState,
    personalNotes,
    releaseCollection,
    supportState,
    updateRelease,
    user?.recordCollection,
    imagesKeys,
    images,
  ])

  const addReleaseToCollection = useCallback(() => {
    if (user?.recordCollection && release) {
      const payload = discogsReleaseToCollectionRelease(release, user, {
        supportState,
        mediaState,
        personalNotes,
      })

      addRelease({ release: payload, images })
    }
  }, [addRelease, mediaState, personalNotes, release, supportState, user, images])

  const onPress = useCallback(() => {
    isUpdate ? updateReleaseCollection() : addReleaseToCollection()
  }, [addReleaseToCollection, isUpdate, updateReleaseCollection])

  useLayoutEffect(() => {
    setOptions({
      headerLeft: () => (
        <TouchableOpacity onPress={NavigationService.goBack}>
          <Icon type="AntDesign" name="close" style={styles.closeIcon} />
        </TouchableOpacity>
      ),
      headerRight: () => (
        <TouchableOpacity onPress={onPress} style={styles.headerRight}>
          {isLoadingAddRelease || isLoadingUpdateRelease ? (
            <Spinner color={EStyleSheet.value('$textColor')} style={styles.spinner} size="small" />
          ) : (
            <Text style={styles.textHeaderRight}>{isUpdate ? 'Actualizar' : 'Agregar'}</Text>
          )}
        </TouchableOpacity>
      ),
      title: '',
    })
  }, [setOptions, isUpdate, isLoadingAddRelease, isLoadingUpdateRelease, onPress])

  const onChangeImages = (newImages: ImageResult[], ownImages: OwnImage[]) => {
    setImages(newImages)
    setImagesKeys(ownImages.map((image) => image.key))
  }

  return (
    <Container style={styles.container}>
      <Content contentContainerStyle={styles.contentContainer}>
        <StatePicker
          current={supportState}
          title="Estado del soporte"
          onSelect={(value) => setSupportState(value)}
        />
        <StatePicker
          current={mediaState}
          title="Estado del vinilo/cassete/cd"
          onSelect={(value) => setMediaState(value)}
        />
        <Textarea
          rowSpan={5}
          bordered
          placeholder="Notas"
          style={styles.textArea}
          underline
          onChangeText={setPersonalNotes}
          value={personalNotes}
        />
        <ImagePicker onChange={onChangeImages} defaultImages={releaseCollection?.ownImages} />
      </Content>
    </Container>
  )
}

export { AddOrEditRelease }
