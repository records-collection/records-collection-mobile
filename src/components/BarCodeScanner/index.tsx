import React, { useState, useEffect } from 'react'
import { Text } from 'react-native'
import { BarCodeEvent, BarCodeScanner as BarCodeScannerExpo } from 'expo-barcode-scanner'
import { Container, Spinner, View } from 'native-base'

import { SCREEN_NAMES } from 'consts'
import { NavigationService } from 'services'
import { SearchResponse } from 'types'
import { useBarScannerSearch } from 'hooks'

import { InfoDialog } from '../InfoDialog'
import { styles } from './styles'

const BarCodeScanner: React.FC = () => {
  const [barcode, setBarcode] = useState<string | undefined>('')

  const [hasPermission, setHasPermission] = useState<boolean>()
  const [isNotResultDialogOpen, setIsNotResultDialogOpen] = useState(false)

  const onSuccess = (response: SearchResponse) => {
    setBarcode(undefined)
    const { results } = response
    if (results.length === 1) {
      NavigationService.navigate(SCREEN_NAMES.RELEASE_DETAIL_SCREEN, {
        releaseId: results[0].id,
      })
    }
    if (results.length > 1) {
      NavigationService.navigate(SCREEN_NAMES.SEARCH_SCREEN, { barcode })
    }
    if (results.length === 0) {
      setIsNotResultDialogOpen(true)
    }
  }

  const { isLoading } = useBarScannerSearch(barcode, onSuccess)

  useEffect(() => {
    const requesPermission = async () => {
      const { status } = await BarCodeScannerExpo.requestPermissionsAsync()
      setHasPermission(status === 'granted')
    }
    requesPermission()
  }, [setBarcode])

  const handleBarCodeScanned = ({ data }: BarCodeEvent) => {
    setBarcode(data)
  }

  if (hasPermission === null) {
    return <Text>Requesting for camera permission</Text>
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>
  }

  return (
    <Container style={styles.container}>
      <BarCodeScannerExpo onBarCodeScanned={handleBarCodeScanned} style={styles.barCodeScanner} />
      {isLoading && (
        <View style={styles.spinnerView}>
          <Spinner color="#fff" size="large" />
        </View>
      )}
      <InfoDialog
        visible={isNotResultDialogOpen}
        title="Ups! No encontramos nada!"
        onConfirm={() => setIsNotResultDialogOpen(false)}
      />
    </Container>
  )
}

export { BarCodeScanner }
