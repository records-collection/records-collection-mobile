import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  container: {
    width: '100%',
    height: 400,
  },
  barCodeScanner: EStyleSheet.absoluteFillObject,
  spinnerView: {
    // ...EStyleSheet.absoluteFillObject,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    width: '100%',
  },
})

export { styles }
