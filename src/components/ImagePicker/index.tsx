import React, { useState, useEffect } from 'react'
import { Button, View, Icon } from 'native-base'
import { Platform, FlatList } from 'react-native'
import * as ExpoImagePicker from 'expo-image-picker'

import { ImageResult, OwnImage } from 'types'

import { styles } from './styles'
import { ImageItem } from './components'

interface ImagePickerProps {
  onChange: (images: ImageResult[], ownImages: OwnImage[]) => void
  defaultImages?: OwnImage[]
}

const ImagePicker: React.FC<ImagePickerProps> = ({ onChange, defaultImages }) => {
  const [images, setImages] = useState<ImageResult[]>([])

  const [ownImages, setOwnImages] = useState<OwnImage[]>(defaultImages ?? [])

  useEffect(() => {
    ;(async () => {
      if (Platform.OS !== 'web') {
        const { status } = await ExpoImagePicker.requestCameraRollPermissionsAsync()
        if (status !== 'granted') {
          // eslint-disable-next-line no-alert
          alert('Sorry, we need camera roll permissions to make this work!')
        }
      }
    })()
  }, [])

  const pickImage = async () => {
    const result = await ExpoImagePicker.launchImageLibraryAsync({
      mediaTypes: ExpoImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    })
    if (!result.cancelled) {
      const newImages = [...images, { uri: result.uri, name: 'image.jpg', type: 'image/jpeg' }]
      setImages(newImages)
      onChange(newImages, ownImages)
    }
  }

  const takeImage = async () => {
    const result = await ExpoImagePicker.launchCameraAsync({
      mediaTypes: ExpoImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    })
    if (!result.cancelled) {
      const newImages = [...images, { uri: result.uri, name: 'image.jpg', type: 'image/jpeg' }]
      setImages(newImages)
      onChange(newImages, ownImages)
    }
  }

  const removeImage = (imageUri: string) => {
    const newImages = images.filter((image) => image.uri !== imageUri)
    setImages(newImages)
    onChange(newImages, ownImages)
  }

  const deleteImageKey = (key: string) => {
    const newOwnImages = ownImages.filter((image) => image.key !== key)
    setOwnImages(newOwnImages)
    onChange(images, newOwnImages)
  }

  return (
    <View style={styles.container}>
      {/* <Text style={styles.title}>Agregar imagenes</Text> */}
      <View style={styles.buttonsContainer}>
        <Button onPress={pickImage} style={styles.button} transparent bordered>
          <Icon type="MaterialIcons" name="photo-library" />
        </Button>
        <Button onPress={takeImage} style={styles.button} transparent bordered>
          <Icon type="MaterialIcons" name="camera-alt" />
        </Button>
      </View>
      <FlatList
        data={images}
        renderItem={({ item }) => (
          <ImageItem key={item.uri} uri={item.uri} onPressTrashIcon={() => removeImage(item.uri)} />
        )}
        numColumns={3}
        keyExtractor={(item) => item.uri}
      />
      <FlatList
        data={ownImages}
        renderItem={({ item }) => (
          <ImageItem
            key={item.key}
            uri={item.uri}
            onPressTrashIcon={() => deleteImageKey(item.key)}
          />
        )}
        numColumns={3}
        keyExtractor={(item) => item.key}
      />
    </View>
  )
}

export { ImagePicker }
