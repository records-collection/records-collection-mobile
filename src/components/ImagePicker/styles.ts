import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonsContainer: {
    flexDirection: 'row',
  },
  imagesContainer: {
    flexDirection: 'row',
  },
  button: {
    margin: 5,
  },
  title: {
    color: '$backgroundContrastColor',
    fontSize: '1.2rem',
  },
})

export { styles }
