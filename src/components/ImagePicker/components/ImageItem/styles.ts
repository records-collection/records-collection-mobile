import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  container: {
    position: 'relative',
  },
  image: {
    margin: 5,
    width: 100,
    height: 100,
  },
  button: {
    position: 'absolute',
    top: 0,
    right: 0,
    zIndex: 2,
  },
  icon: {
    color: 'red',
  },
})

export { styles }
