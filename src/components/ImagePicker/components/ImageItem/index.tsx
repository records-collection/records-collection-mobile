import { Button, Icon, View } from 'native-base'
import React from 'react'
import { Image } from 'react-native'

import { styles } from './styles'

interface ImageItemProps {
  uri: string
  onPressTrashIcon?: () => void
}

const ImageItem: React.FC<ImageItemProps> = ({ uri, onPressTrashIcon }) => {
  return (
    <View style={styles.container}>
      <Button icon transparent style={styles.button} onPress={onPressTrashIcon}>
        <Icon name="trash" style={styles.icon} />
      </Button>
      <Image source={{ uri }} style={styles.image} />
    </View>
  )
}

export { ImageItem }
