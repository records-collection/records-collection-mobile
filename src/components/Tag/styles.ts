import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  tag: {
    paddingVertical: 6,
    paddingHorizontal: 12,
    borderRadius: 15,
  },
})

export { styles }
