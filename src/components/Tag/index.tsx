import React from 'react'
import { View, Text, RnViewStyleProp } from 'native-base'

import { styles } from './styles'

interface TagProps {
  text: string
  color?: string
  textColor?: string
  style?: RnViewStyleProp | Array<RnViewStyleProp>
}

const Tag: React.FC<TagProps> = ({ text, color, textColor, style }) => (
  <View style={[styles.tag, { backgroundColor: color }, style]}>
    <Text style={{ color: textColor ?? 'white' }}>{text}</Text>
  </View>
)

export { Tag }
