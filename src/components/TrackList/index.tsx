import React from 'react'
import { View, Text } from 'native-base'

import { Track } from 'types'
import { Layouts } from 'styles'

import { styles } from './styles'

interface TracklistProps {
  tracklist: Track[]
}

const Tracklist: React.FC<TracklistProps> = ({ tracklist }) => (
  <View style={styles.container}>
    <Text style={styles.title}>Tracklist</Text>
    {tracklist.map((track) => (
      <View style={styles.track}>
        <View style={Layouts.row}>
          <Text style={styles.positionText}>{`${track.position}.`}</Text>
          <Text style={styles.titleText}>{track.title}</Text>
        </View>
        <Text style={styles.durationText}>{track.duration}</Text>
      </View>
    ))}
  </View>
)

export { Tracklist }
