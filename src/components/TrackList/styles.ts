import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  container: {
    margin: 20,
  },
  title: {
    fontWeight: 'bold',
    color: '$textColor',
    marginBottom: 20,
    fontSize: '1.2rem',
  },
  track: {
    flexDirection: 'row',
    marginBottom: 5,
    justifyContent: 'space-between',
    width: '80%',
  },
  positionText: {
    fontWeight: 'bold',
    color: '$textColor',
    width: 30,
  },
  titleText: {
    color: '$textColor',
  },
  durationText: {
    color: '$textColor',
  },
})

export { styles }
