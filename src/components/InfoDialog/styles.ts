import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  overlay: {
    width: 300,
    textAlign: 'center',
    borderRadius: 15,
    padding: 0,
  },
  overlayTitle: {
    textAlign: 'center',
    marginTop: 30,
    marginBottom: 40,
    marginHorizontal: 20,
    fontSize: '1.2rem',
    color: '#3f3f3f',
  },
  viewButtons: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    borderTopWidth: 0.5,
    borderTopColor: 'gray',
  },
  button: {
    height: 55,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
})

export { styles }
