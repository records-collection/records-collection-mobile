import React from 'react'
import { Overlay } from 'react-native-elements'
import { View, Text } from 'native-base'
import { TouchableOpacity } from 'react-native'

import { styles } from './styles'

interface InfoDialogProps {
  visible: boolean
  onConfirm: () => void
  title: string
}

const InfoDialog: React.FC<InfoDialogProps> = ({ visible, onConfirm, title }) => {
  return (
    <Overlay overlayStyle={styles.overlay} isVisible={visible} onBackdropPress={() => onConfirm()}>
      <View>
        <Text style={styles.overlayTitle}>{title}</Text>
        <View style={styles.viewButtons}>
          <TouchableOpacity style={styles.button} onPress={() => onConfirm()}>
            <Text>Aceptar</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Overlay>
  )
}

export { InfoDialog }
