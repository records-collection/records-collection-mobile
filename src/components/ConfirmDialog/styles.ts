import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  overlay: {
    width: 300,
    textAlign: 'center',
    borderRadius: 15,
    padding: 0,
    backgroundColor: '$backgroundContrastColor',
  },
  overlayTitle: {
    textAlign: 'center',
    marginTop: 30,
    marginBottom: 40,
    marginHorizontal: 20,
    fontSize: '1.2rem',
    color: '$textColor',
  },
  viewButtons: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    borderTopWidth: 0.5,
    borderTopColor: '$textColor',
  },
  button: {
    height: 55,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: '50%',
    alignSelf: 'center',
  },
  divider: {
    width: 1,
    backgroundColor: '$textColor',
  },
  textButton: {
    color: '$textColor',
    width: '100%',
    textAlign: 'center',
  },
})

export { styles }
