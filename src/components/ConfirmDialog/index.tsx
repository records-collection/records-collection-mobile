import React from 'react'
import { Overlay } from 'react-native-elements'
import { View, Text } from 'native-base'
import { TouchableOpacity } from 'react-native-gesture-handler'

import { styles } from './styles'

interface ConfirmDialogProps {
  visible: boolean
  onCancel: () => void
  onConfirm: () => void
  title: string
}

const ConfirmDialog: React.FC<ConfirmDialogProps> = ({ visible, onCancel, onConfirm, title }) => {
  return (
    <Overlay overlayStyle={styles.overlay} isVisible={visible} onBackdropPress={() => onCancel()}>
      <View>
        <Text style={styles.overlayTitle}>{title}</Text>
        <View style={styles.viewButtons}>
          <TouchableOpacity style={styles.button} onPress={() => onCancel()}>
            <Text style={styles.textButton}>Cancelar</Text>
          </TouchableOpacity>
          <View style={styles.divider} />
          <TouchableOpacity style={styles.button} onPress={() => onConfirm()}>
            <Text style={styles.textButton}>Aceptar</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Overlay>
  )
}

export { ConfirmDialog }
