import React from 'react'
import { View, Text } from 'native-base'
import { TouchableOpacity as TouchableOpacityGestureHandler } from 'react-native-gesture-handler'
import { Platform, TouchableOpacity } from 'react-native'

import { styles } from './styles'

interface StatePickerProps {
  title: string
  onSelect: (value: string) => void
  current: string
}

interface OptionProps {
  label: string
  selected: string
  onPress: (value: string) => void
}

const Option: React.FC<OptionProps> = ({ label, onPress, selected }) => {
  if (Platform.OS === 'ios') {
    return (
      <TouchableOpacity
        onPress={() => onPress(label)}
        style={[styles.option, selected === label && styles.optionSelected]}
      >
        <Text style={styles.optionText}>{label}</Text>
      </TouchableOpacity>
    )
  } else {
    return (
      <TouchableOpacityGestureHandler
        onPress={() => onPress(label)}
        style={[styles.option, selected === label && styles.optionSelected]}
      >
        <Text style={styles.optionText}>{label}</Text>
      </TouchableOpacityGestureHandler>
    )
  }
}

const StatePicker: React.FC<StatePickerProps> = ({ title, onSelect, current }) => (
  <View style={styles.container}>
    <Text style={styles.title}>{title}</Text>
    <View style={styles.optionsView}>
      <Option label="P/F" onPress={onSelect} selected={current} />
      <Option label="G" onPress={onSelect} selected={current} />
      <Option label="VG" onPress={onSelect} selected={current} />
      <Option label="VG+" onPress={onSelect} selected={current} />
      <Option label="NM" onPress={onSelect} selected={current} />
      <Option label="M" onPress={onSelect} selected={current} />
    </View>
  </View>
)

export { StatePicker }
