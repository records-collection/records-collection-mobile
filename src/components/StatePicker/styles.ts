import EStyleSheet from 'react-native-extended-stylesheet'

const styles = EStyleSheet.create({
  container: {
    height: 100,
    alignSelf: 'center',
  },
  title: {
    fontSize: '1rem',
    textAlign: 'center',
  },
  optionsView: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '95%',
  },
  option: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'gray',
    width: 35,
    height: 35,
    borderRadius: 5,
    marginHorizontal: 6,
  },
  optionText: {
    fontSize: '0.8rem',
  },
  optionSelected: {
    backgroundColor: 'green',
  },
})

export { styles }
