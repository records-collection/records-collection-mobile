import { View } from 'native-base'
import React from 'react'
import { SliderBox } from 'react-native-image-slider-box'

import { SCREEN_NAMES } from 'consts'
import { NavigationService } from 'services'
import { Image, OwnImage } from 'types'

import { styles } from './styles'

interface ImageSliderProps {
  images: Image[]
  ownImages?: OwnImage[]
}

const ImageSlider: React.FC<ImageSliderProps> = ({ images, ownImages }) => (
  <View style={styles.container}>
    <SliderBox
      images={ownImages?.length ? ownImages.map((img) => img.uri) : images.map((img) => img.uri)}
      sliderBoxHeight={250}
      resizeMode="cover"
      imageLoadingColor="gray"
      onCurrentImagePressed={(index) =>
        NavigationService.navigate(SCREEN_NAMES.IMAGE_VIEW_SCREEN, {
          images: ownImages?.length
            ? ownImages.map((image) => image.uri)
            : images.map((image) => image.uri),
          initialIndex: index,
        })
      }
    />
  </View>
)

export { ImageSlider }
