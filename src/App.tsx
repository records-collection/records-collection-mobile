import { registerRootComponent } from 'expo'
import React, { useEffect } from 'react'
import { Root, View } from 'native-base'
import { ThemeProvider } from 'react-native-elements'
import { setConsole } from 'react-query'
import EStyleSheet from 'react-native-extended-stylesheet'
import * as SplashScreen from 'expo-splash-screen'
import { AppearanceProvider, useColorScheme } from 'react-native-appearance'

import { ContextProviders } from 'contexts'
import { Splash } from 'components'
import { useFonts } from 'hooks'
import { darkTheme, lightTheme } from 'styles'

import { NavigationContainer } from '../src/navigation'

setConsole({
  log: console.log,
  warn: console.warn,
  error: console.log,
})

SplashScreen.preventAutoHideAsync()

const App = () => {
  const { isLoadingFonts } = useFonts()
  const scheme = useColorScheme()

  useEffect(() => {
    EStyleSheet.build(scheme === 'dark' ? darkTheme : lightTheme)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return isLoadingFonts ? (
    <View />
  ) : (
    <AppearanceProvider>
      <Root>
        <ContextProviders>
          <Splash>
            <ThemeProvider>
              <NavigationContainer />
            </ThemeProvider>
          </Splash>
        </ContextProviders>
      </Root>
    </AppearanceProvider>
  )
}

export default registerRootComponent(App)
