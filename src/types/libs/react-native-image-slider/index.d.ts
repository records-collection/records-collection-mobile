declare module 'react-native-image-slider-box' {
  import React from 'react'
  export class SliderBox extends React.Component<SliderBoxProps> {}

  interface SliderBoxProps {
    images: string[]
    ImageComponentStyle?: object
    sliderBoxHeight?: number
    resizeMethod?: string
    resizeMode?: string
    dotStyle?: object
    disableOnPress?: boolean
    onCurrentImagePressed?: (index: number) => void
    imageLoadingColor?: string
  }
}
