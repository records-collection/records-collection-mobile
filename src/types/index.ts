import { UpdateReleasePayload } from 'hooks/useCollection/types'

export interface User {
  id: string
  username: string
  email: string
  password?: string
  recordCollection: string
  postsSaved: string[]
}

export interface Artist {
  id: number
  name: string
}

export interface Serie {
  id: number
  name: string
  catno: string
}

export interface Label {
  id: number
  name: string
  catno: string
}

export interface Format {
  name: string
  descriptions: string[]
  qty: string
}

export interface Company {
  name: string
  entity_type_name: string
  catno: string
}

export interface AdditionalData {
  personalNotes: string
  mediaState: string
  supportState: string
}

export interface Release {
  id: number
  title: string
  cover_image: string
  images: Image[]
  artists: Artist[]
  artists_sort: string
  series: Serie[]
  label: string[]
  formats: Format[]
  country: string
  year: number
  released_formatted: string
  released: string
  genres: string[]
  styles: string[]
  companies: Company[]
  notes: string
  catno: string
}

export interface OwnImage {
  uri: string
  key: string
}

export interface ReleaseCollection {
  _id: string
  title: string
  images: Image[]
  artists: Artist[]
  year: number
  country: string
  discogsId: number
  formats: Format[]
  supportState?: string
  mediaState?: string
  personalNotes?: string
  styles: string[]
  companies: Company[]
  genres: string[]
  label: string[]
  series: Serie[]
  collectionId: string
  userId: string
  ownImages?: OwnImage[]
  ownImage?: OwnImage
}

export interface Image {
  uri: string
  type: 'primary' | 'secondary'
}

export interface SearchResponse {
  results: Release[]
  pagination: Pagination
}

export interface SearchArtistsResponse {
  results: ArtistDiscogs[]
  pagination: Pagination
}

export interface Pagination {
  page: number
  pages: number
  per_page: number
  items: number
}

export interface SearchParams {
  query: string
  format?: string
  country?: string
  year?: string
  page?: number
  type?: string
  pageSize?: number
}

export interface ArtistDiscogs {
  id: number
  type: string
  master_id: number | null
  master_url: string | null
  uri: string
  title: string
  thumb: string
  cover_image: string
  resource_url: string
}

interface Group {
  id: number
  name: string
  resource_url: string
  active: boolean
}

export interface ArtistDetailDiscogs {
  name: string
  id: number
  resource_url: string
  uri: string
  releases_url: string
  images: Image[]
  profile: string
  urls: string[]
  namevariations: string[]
  groups: Group[]
  data_quality: string
}

export interface Master {
  id: number
  title: string
  type: string
  main_release: number
  artist: string
  role: string
  resource_url: string
  year: number
  thumb: string
  stats: {
    community: {
      in_wantlist: number
      in_collection: number
    }
  }
}

export interface Track {
  position: string
  type_: string
  title: string
  extraartists: Artist[]
  duration: string
}

export interface Video {
  uri: string
  title: string
  description: string
  duration: number
  enbed: true
}

export interface MasterRelease {
  id: number
  images: Image[]
  genres: string[]
  styles: string[]
  year: number
  tracklist: Track[]
  title: string
  notes: string
  videos: Video[]
  artists: Artist[]
}

export interface VersionRelease {
  status: string
  stats: {
    user: {
      in_collection: number
      in_wantlist: number
    }
    community: {
      in_collection: number
      in_wantlist: number
    }
  }
  thumb: string
  format: string
  country: string
  title: string
  label: string
  released: string
  major_formats: string[]
  catno: string
  resource_url: string
  id: number
}

export interface Comment {
  _id: string
  comment: string
  userId: string
  username: string
  userPicture?: string
  createdAt: Date
}

// eslint-disable-next-line no-shadow
export enum PostStateEnum {
  Reserved = 'Reserved',
  Available = 'Available',
  Sold = 'Sold',
}

export interface Post {
  _id: string
  state: PostStateEnum
  likes: string[]
  createdAt: Date
  price: number
  notes?: string
  user: {
    _id: string
    email: string
    username: string
  }
  release: ReleaseCollection
  comments: Comment[]
  reservedBy: {
    _id: string
    email: string
    username: string
  }
}

export interface CreatePostForm {
  notes?: string
  release: string
  price: string
  releaseUpdate?: UpdateReleasePayload
}

export interface UpdatePostForm {
  postId: string
  notes?: string
  price: string
  state?: string
  soldTo?: string
  releaseUpdate?: UpdateReleasePostPayload
  images?: ImageResult[]
}

export interface Message {
  _id: string
  user: string
  type: 'Message' | 'Reservation'
  message: string
  createdAt: Date
  postId?: string
  postPicture?: string
  postTitle?: string
}

export interface Chat {
  _id: string
  user1: {
    _id: string
    username: string
  }
  user2: {
    _id: string
    username: string
  }
  messages: Message[]
  createdAt: Date
  updatedAt: Date
}

export interface SendMessageForm {
  chatId: string
  message: string
  type?: 'Message' | 'Reservation'
  postId?: string
  postPicture?: string
  postTitle?: string
}

export interface PostUpdateResponse {
  state: string
  likes: number
  _id: string
  price: number
  release: string
  notes: string
  user: string
  comments: Comment[]
  createdAt: Date
  updatedAt: Date
}

export interface ImageResult {
  uri: string
  name: string
  type: string
}

export interface UpdateReleasePostPayload {
  releaseId: string
  mediaState?: string
  supportState?: string
  personalNotes?: string
  imagesKeys?: string[]
}
