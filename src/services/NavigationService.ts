import { NavigationAction, NavigationContainerRef, CommonActions } from '@react-navigation/native'

let navigator: NavigationContainerRef

function setTopLevelNavigator(navigatorRef: NavigationContainerRef) {
  navigator = navigatorRef
}

function navigate(routeName: string, params?: object) {
  dispatch(
    CommonActions.navigate({
      name: routeName,
      params: params,
    }),
  )
}

function goBack() {
  dispatch(CommonActions.goBack())
}

function dispatch(action: NavigationAction) {
  try {
    navigator.dispatch(action)
  } catch (error) {
    console.error(error)
  }
}

export const NavigationService = {
  setTopLevelNavigator,
  navigate,
  goBack,
}
