const Config = {
  displayName: process.env.APP_DISPLAY_NAME,
  apiURL: process.env.API_URL,
}

export { Config }
