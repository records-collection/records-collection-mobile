const COLORS = {
  PRIMARY: '#383838',
  WHITE: '#fff',
  BLACK: '#000000',
}
export { COLORS }
