import { Theme } from '@react-navigation/native'

const DarkTheme: Theme = {
  dark: true,
  colors: {
    primary: '#fff',
    background: '#fff',
    card: '#383838',
    text: '#FFF',
    border: '0000028',
    notification: '#9933FF',
  },
}

const darkTheme = {
  $theme: 'dark',
  $backgroundColor: '#383838',
  $textColor: '#fff',
  $textPlaceholderColor: '#fff',
  $backgroundContrastColor: 'gray',
}

const lightTheme = {
  $theme: 'light',
  $backgroundColor: '#fff',
  $textColor: '#000000',
  $textPlaceholderColor: '#000000',
  $backgroundContrastColor: '#d9d9d9',
}

export { DarkTheme, darkTheme, lightTheme }
