import { StyleSheet } from 'react-native'

const Layouts = StyleSheet.create({
  flex: {
    flex: 1,
  },
  flexGrow: {
    flexGrow: 1,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  row: {
    flexDirection: 'row',
  },
  col: {
    flexDirection: 'column',
  },
  flexRow: {
    flex: 1,
    flexDirection: 'row',
  },
  flexCol: {
    flex: 1,
    flexDirection: 'column',
  },
})

export { Layouts }
