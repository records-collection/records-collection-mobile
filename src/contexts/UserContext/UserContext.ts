import React from 'react'

import { UserContextType } from './types'

const UserContext = React.createContext<UserContextType>({
  user: undefined,
  setUser: () => {},
})

export { UserContext }
