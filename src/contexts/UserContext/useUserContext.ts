import { useState } from 'react'

import { User } from 'types'

import { UserContextType } from './types'

const useUserContext = (): UserContextType => {
  const [user, setUser] = useState<User | undefined>(undefined)

  return { user, setUser }
}

export { useUserContext }
