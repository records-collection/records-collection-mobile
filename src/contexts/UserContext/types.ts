import { Dispatch, SetStateAction } from 'react'

import { User } from 'types'

export type UserContextType = {
  user?: User
  setUser: Dispatch<SetStateAction<User | undefined>>
}
