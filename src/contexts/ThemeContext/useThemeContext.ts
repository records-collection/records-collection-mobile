import { useCallback, useEffect, useState } from 'react'
import { useColorScheme } from 'react-native-appearance'
import AsyncStorage from '@react-native-async-storage/async-storage'

import { ASYNC_STORAGE_KEYS, SCHEMES } from 'consts'

import { ThemeContextType } from './types'

const useThemeContext = (): ThemeContextType => {
  const systemScheme = useColorScheme()

  const [scheme, setScheme] = useState<string | undefined>(undefined)

  const [theme, setTheme] = useState<string | undefined>(undefined)

  const setInitialScheme = useCallback(async () => {
    const initialScheme = await AsyncStorage.getItem(ASYNC_STORAGE_KEYS.SCHEME)
    setScheme(initialScheme ?? SCHEMES.LIGHT)
  }, [])

  useEffect(() => {
    setInitialScheme()
  }, [setInitialScheme])

  useEffect(() => {
    if (scheme) {
      AsyncStorage.setItem(ASYNC_STORAGE_KEYS.SCHEME, scheme)

      if (scheme === SCHEMES.SYSTEM) {
        setTheme(systemScheme === SCHEMES.DARK ? SCHEMES.DARK : SCHEMES.LIGHT)
      } else {
        setTheme(scheme)
      }
    }
  }, [scheme, systemScheme])

  return {
    scheme,
    setScheme,
    theme,
  }
}

export { useThemeContext }
