import { Dispatch, SetStateAction } from 'react'

import { User } from 'types'

export type UserContextType = {
  user?: User
  setUser: Dispatch<SetStateAction<User | undefined>>
}

export type ThemeContextType = {
  theme?: string
  scheme?: string
  setScheme: Dispatch<SetStateAction<string | undefined>>
}
