import React from 'react'

import { ThemeContextType } from './types'

const ThemeContext = React.createContext<ThemeContextType>({
  theme: undefined,
  scheme: undefined,
  setScheme: () => {},
})

export { ThemeContext }
