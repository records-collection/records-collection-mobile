import React from 'react'

import { ThemeContext, useThemeContext } from './ThemeContext'
import { UserContext, useUserContext } from './UserContext'

const ContextProviders: React.FC = ({ children }) => {
  const userContextValue = useUserContext()
  const themeContextValue = useThemeContext()

  return (
    <ThemeContext.Provider value={themeContextValue}>
      <UserContext.Provider value={userContextValue}>{children}</UserContext.Provider>
    </ThemeContext.Provider>
  )
}

export { ContextProviders }
