import { api } from 'utils'
import { SearchArtistsResponse, SearchParams, SearchResponse } from 'types'

const searchReleases = async (
  _: string,
  { query, format, country, year, type, pageSize }: SearchParams,
  page: number = 0,
): Promise<SearchResponse> => {
  const params = {
    query,
    type: type ?? 'release',
    format: format ?? '',
    pageSize: pageSize ?? 10,
    page: page,
    year: year ?? '',
    country: country ?? '',
  }
  const response = await api.get('/discogs/search', { params })

  return response.data
}

const searchArtists = async (
  _: string,
  { query, format, country, year, pageSize }: SearchParams,
  page: number = 0,
): Promise<SearchArtistsResponse> => {
  const params = {
    query,
    type: 'artist',
    format: format ?? '',
    pageSize: pageSize ?? 10,
    page: page,
    year: year ?? '',
    country: country ?? '',
  }
  const response = await api.get('/discogs/search', { params })

  return response.data
}

export const SearchApi = {
  searchReleases,
  searchArtists,
}
