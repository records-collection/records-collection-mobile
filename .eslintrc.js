module.exports = {
  root: true,
  extends: [
    '@react-native-community',
    'plugin:import/errors',
    'plugin:import/warnings',
    'plugin:import/typescript',
    'plugin:prettier/recommended',
  ],
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint'],
  settings: { 'import/resolver': { node: { paths: ['./src'] } } },
  rules: {
    // eslint-plugin-prettier
    'prettier/prettier': 'error',
    // eslint-plugin-import
    'import/order': [
      'error',
      {
        groups: ['builtin', 'external', 'internal', ['sibling', 'parent', 'index']],
        'newlines-between': 'always',
      },
    ],
    'import/named': 'off',
    'import/namespace': 'off',
    'import/default': 'off',
    'import/no-named-as-default-member': 'off',
    // eslint-plugin-react
    'react/jsx-filename-extension': [1, { extensions: ['.tsx', '.jsx'] }],
    // eslint-plugin-react-native
    'react-native/no-inline-styles': 'error',
    // eslint
    'no-param-reassign': ['error', { props: true }],
    'no-return-await': 'error',
    eqeqeq: 'error',
    'no-unneeded-ternary': 'error',
    // @typescript-eslint
    '@typescript-eslint/no-explicit-any': 'error',
  },
}
